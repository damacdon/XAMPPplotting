Import XAMPPplotting/HistoConf/WHTauTau/Histos_Defs.conf
##
# predefined cuts
##################

#eta
defineMultiLine BaseTausEtaCentral
    CombCut AND
            ParCut BaseTaus eta  |>=| 0.0
            ParCut BaseTaus eta  |<=| 0.8
    End_CombCut
End_MultiLine

defineMultiLine BaseTausEtaForward
    CombCut AND
            ParCut BaseTaus eta  |>| 0.8
            ParCut BaseTaus eta  |<=| 2.5
    End_CombCut
End_MultiLine

#iso lepton event
defineMultiLine RealIsoLeptonEvent
    NumParCut  SignalTrueLeptons > 0
End_MultiLine

#anti-iso lepton event
defineMultiLine RealAntiIsoLeptonEvent
    NumParCut  AntiIsoTrueLeptons > 0
End_MultiLine

# tau - iso lepton
defineMultiLine BaseTausEtaCentral_RealIsoLeptonEvent
    CombCut AND
            @BaseTausEtaCentral
            @RealIsoLeptonEvent
    End_CombCut
End_MultiLine

defineMultiLine BaseTausEtaForward_RealIsoLeptonEvent
    CombCut AND
            @BaseTausEtaForward
            @RealIsoLeptonEvent
    End_CombCut
End_MultiLine

# tau - anti-iso lepton
defineMultiLine BaseTausEtaCentral_RealAntiIsoLeptonEvent
    CombCut AND
            @BaseTausEtaCentral
            @RealAntiIsoLeptonEvent
    End_CombCut
End_MultiLine

defineMultiLine BaseTausEtaForward_RealAntiIsoLeptonEvent
    CombCut AND
            @BaseTausEtaForward
            @RealAntiIsoLeptonEvent
    End_CombCut
End_MultiLine


##################################################################################################################
#                             Base Taus for FF - Origin = Incl - Lepton = Incl                                   #
##################################################################################################################

# range incl, eta incl
NewVar  
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origIncl_etaIncl_prongIncl_rangeIncl_leptonIncl_varPt
    ParReader BaseTaus pt[0]
    xLabel p_{T}(Base #tau) [GeV]
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origIncl_etaIncl_prong1P_rangeIncl_leptonIncl_varPt
    ParReader BaseTaus1P pt[0]
    xLabel p_{T}(Base #tau 1p) [GeV]
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origIncl_etaIncl_prong3P_rangeIncl_leptonIncl_varPt
    ParReader BaseTaus3P pt[0]
    xLabel p_{T}(Base #tau 3p) [GeV]
EndVar

# range incl, eta 0-0.8
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origIncl_eta0p0to0p8_prongIncl_rangeIncl_leptonIncl_varPt
    ParReader BaseTaus pt[0]
    xLabel 0.0 < |#eta| < 0.8 p_{T}(Base #tau) [GeV]
    @BaseTausEtaCentral
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origIncl_eta0p0to0p8_prong1P_rangeIncl_leptonIncl_varPt
    ParReader BaseTaus1P pt[0]
    xLabel 0.0 < |#eta| < 0.8  p_{T}(Base #tau 1p) [GeV]
    @BaseTausEtaCentral
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origIncl_eta0p0to0p8_prong3P_rangeIncl_leptonIncl_varPt
    ParReader BaseTaus3P pt[0]
    xLabel 0.0 < |#eta| < 0.8  p_{T}(Base #tau 3p) [GeV]
    @BaseTausEtaCentral
EndVar

# range incl, eta 0.8-2.5
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origIncl_eta0p8to2p5_prongIncl_rangeIncl_leptonIncl_varPt
    ParReader BaseTaus pt[0]
    xLabel 0.8 < |#eta| < 2.5 p_{T}(Base #tau) [GeV]
    @BaseTausEtaForward
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origIncl_eta0p8to2p5_prong1P_rangeIncl_leptonIncl_varPt
    ParReader BaseTaus1P pt[0]
    xLabel 0.8 < |#eta| < 2.5 p_{T}(Base #tau 1p) [GeV]
    @BaseTausEtaForward
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origIncl_eta0p8to2p5_prong3P_rangeIncl_leptonIncl_varPt
    ParReader BaseTaus3P pt[0]
    xLabel 0.8 < |#eta| < 2.5 p_{T}(Base #tau 3p) [GeV]
    @BaseTausEtaForward
EndVar

#access to truth, so MC only...
ifdef isMC

##################################################################################################################
#                             Base Taus for FF - Origin = Not jet fake - Lepton = Incl                           #
##################################################################################################################

# range incl, eta incl
NewVar  
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origNotJetFake_etaIncl_prongIncl_rangeIncl_leptonIncl_varPt
    ParReader BaseTausNotJetFake pt[0]
    xLabel p_{T}(Not-jet-fake Base #tau) [GeV]
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origNotJetFake_etaIncl_prong1P_rangeIncl_leptonIncl_varPt
    ParReader BaseTausNotJetFake1P pt[0]
    xLabel p_{T}(Not-jet-fake Base #tau 1p) [GeV]
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origNotJetFake_etaIncl_prong3P_rangeIncl_leptonIncl_varPt
    ParReader BaseTausNotJetFake3P pt[0]
    xLabel p_{T}(Not-jet-fake Base #tau 3p) [GeV]
EndVar

# range incl, eta 0-0.8
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origNotJetFake_eta0p0to0p8_prongIncl_rangeIncl_leptonIncl_varPt
    ParReader BaseTausNotJetFake pt[0]
    xLabel 0.0 < |#eta| < 0.8 p_{T}(Not-jet-fake Base #tau) [GeV]
    @BaseTausEtaCentral
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origNotJetFake_eta0p0to0p8_prong1P_rangeIncl_leptonIncl_varPt
    ParReader BaseTausNotJetFake1P pt[0]
    xLabel 0.0 < |#eta| < 0.8  p_{T}(Not-jet-fake Base #tau 1p) [GeV]
    @BaseTausEtaCentral
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origNotJetFake_eta0p0to0p8_prong3P_rangeIncl_leptonIncl_varPt
    ParReader BaseTausNotJetFake3P pt[0]
    xLabel 0.0 < |#eta| < 0.8  p_{T}(Not-jet-fake Base #tau 3p) [GeV]
    @BaseTausEtaCentral
EndVar

# range incl, eta 0.8-2.5
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origNotJetFake_eta0p8to2p5_prongIncl_rangeIncl_leptonIncl_varPt
    ParReader BaseTausNotJetFake pt[0]
    xLabel 0.8 < |#eta| < 2.5 p_{T}(Not-jet-fake Base #tau) [GeV]
    @BaseTausEtaForward
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origNotJetFake_eta0p8to2p5_prong1P_rangeIncl_leptonIncl_varPt
    ParReader BaseTausNotJetFake1P pt[0]
    xLabel 0.8 < |#eta| < 2.5 p_{T}(Not-jet-fake Base #tau 1p) [GeV]
    @BaseTausEtaForward
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origNotJetFake_eta0p8to2p5_prong3P_rangeIncl_leptonIncl_varPt
    ParReader BaseTausNotJetFake3P pt[0]
    xLabel 0.8 < |#eta| < 2.5 p_{T}(Not-jet-fake Base #tau 3p) [GeV]
    @BaseTausEtaForward
EndVar

##################################################################################################################
#                             Base Taus for FF - Origin = Incl - Lepton = anti-iso Real                          #
##################################################################################################################

# range incl, eta incl
NewVar  
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origIncl_etaIncl_prongIncl_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTaus pt[0]
    xLabel real anti-iso lepton - p_{T}(Base #tau) [GeV]
    @RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origIncl_etaIncl_prong1P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTaus1P pt[0]
    xLabel real anti-iso lepton - p_{T}(Base #tau 1p) [GeV]
    @RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origIncl_etaIncl_prong3P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTaus3P pt[0]
    xLabel real anti-iso lepton - p_{T}(Base #tau 3p) [GeV]
    @RealAntiIsoLeptonEvent
EndVar

# range incl, eta 0-0.8
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origIncl_eta0p0to0p8_prongIncl_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTaus pt[0]
    xLabel real anti-iso lepton - 0.0 < |#eta| < 0.8 p_{T}(Base #tau) [GeV]
    @BaseTausEtaCentral_RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origIncl_eta0p0to0p8_prong1P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTaus1P pt[0]
    xLabel real anti-iso lepton - 0.0 < |#eta| < 0.8  p_{T}(Base #tau 1p) [GeV]
    @BaseTausEtaCentral_RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origIncl_eta0p0to0p8_prong3P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTaus3P pt[0]
    xLabel real anti-iso lepton - 0.0 < |#eta| < 0.8  p_{T}(Base #tau 3p) [GeV]
    @BaseTausEtaCentral_RealAntiIsoLeptonEvent
EndVar

# range incl, eta 0.8-2.5
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origIncl_eta0p8to2p5_prongIncl_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTaus pt[0]
    xLabel real anti-iso lepton 0.8 < |#eta| < 2.5 p_{T}(Base #tau) [GeV]
    @BaseTausEtaForward_RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origIncl_eta0p8to2p5_prong1P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTaus1P pt[0]
    xLabel real anti-iso lepton - 0.8 < |#eta| < 2.5 p_{T}(Base #tau 1p) [GeV]
    @BaseTausEtaForward_RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origIncl_eta0p8to2p5_prong3P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTaus3P pt[0]
    xLabel real anti-iso lepton - 0.8 < |#eta| < 2.5 p_{T}(Base #tau 3p) [GeV]
    @BaseTausEtaForward_RealAntiIsoLeptonEvent
EndVar

##################################################################################################################
#                             Base Taus for FF - Origin = Not jet fake - Lepton = AntiIsoReal                    #
##################################################################################################################

#eta incl
NewVar  
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origNotJetFake_etaIncl_prongIncl_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTausNotJetFake pt[0]
    xLabel real anti-iso lepton - p_{T}(Not-jet-fake Base #tau) [GeV]
    @RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origNotJetFake_etaIncl_prong1P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTausNotJetFake1P pt[0]
    xLabel real anti-iso lepton - p_{T}(Not-jet-fake Base #tau 1p) [GeV]
    @RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origNotJetFake_etaIncl_prong3P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTausNotJetFake3P pt[0]
    xLabel real anti-iso lepton - p_{T}(Not-jet-fake Base #tau 3p) [GeV]
    @RealAntiIsoLeptonEvent
EndVar

#eta <0.8
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origNotJetFake_eta0p0to0p8_prongIncl_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTausNotJetFake pt[0]
    xLabel real anti-iso lepton - 0.0 < |#eta| < 0.8 p_{T}(Not-jet-fake Base #tau) [GeV]
    @BaseTausEtaCentral_RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origNotJetFake_eta0p0to0p8_prong1P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTausNotJetFake1P pt[0]
    xLabel real anti-iso lepton - 0.0 < |#eta| < 0.8  p_{T}(Not-jet-fake Base #tau 1p) [GeV]
    @BaseTausEtaCentral_RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origNotJetFake_eta0p0to0p8_prong3P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTausNotJetFake3P pt[0]
    xLabel real anti-iso lepton - 0.0 < |#eta| < 0.8  p_{T}(Not-jet-fake Base #tau 3p) [GeV]
    @BaseTausEtaCentral_RealAntiIsoLeptonEvent
EndVar

#eta 0.8-2.5
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origNotJetFake_eta0p8to2p5_prongIncl_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTausNotJetFake pt[0]
    xLabel real anti-iso lepton - 0.8 < |#eta| < 2.5 p_{T}(Not-jet-fake Base #tau) [GeV]
    @BaseTausEtaForward_RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origNotJetFake_eta0p8to2p5_prong1P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTausNotJetFake1P pt[0]
    xLabel real anti-iso lepton - 0.8 < |#eta| < 2.5 p_{T}(Not-jet-fake Base #tau 1p) [GeV]
    @BaseTausEtaForward_RealAntiIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origNotJetFake_eta0p8to2p5_prong3P_rangeIncl_leptonAntiIsoReal_varPt
    ParReader BaseTausNotJetFake3P pt[0]
    xLabel real anti-iso lepton - 0.8 < |#eta| < 2.5 p_{T}(Not-jet-fake Base #tau 3p) [GeV]
    @BaseTausEtaForward_RealAntiIsoLeptonEvent
EndVar

##################################################################################################################
#                             Base Taus for FF - Origin = Incl - Lepton = Iso Real                               #
##################################################################################################################

# range incl, eta incl
NewVar  
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origIncl_etaIncl_prongIncl_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTaus pt[0]
    xLabel real iso lepton - p_{T}(Base #tau) [GeV]
    @RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origIncl_etaIncl_prong1P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTaus1P pt[0]
    xLabel real iso lepton - p_{T}(Base #tau 1p) [GeV]
    @RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origIncl_etaIncl_prong3P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTaus3P pt[0]
    xLabel real iso lepton - p_{T}(Base #tau 3p) [GeV]
    @RealIsoLeptonEvent
EndVar

# range incl, eta 0-0.8
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origIncl_eta0p0to0p8_prongIncl_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTaus pt[0]
    xLabel real iso lepton - 0.0 < |#eta| < 0.8 p_{T}(Base #tau) [GeV]
    @BaseTausEtaCentral_RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origIncl_eta0p0to0p8_prong1P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTaus1P pt[0]
    xLabel real iso lepton - 0.0 < |#eta| < 0.8  p_{T}(Base #tau 1p) [GeV]
    @BaseTausEtaCentral_RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origIncl_eta0p0to0p8_prong3P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTaus3P pt[0]
    xLabel real iso lepton - 0.0 < |#eta| < 0.8  p_{T}(Base #tau 3p) [GeV]
    @BaseTausEtaCentral_RealIsoLeptonEvent
EndVar

# range incl, eta 0.8-2.5
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origIncl_eta0p8to2p5_prongIncl_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTaus pt[0]
    xLabel real iso lepton 0.8 < |#eta| < 2.5 p_{T}(Base #tau) [GeV]
    @BaseTausEtaForward_RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origIncl_eta0p8to2p5_prong1P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTaus1P pt[0]
    xLabel real iso lepton - 0.8 < |#eta| < 2.5 p_{T}(Base #tau 1p) [GeV]
    @BaseTausEtaForward_RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origIncl_eta0p8to2p5_prong3P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTaus3P pt[0]
    xLabel real iso lepton - 0.8 < |#eta| < 2.5 p_{T}(Base #tau 3p) [GeV]
    @BaseTausEtaForward_RealIsoLeptonEvent
EndVar

##################################################################################################################
#                             Base Taus for FF - Origin = Not jet fake - Lepton = IsoReal                        #
##################################################################################################################

#eta incl
NewVar  
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origNotJetFake_etaIncl_prongIncl_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTausNotJetFake pt[0]
    xLabel real iso lepton - p_{T}(Not-jet-fake Base #tau) [GeV]
    @RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origNotJetFake_etaIncl_prong1P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTausNotJetFake1P pt[0]
    xLabel real iso lepton - p_{T}(Not-jet-fake Base #tau 1p) [GeV]
    @RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origNotJetFake_etaIncl_prong3P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTausNotJetFake3P pt[0]
    xLabel real iso lepton - p_{T}(Not-jet-fake Base #tau 3p) [GeV]
    @RealIsoLeptonEvent
EndVar

#eta <0.8
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origNotJetFake_eta0p0to0p8_prongIncl_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTausNotJetFake pt[0]
    xLabel real iso lepton - 0.0 < |#eta| < 0.8 p_{T}(Not-jet-fake Base #tau) [GeV]
    @BaseTausEtaCentral_RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origNotJetFake_eta0p0to0p8_prong1P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTausNotJetFake1P pt[0]
    xLabel real iso lepton - 0.0 < |#eta| < 0.8  p_{T}(Not-jet-fake Base #tau 1p) [GeV]
    @BaseTausEtaCentral_RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origNotJetFake_eta0p0to0p8_prong3P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTausNotJetFake3P pt[0]
    xLabel real iso lepton - 0.0 < |#eta| < 0.8  p_{T}(Not-jet-fake Base #tau 3p) [GeV]
    @BaseTausEtaCentral_RealIsoLeptonEvent
EndVar

#eta 0.8-2.5
NewVar
    Type 1D
    Template tauVar_pt_ff
    Name TauFF_defBase_origNotJetFake_eta0p8to2p5_prongIncl_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTausNotJetFake pt[0]
    xLabel real iso lepton - 0.8 < |#eta| < 2.5 p_{T}(Not-jet-fake Base #tau) [GeV]
    @BaseTausEtaForward_RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_1p
    Name TauFF_defBase_origNotJetFake_eta0p8to2p5_prong1P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTausNotJetFake1P pt[0]
    xLabel real iso lepton - 0.8 < |#eta| < 2.5 p_{T}(Not-jet-fake Base #tau 1p) [GeV]
    @BaseTausEtaForward_RealIsoLeptonEvent
EndVar

NewVar
    Type 1D
    Template tauVar_pt_ff_3p
    Name TauFF_defBase_origNotJetFake_eta0p8to2p5_prong3P_rangeIncl_leptonIsoReal_varPt
    ParReader BaseTausNotJetFake3P pt[0]
    xLabel real iso lepton - 0.8 < |#eta| < 2.5 p_{T}(Not-jet-fake Base #tau 3p) [GeV]
    @BaseTausEtaForward_RealIsoLeptonEvent
EndVar


#end of truth
endif


Import XAMPPplotting/HistoConf/LepHadStau/Histos_Defs.conf
##################################################################################################################
#                             Event-based complex Observables                                                    #
##################################################################################################################

# HT
NewVar 
    Type 1D
    Template Var_Ht
    Name ht_jets
    EvReader floatGeV Ht_Jet
    xLabel H_{T}(jets) [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

# PTt
NewVar 
    Type 1D
    Template pTt
    Name pTt
    EvReader floatGeV PTt
    xLabel p_{Tt} [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

#effective mass = pt lep + pt tau + pt met + pt jets (scalar)
NewVar 
    Type 1D
    Template Var_meff_low
    Name meff_low_range    
    EvReader floatGeV Meff
    xLabel m_{eff} [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar 
    Type 1D
    Template Var_meff_med
    Name meff_high_range    
    EvReader floatGeV Meff
    xLabel m_{eff} [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

### stransverse mass ##
NewVar
    Type 1D
    Template Var_mt2
    Name mt2_max
    EvReader floatGeV MT2_max
    xLabel max m_{T2}(lep, #tau, MET) [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template Var_mt2
    Name mt2_min
    EvReader floatGeV MT2_min
    xLabel min m_{T2}(lep, #tau, MET) [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

### angles between planes ###
NewVar
    Type 1D
    Template cos_chi
    Name CosChi1
    EvReader float CosChi1
    xLabel cos#chi(#tau, lepton | lead jet)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template cos_chi
    Name CosChi2
    EvReader float CosChi2
    xLabel cos#chi(#tau, lepton | lead jet, sublead jet)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

###       Event shape variables ###
NewVar
    Type 1D
    Template spherocity
    Name spherocity
    EvReader float Spherocity
    xLabel Spherocity
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template sphericity
    Name sphericity
    EvReader float Sphericity
    xLabel Sphericity
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template planarity
    Name planarity
    EvReader float Planarity
    xLabel Planarity
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template aplanarity
    Name aplanarity
    EvReader float Aplanarity
    xLabel Aplanarity
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template circularity
    Name circularity
    EvReader float Circularity
    xLabel Circularity
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

#NewVar
#    Type 1D
#    Template sphericity
#    Name lin_sphericity
#    EvReader float Lin_Sphericity
#    xLabel Linearized Sphericity
#    ifdef NormByBinWidth
#          DivideByBinWidth
#    endif
#EndVar
#
#NewVar
#    Type 1D
#    Template sphericity
#    Name lin_sphericity_C
#    EvReader float Lin_Sphericity_C
#    xLabel Linearized Sphericity C
#    ifdef NormByBinWidth
#          DivideByBinWidth
#    endif
#EndVar
#
#NewVar
#    Type 1D
#    Template sphericity
#    Name lin_sphericity_D
#    EvReader float Lin_Sphericity_D
#    xLabel Linearized Sphericity D
#    ifdef NormByBinWidth
#          DivideByBinWidth
#    endif
#EndVar
#
#NewVar
#    Type 1D
#    Template planarity
#    Name lin_planarity
#    EvReader float Lin_Planarity
#    xLabel Linearized Planarity
#    ifdef NormByBinWidth
#          DivideByBinWidth
#    endif
#EndVar
#
#NewVar
#    Type 1D
#    Template aplanarity
#    Name lin_aplanarity
#    EvReader float Lin_Aplanarity
#    xLabel Linearized Aplanarity
#    ifdef NormByBinWidth
#          DivideByBinWidth
#    endif
#EndVar
#
#NewVar
#    Type 1D
#    Template circularity
#    Name lin_circularity
#    EvReader float Lin_Circularity
#    xLabel Linearized Circularity
#    ifdef NormByBinWidth
#          DivideByBinWidth
#    endif
#EndVar

NewVar
    Type 1D
    Template thrust
    Name thrust
    EvReader float Thrust
    xLabel Thrust
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template thrust_axis
    Name thrust_major
    EvReader float ThrustMajor
    xLabel Thrust major axis
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template thrust_axis
    Name thrust_minor
    EvReader float ThrustMinor
    xLabel Thrust minor axis
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template oblateness
    Name oblateness
    EvReader float Oblateness
    xLabel Oblateness
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name normalized_fox_wolfram_moment_0
    EvReader float WolframMoment_0
    xLabel Normalized Fox-Wolfram moment 0
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name  normalized_fox_wolfram_moment_1
    EvReader float WolframMoment_1
    xLabel Normalized Fox-Wolfram moment 1
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name  normalized_fox_wolfram_moment_2
    EvReader float WolframMoment_2
    xLabel Normalized Fox-Wolfram moment 2
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name  normalized_fox_wolfram_moment_3
    EvReader float WolframMoment_3
    xLabel Normalized Fox-Wolfram moment 3
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name  normalized_fox_wolfram_moment_4
    EvReader float WolframMoment_4
    xLabel Normalized Fox-Wolfram moment 4
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name  normalized_fox_wolfram_moment_5
    EvReader float WolframMoment_5
    xLabel Normalized Fox-Wolfram moment 5
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name  normalized_fox_wolfram_moment_6
    EvReader float WolframMoment_6
    xLabel Normalized Fox-Wolfram moment 6
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name  normalized_fox_wolfram_moment_7
    EvReader float WolframMoment_7
    xLabel Normalized Fox-Wolfram moment 7
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name  normalized_fox_wolfram_moment_8
    EvReader float WolframMoment_8
    xLabel Normalized Fox-Wolfram moment 8
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name  normalized_fox_wolfram_moment_9
    EvReader float WolframMoment_9
    xLabel Normalized Fox-Wolfram moment 9
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

#NewVar
#    Type 1D
#    Template fwm
#    Name  normalized_fox_wolfram_moment_10
#    EvReader float WolframMoment_10
#    xLabel Normalized Fox-Wolfram moment 10
#    ifdef NormByBinWidth
#          DivideByBinWidth
#    endif
#EndVar
#
#NewVar
#    Type 1D
#    Template fwm
#    Name  normalized_fox_wolfram_moment_11
#    EvReader float WolframMoment_11
#    xLabel Normalized Fox-Wolfram moment 11
#    ifdef NormByBinWidth
#          DivideByBinWidth
#    endif
#EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_0
    EvReader float UnNormed_WolframMoment_0
    xLabel Unnormed Fox-Wolfram moment 0
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_1
    EvReader float UnNormed_WolframMoment_1
    xLabel Unnormed Fox-Wolfram moment 1
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_2
    EvReader float UnNormed_WolframMoment_2
    xLabel Unnormed Fox-Wolfram moment 2
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_3
    EvReader float UnNormed_WolframMoment_3
    xLabel Unnormed Fox-Wolfram moment 3
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_4
    EvReader float UnNormed_WolframMoment_4
    xLabel Unnormed Fox-Wolfram moment 4
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_5
    EvReader float UnNormed_WolframMoment_5
    xLabel Unnormed Fox-Wolfram moment 5
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_6
    EvReader float UnNormed_WolframMoment_6
    xLabel Unnormed Fox-Wolfram moment 6
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_7
    EvReader float UnNormed_WolframMoment_7
    xLabel Unnormed Fox-Wolfram moment 7
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_8
    EvReader float UnNormed_WolframMoment_8
    xLabel Unnormed Fox-Wolfram moment 8
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template fwm
    Name fox_wolfram_moment_9
    EvReader float UnNormed_WolframMoment_9
    xLabel Unnormed Fox-Wolfram moment 9
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

#NewVar
#    Type 1D
#    Template fwm
#    Name fox_wolfram_moment_10
#    EvReader float UnNormed_WolframMoment_10
#    xLabel Unnormed Fox-Wolfram moment 10
#    ifdef NormByBinWidth
#          DivideByBinWidth
#    endif
#EndVar
#
#NewVar
#    Type 1D
#    Template fwm
#    Name fox_wolfram_moment_11
#    EvReader float UnNormed_WolframMoment_11
#    xLabel Unnormed Fox-Wolfram moment 11
#    ifdef NormByBinWidth
#          DivideByBinWidth
#    endif
#EndVar





ifdef isMC
New_Branch
    Name mcChannelNumber
    Type int
    EvReader int mcChannelNumber
End_Branch
New_Branch
    Name MEPhoton
    Type bool
    EvReader char MEPhoton
End_Branch
New_Branch
    Name GenFiltMET
    Type float
    EvReader floatGeV GenFiltMET
End_Branch
New_Branch
    Name GenWeight
    Type float
    PseudoReader GenWeightPseudo 1
End_Branch
endif
ifdef isData
New_Branch
    Name mcChannelNumber
    Type int
    PseudoReader mcChannelNumber 0
End_Branch
New_Branch
    Name MEPhoton
    Type bool
    PseudoReader MEPhoton 1
End_Branch
endif

New_Branch
    Name eventNumber
    Type int
    EvReader llu eventNumber
End_Branch

New_Branch
    Name IsMETTrigPassed
    Type bool
    EvReader char IsMETTrigPassed
End_Branch
New_Branch
    Name treatAsYear
    Type int
    EvReader int treatAsYear
End_Branch

##########
# Leptons
##########
New_Branch
    Name N_SignalLeptons  
    Type int
    NumParReader SignalLeps
End_Branch
New_Branch
    Name Lep_pt
    Type floatVec
    ParReader SignalLeps pt
End_Branch
New_Branch
    Name Lep_eta
    Type floatVec
    ParReader SignalLeps eta
End_Branch
New_Branch
    Name Lep_charge
    Type floatVec
    ParReader SignalLeps charge
End_Branch
New_Branch
    Name Elec_pt
    Type floatVec
    ParReader SignalElectrons pt
End_Branch
New_Branch
    Name N_SigElectrons 
    Type int
    NumParReader SignalElectrons
End_Branch
New_Branch
    Name Elec_charge
    Type floatVec
    ParReader SignalElectrons charge
End_Branch
New_Branch
    Name N_SigMuons  
    Type int
    NumParReader SignalMuons
End_Branch
New_Branch
    Name Muon_pt
    Type floatVec
    ParReader SignalMuons pt
End_Branch
New_Branch
    Name Muon_charge
    Type floatVec
    ParReader SignalMuons charge
End_Branch
New_Branch
    Name LepType
    Type intVec
    DiPartCompReader SignalLeps
End_Branch


##########
# b-Jet multiplicity
##########
New_Branch
    Name N_BJets  
    Type int
    NumParReader Bjets_ASC_MV2c10
End_Branch


##########
# FatJets
##########
New_Branch
    Name NAntiKt8  
    Type int
    NumParReader FatJetsR8
End_Branch
New_Branch
    Name FatJet08_m
    Type floatVec
    ParReader FatJetsR8 m
End_Branch
New_Branch
    Name AntiKt8NConst
    Type intVec
    ParReader FatJetsR8 constituents
End_Branch
New_Branch
    Name AntiKt8_DijetMassAsym
    Type float
    EvReader float AsymmetryMTopR08
End_Branch

New_Branch
    Name NAntiKt12  
    Type int
    NumParReader FatJetsR12
End_Branch
New_Branch
    Name FatJet12_m
    Type floatVec
    ParReader FatJetsR12 m
End_Branch
New_Branch
    Name AntiKt12NConst
    Type intVec
    ParReader FatJetsR12 constituents
End_Branch
New_Branch
    Name AntiKt12_DijetMassAsym
    Type float
    EvReader float AsymmetryMTopR12
End_Branch

##########
# Met
##########
New_Branch
    Name MetTST_met
    Type float
    EvReader floatGeV MetTST_met
End_Branch
New_Branch
    Name MetTrack_met
    Type float
    EvReader floatGeV MetTrack_met
End_Branch


##########
# Others
##########
New_Branch
    Name HT
    Type float
    EvReader floatGeV HT
End_Branch
New_Branch
    Name HTLep
    Type float
    HtReader JetLeps
End_Branch

New_Branch
    Name MEff
    Type float
    MeffReader SignalJets MetTST
End_Branch
New_Branch
    Name MEffLep
    Type float
    MeffReader JetLeps MetTST
End_Branch

New_Branch
    Name HtSig
    Type float
    EvReader float HtSig
End_Branch
New_Branch
    Name HtSigLep
    Type float
    EvReader min HtSigLep
End_Branch

New_Branch
    Name MT2Chi2
    Type float
    EvReader floatGeV mt2
End_Branch

New_Branch
    Name DPhiMetTrackMet
    Type float
    |EvReader| float DPhiMetTrackMet
End_Branch
New_Branch
    Name JetDPhiMetMin2
    Type float
    |EvReader| min DeltaPhiMin_2
End_Branch
New_Branch
    Name JetDPhiMetMin3
    Type float
    |EvReader| min DeltaPhiMin_3
End_Branch
New_Branch
    Name AbsDeltaPhiMin4  
    Type float
    |EvReader| min DeltaPhiMin_4
End_Branch

New_Branch
    Name HasTauCand
    Type bool
    EvReader char HasTauCand
End_Branch

New_Branch
    Name MtTauCand 
    Type float
    EvReader floatGeV MtTauCand
End_Branch

New_Branch
    Name MinMt
    Type float
    EvReader floatGeV MtJetMin
End_Branch
New_Branch
    Name MtNonBMin
    Type float
    EvReader floatGeV MtNonBMin
End_Branch
New_Branch
    Name MtNonBMax
    Type float
    EvReader floatGeV MtNonBMax
End_Branch
New_Branch
    Name MtBMin
    Type float
    EvReader floatGeV MtBMin
End_Branch
New_Branch
    Name MtBMax
    Type float
    EvReader floatGeV MtBMax
End_Branch
New_Branch
    Name dRBB
    Type float
    |dRReader| Bjets_ASC_MV2c10[1] Bjets_ASC_MV2c10[0]
End_Branch
New_Branch
    Name MBB
    Type float
    InvDiMReader Bjets_ASC_MV2c10[1] Bjets_ASC_MV2c10[0]
End_Branch

New_Branch
    Name MtMetLep
    Type float
    EvReader floatGeV MtLepMet
End_Branch

New_Branch
    Name DRBLep0
    Type float
    |dRReader| Bjets_ASC_MV2c10[0] SignalLeps[0]
End_Branch
New_Branch
    Name DRBLep1
    Type float
    |dRReader| Bjets_ASC_MV2c10[1] SignalLeps[0]
End_Branch
New_Branch
    Name dRBLMin
    Type float
    EvReader min dRBLMinReader
End_Branch

New_Branch
    Name MtBMinLep
    Type float
    EvReader floatGeV MtBMinLep
End_Branch
New_Branch
    Name MtBMaxLep
    Type float
    EvReader floatGeV MtBMaxLep
End_Branch

New_Branch
    Name Mll
    Type float
    EvReader floatGeV Mll
End_Branch

ifdef isMC
    New_Branch
        Name averageInteractionsPerCrossing
        Type float
        EvReader float averageInteractionsPerCrossing
    End_Branch
endif
ifdef isData
    New_Branch
        Name averageInteractionsPerCrossing
        Type float
        EvReader float corr_avgIntPerX
    End_Branch
endif

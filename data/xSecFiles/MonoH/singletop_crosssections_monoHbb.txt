#dataset_number/I:physics_short/C:crossSection_pb/D:kFactor/D:genFiltEff/D:crossSectionTotalRelUncertUP/D
#id/I:name/C:xsec/F:kfac/F:eff/F:relunc/F
# cross-sections are in pb
# DSID  Physics short                               xsec    k-fac   eff     rel. unc. 
##########################################################################################################################
## missing cross-sections for single top background processes (not included in SUSYtools cross-section list)
## values taken from AMI

410659          PhPy8EG_A14_tchan_BW50_lept_antitop                   22.175                1.0      1.0000E+00  0.0                           
410658          PhPy8EG_A14_tchan_BW50_lept_top                       36.993                1.0      1.0000E+00  0.0                           
410647          PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop       37.907                1.0      1.0000E+00  0.0                           
410646          PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top           37.937                1.0      1.0000E+00  0.0                           
410645          PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop  1.2675                1.0      1.0000E+00  0.0                           
410644          PowhegPythia8EvtGen_A14_singletop_schan_lept_top      2.0267                1.0      1.0000E+00  0.0        

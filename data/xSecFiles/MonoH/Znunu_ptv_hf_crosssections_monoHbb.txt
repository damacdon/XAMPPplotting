dataset_number/I:physics_short/C:crossSection/D:genFiltEff/D:kFactor/D:relUncertUP/D:relUncertDOWN/D:generator_name/C
# cross-sections are in pb

# DSID  Physics short                               xsec     eff      k-fac      rel. unc.

##########################################################################################################################

## Adding (estimated) correct filter efficiencies for PTV / mJJ sliced b-filtered Znunu samples (366010-366017) for the time being.
## To DSID of the b-filtered sample is obtained by adding a 9 to the DSID listed below, e.g. for 366010 the corresponding b-filtered DSID is 366010/


366001 Sh_221_NN30NNLO_Znunu_PTV70_100_BFilter                        2.7516E+02      0.0801589426722     0.9728      1.0          1.0    Sherpa(v2.2.2)  
366002 Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ0_500_BFilter              1.0906E+02      0.0909001133318     0.9728      1.0          1.0    Sherpa(v2.2.2)
366003 Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ500_1000_BFilter           4.5528          0.121116378159      0.9728      1.0          1.0    Sherpa(v2.2.2)
366004 Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ1000_E_CMS_BFilter         1.2026          0.111392280028      0.9728      1.0          1.0    Sherpa(v2.2.2)
366005 Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ0_500_BFilter              5.1796E+01      0.0995896960004     0.9728      1.0          1.0    Sherpa(v2.2.2)
366006 Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ500_1000_BFilter           4.4727          0.126308611254      0.9728      1.0          1.0    Sherpa(v2.2.2)
366007 Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ1000_E_CMS_BFilter         1.3760          0.118453717673      0.9728      1.0          1.0    Sherpa(v2.2.2)
366008 Sh_221_NN30NNLO_Znunu_PTV280_500_BFilter                       4.2444          0.109206537849      0.9728      1.0          1.0    Sherpa(v2.2.2)

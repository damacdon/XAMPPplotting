## This file is the RunConfig for the mono-h(bb) analysis
## for the resolved signal region

#---------------------------------------------------------------------------------------
# The basic setup is imported from the common run config and the common definitions file
#---------------------------------------------------------------------------------------
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig.conf
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_commonDefinitions.conf


#----------------------------------------------------------------------------------------------------
# Option to re-calculate the b-tagging SFs.
# If this is used, TrackJetWeight_New must be added to the list of weights and TrackJetWeight removed
#----------------------------------------------------------------------------------------------------
# Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_bTagSF_Resolved.conf


#------------------------------
## Region definitions
## Define variables for regions
#------------------------------
defineVar Channel SR
defineVar PTV MetTST_met
defineVar BJets N_BJets_04
defineVar Topology Resolved
defineVar HJet Jet_BLight


## --------------------------------
## Region specific cuts and weights
## --------------------------------

## ---------------------------------------------------------------------------
## Weights
# The weights which should be read from the trees. DO NOT add the PileUp weight to the list in any case
## ---------------------------------------------------------------------------
Weights GenWeight GenWeightMCSampleMerging EleWeight MuoWeight TauWeight JetWeightJVT JetWeightBTag MET_TriggerSF Znunu_Normalization Znunu_Merging

@common_ZeroLepton
@common_Resolved

# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039
EvCut float DeltaPhiJJ  |<=| 2.44346095279
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239

# MET significance cut (also used as anti-QCD cut)
EvCut float MetTST_Significance_noPUJets_noSoftTerm > 12.

# At least one of the two Higgs candidate jets must have pT > 45 GeV
CombCut OR
    EvCut floatGeV Jet_BLight1BPt > 45.
    EvCut floatGeV Jet_BLight2BPt > 45.
End_CombCut

# pT sum of the three leading 2 (3) small-R jets must be > 120 (150) GeV
CombCut OR
    CombCut AND
        EvCut int N_Jets04 = 2
        EvCut floatGeV sigjet012ptsum > 120.
    End_CombCut
    CombCut AND
        EvCut int N_Jets04 > 2
        EvCut floatGeV sigjet012ptsum > 150.
    End_CombCut
End_CombCut

# DeltaR(jj) and HtRatio: Variables used for ttbar suppresion
EvCut float DeltaRJJ < 1.8
EvCut float HtRatioResolved <= 0.37

# blinding m_jj around the Higgs mass (always have this applied when comparing data17 and mc16d! can be removed for data1516 and mc16a)
@Blinding_Resolved
@Blinding_Merged

# special option for creating plots in Higgs mass window, should not be active if not explicitly requested
# @HiggsWindow_Resolved



#----------------------------------------------------------------------------------------------------------------------
## Config to produce plots with 2 b-tagged small-R jets, inclusive in the resolved region and in the different MET bins
#----------------------------------------------------------------------------------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_Resolved_2b.conf
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_2b.conf


#------------------------------------------------------------------------
## Config to produce plots for the b-tag inclusive and 0/1/3 b-tag region
#------------------------------------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_common_additional_Resolved.conf
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_additional.conf


#-----------------------------------------------
## Config to produce plots for V+jets background
#-----------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_Vjets_0123b.conf

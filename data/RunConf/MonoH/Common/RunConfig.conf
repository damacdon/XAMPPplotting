## This file is a baseline run config file where the common
## options for all mono-h(bb) plotting RunConfigs are defined
##

## Basic definitions
## -----------------------------------------------------------------------
### Lets tell the code the name of the tree in the input files i.e. SUSYAnalysisConfg_******
Tree MonoH

### This option defines the name of the top directories in the output histogram files.
### For the Tree files the name is taken from the SampleName property in the InputConfig
Analysis MonoH

### Create CutFlow histogram (needed for yields + significances!!)
doCutFlow

## Cross section
## ----------------------------------------------------------------------
### Use cross-section information from a specific directory
### Now the cross-section statments have been moved to the input configs separately for different processes as we want
### to use dummy xsec 1pb for signal samples and the PMG values for the others
### One needs to add in the input configs for the signal sample:
#xSecDir XAMPPplotting/xSecFiles/MonoH
#xSecDirBkg XAMPPplotting/xSecFiles/MonoH/signal_crosssections_monoHbb_dummy.txt
### and in the input configs for the other samples:

### xSecDir XAMPPplotting/xSecFiles/MonoH
### xSecDirBkg dev/PMGTools/2020-04-06/PMGxsecDB_mc16.txt

### Disable the cross-sections completely
#disableXS

## Systematics
## ---------------------------------------------------------------------------
### Run without systematics (nominal only, runs much quicker)
#noSyst

# LHE weights are there to evaluate the uncertainties on the theory
# parameters like the renormalization or the factorization scale or
# the choice of the PDF set. Each of the variation comes a long with its
# own sum of weights saved in the XAMPP metaData tree with processIDs beyond the 1k
#  There are two different ways to handle the uncertainties
#     1) "AcceptanceOnly":
#        Evaluate the effect of the LHE uncertainties on the acceptance only (DEFAULT).
#        Each of the LHE variations is normalized to its respective sum of weights.
#        This approach is usually taken if the value of the cross-section is taken from
#        external calculations.
#     2) "CrossSectionAndAcceptance"
#        Evaluate the combined effect of the LHE uncertainties on the cross-section and the
#        cross-section: Each LHE variation is normalized w.r.t. the nominal sum of weights

#LHENormalization AcceptanceOnly
#
## rename lhe systematics created from LHE weights (modeling systematics for scale + PDF variations)
#ifdef LHESystematics
#    ImportAlways XAMPPplotting/Misc/TruthSystematics/aMCatNLO_WeightVariations.conf
#    ImportAlways XAMPPplotting/Misc/TruthSystematics/PowHeg_WeightVariations.conf
#    ImportAlways XAMPPplotting/Misc/TruthSystematics/Sherpa_WeightVariations.conf
#endif


######################################################
#
# switch on/off PRW systematics as we have two 
# sets of Ntuples, one with a buggy PRW syst
#
######################################################

ExcludeSystematicW PRW_DATASF__1down
ExcludeSystematicW PRW_DATASF__1up

# Signal samples are affected by a MadGraph bug, due to which the nominal GenWeigts are stored at 0th position
# --> actual GenWeight is stored it LHE variation LHE_9 / LHE_45 for 2HDMa / Z'-2HDM samples
# --> exclude all other LHE_weights from being processed
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_ExcludeLHE.conf


# rename systematics
RenameSyst MET_JetTrk_ScaleDown MET_JetTrk_Scale__1down
RenameSyst MET_JetTrk_ScaleUp MET_JetTrk_Scale__1up
RenameSyst MET_SoftTrk_ScaleDown MET_SoftTrk_Scale__1down
RenameSyst MET_SoftTrk_ScaleUp MET_SoftTrk_Scale__1up
RenameSyst MonoH_MET_SoftTrk_ResoPara MonoH_MET_SoftTrk_ResoPara__1up
RenameSyst MonoH_MET_SoftTrk_ResoPerp MonoH_MET_SoftTrk_ResoPerp__1up


## Pile-up
## ---------------------------------------------------------------------------
### Running with PRW: which periods should be used (possible entries: `data15-16`, `data17`, `data18`, `data15_17`, `data17_18`, `data15_18`, `all`)
#redoPRW
normalizeToPeriod all


### Run without pile-up weights
# noPRW


## Particle readers
## ---------------------------------------------------------------------------
New_ParticleM JetReader Jet
    Var char bjet
    Var double BTagScore
    Var int n_MuonInJet
    ifdef isMC
        Var int HadronConeExclTruthLabelID
    endif
End_Particle

New_ParticleM BJetReader Jet
    Cut char bjet = 1
End_Particle

New_ParticleM ForwardJetReader ForwardJet
End_Particle

New_ParticleM FatJetReader FatJet
    Var int n_matchedasstrkjets
End_Particle

New_ParticleM TrackJetReader TrackJet
    Var char bjet
    Var double BTagScore
    Var char isAssociated
    Var char passDRcut
    ifdef isMC
        Var int HadronConeExclTruthLabelID
    endif
End_Particle

New_ParticleM BaselineMuons Muon
    Var float charge
End_Particle

New_ParticleM BaselineElectrons Electron
    Var float charge
End_Particle


Import XAMPPplotting/RunConf/LepHadStau/CommonConfigs/LeptonPair.conf
Import XAMPPplotting/RunConf/LepHadStau/CommonConfigs/Weights.conf
Import XAMPPplotting/RunConf/LepHadStau/CommonConfigs/CutDefinitions.conf


###########################################
# SR common cut sets for use in SR/CR files
###########################################
defineMultiLine SR_Common
    #@DataPreCuts
    @B_Veto
    #@MT_Tau_MET_Cut

    #dR(tau, lepton)    
    @LepTau_DeltaR_VeryLoose_Cut
    
End_MultiLine

defineMultiLine SR_Common_CR_QCD
     # here we only invert the lepton iso
     # keep the rest of cuts identical
     @SR_Common
End_MultiLine

defineMultiLine SR_Common_CR_Wjets
    #@DataPreCuts
    @B_Veto
    #invert MT
    @MT_Tau_MET_Inverse_Cut
End_MultiLine

defineMultiLine SR_Common_CR_Top
    #@DataPreCuts
    #invert b-veto and MT
    @B_Veto_Inverse
    #@B_Jets_Rich
    #@MT_Tau_MET_Inverse_Cut
End_MultiLine

#########################
## Preselection category
#########################
defineMultiLine SR_Presel    

    # visible mass
    @MT_Lep_MET_Veto


End_MultiLine

##############################
## MVA
##############################

defineMultiLine MV_0jetA
    
    #Main cut ###############
    @MET_Et_Cut
    #########################

    # lep-tau dEta
    @LepTau_DeltaEta_MVA_0jetA_Cut

    # centrality
    @MET_Centrality_MVA_Cut
    
    # Sig_MET
    @MET_Significance_MVA_0jetA_Cut

    # vectorial pT balance MET, lep, tau
    @MET_LepTau_VecSumPt_MVA_Cut

End_MultiLine

defineMultiLine MV_0jetB
    
    #Main cut ###############
    @MET_Et_Inverse_Cut
    #########################
  
    # lep-tau dEta
    @LepTau_DeltaEta_MVA_0jetB_Cut

    # centrality
    @MET_Centrality_MVA_Cut

    # vectorial pT balance MET, lep, tau
    @MET_LepTau_VecSumPt_MVA_Cut


End_MultiLine

#loose cuts for MV
defineMultiLine MV_1jet

    # RJR Z
    @RJR_Z_MVA_1jet_Cut

    # lep-tau dEta
    @LepTau_DeltaEta_MVA_1jet_Cut

    # lep-tau dR
    @LepTau_DeltaR_MVA_Cut

    # MET cut
    @MET_Et_Loose_Cut

    # Sig_MET
    @MET_Significance_MVA_1jet_Cut

    # Sum MT
    @MET_LepTau_SumMT_MVA_1jet_Cut

    # meff
    @Meff_MVA_Cut

End_MultiLine

##############################
## CBA
##############################
defineMultiLine CB_0jetA_Core

    #Cut on DR
    @LepTau_DeltaR_Loose_Cut

    #lep-tau deta
    @LepTau_DeltaEta_Tight_Cut

    #lep-tau Mvis cut
    @LepTau_Mvis_Medium_Cut    

    # MET cut
    #@MET_Et_Tight_Cut

    # MET signifiance
    @MET_Significance_Medium_Cut

    # MET Centrality
    @MET_centrality_Cut

    # Effective vectorial pT (tau, el, MET)
    @MET_LepTau_VecSumPt_Medium_Cut

    #Sum MT
    @MET_LepTau_SumMT_Tight_Cut
    

End_MultiLine

defineMultiLine CB_0jetB_Core

    #Cut on DR
    @LepTau_DeltaR_Loose_Cut

    #lep-tau deta
    @LepTau_DeltaEta_Tight_Cut

    #lep-tau m-vis
    @LepTau_Mvis_Loose_Cut

    # MET cut
    #@MET_Et_Medium_Cut

    # MET signifiance
    @MET_Significance_Loose_Cut

    # Sum Cos Delta Phi
    #@MET_SumCosDeltaPhi_Cut

    # Effective vectorial pT (tau, el, MET)
    @MET_LepTau_VecSumPt_Tight_Cut

    # MET Centrality
    @MET_centrality_Cut


End_MultiLine

defineMultiLine CB_0jetA
      #@MET_LepTau_SumMT_Tight_Cut
      @MET_Et_Cut
      @CB_0jetA_Core
End_MultiLine

defineMultiLine CB_0jetB
      #@MET_LepTau_SumMT_Tight_Inverse_Cut
      @MET_Et_Inverse_Cut
      @CB_0jetB_Core
End_MultiLine


defineMultiLine CB_1jet

    #dEta tau-lepton
    @LepTau_DeltaEta_VeryTight_Cut
    
    #lep-tau m-vis
    @LepTau_Mvis_Tight_Cut


    # MET signifiance
    @MET_Significance_Medium_Cut

    # MET Centrality
    @MET_centrality_Cut


End_MultiLine

#########################
## Select Regions
#########################
defineMultiLine SR_0jetA

     # signature of this category: no high-pt jets
     @N_0jet

     @MV_0jetA

End_MultiLine

defineMultiLine SR_0jetB

     # signature of this category: no high-pt jets
     @N_0jet
     @MV_0jetB
     
End_MultiLine

defineMultiLine SR_1jet
     # signature of this category: high-pt jets
     @N_1jet
     #MV
     @MV_1jet     
End_MultiLine

################################################
## Preselection category for dileptons
################################################
defineMultiLine SR_Presel_DiLep    
 
    #MET
    EvCut floatGeV MetTST_met > 0.
End_MultiLine


New_Particle SignalElectrons Elec 
    Cut floatGeV pt > 20
    Var float charge
End_Particle 

New_Particle SignalMuons Muon 
	Cut floatGeV pt > 20
	Var float charge
End_Particle 

DiParticle SignalLeps SignalMuons SignalElectrons

New_Particle SignalJets Jet 
    Var char bjet
    Var double MV2c10  
End_Particle 

DiParticle JetLeps SignalLeps SignalJets

New_Particle Bjets Jet
    Cut char bjet = 1 
    Cut float eta |<| 2.5 
    Var double MV2c10
End_Particle 
New_Particle LightJets Jet
    Cut char bjet = 0
    Var double MV2c10
End_Particle

# Sort the Bjets collection by the b-tagging weight (the closest weight to 1 goes first):
ResortedParticle_ASC Bjets MV2c10 1 # now a Bjets_ASC_MV2c10 collection is available

New_ParticleM TopCand1 RecoCandidates 
    Cut int RecoType = 1
    Cut int pdgId = 6    
End_Particle 

New_ParticleM TopCand2 RecoCandidates 
    Cut int RecoType = 2
    Cut int pdgId = 6
End_Particle 

New_ParticleM TopCand1Chi2 RecoCandidates 
    Cut int RecoType = 7
    Cut int pdgId = 6    
End_Particle 

New_ParticleM TopCand2Chi2 RecoCandidates 
    Cut int RecoType = 8
    Cut int pdgId = 6
End_Particle 

New_ParticleM FatJetsR12 FatJet12 
    Var int constituents
End_Particle 

New_ParticleM FatJetsR8 FatJet08
    Var int constituents
End_Particle 

New_Particle Photons Phot 
End_Particle 


xSecDir XAMPPplotting/xSecFiles/

# Luminosity in fb
Lumi 1.

# XAMPPplotting just runs over the nominal tree
#noSyst
# Create CutFlow histogram (needed for yields+significances!!)
doCutFlow

# Analysis name in input tree
Tree Stop0L

# Analysis name in output histogram file
Analysis Stop0L

# The weights which should be read from the trees. DO NOT add the PileUp weight to the list in any case
Weights EleWeight MuonWeight JetWeightBTag JetWeightJVT GenWeight PhoWeight

# SherpaVjetsNjetsWeight not needed anymore for Sherpa 2.2.1
#Disable the reweighting of SumW to  each SUSYproc Id -> unneccessary with the coming derivations
NoSUSYpidWeight

# recombine electron weight from its components
New_CondWeight EleWeight
    New_Conditional
        Weight EleWeightReco
    End_Conditional
    New_Conditional
        Weight EleWeightID
    End_Conditional
    New_Conditional
        Weight EleWeightIsol
    End_Conditional
    New_Conditional
        CombCut AND   
            EvCut int N_SignalLeptons = 1
            NumParCut SignalElectrons = 1
        End_CombCut
        Weight EleWeightTrig
    End_Conditional
End_CondWeight

# recombine muon weight from its components
New_CondWeight MuonWeight
	New_Conditional
		Weight MuoWeightReco
	End_Conditional
	New_Conditional
		Weight MuoWeightTTVA
	End_Conditional
	New_Conditional
		Weight MuoWeightIsol
	End_Conditional
	New_Conditional
    	CombCut AND   
			EvCut int N_SignalLeptons = 1
			NumParCut SignalMuons = 1
		End_CombCut
		Weight MuoWeightTrig
	End_Conditional
End_CondWeight

# do the sample overlap (incl. vs MET filtered) check for ttbar and single top
ifdef isMC
CombCut OR
    CombCut AND                                   
        EvCut int mcChannelNumber = 410000
        EvCut floatGeV GenFiltMET < 200
    End_CombCut
    CombCut AND                                   
        EvCut int mcChannelNumber = 407012
        EvCut floatGeV GenFiltMET < 300
    End_CombCut
    CombCut AND
        EvCut int mcChannelNumber = 410013
        EvCut floatGeV GenFiltMET < 200
    End_CombCut
    CombCut AND
        EvCut int mcChannelNumber = 410014
        EvCut floatGeV GenFiltMET < 200
    End_CombCut
    CombCut AND
        EvCut int mcChannelNumber = 410001
        EvCut floatGeV GenFiltMET < 200
    End_CombCut
    CombCut AND
        EvCut int mcChannelNumber = 410002
        EvCut floatGeV GenFiltMET < 200
    End_CombCut
    CombCut AND
        EvCut int mcChannelNumber = 410004
        EvCut floatGeV GenFiltMET < 200
    End_CombCut
    CombCut AND
        EvCut int mcChannelNumber != 410000
        EvCut int mcChannelNumber != 407012
        EvCut int mcChannelNumber != 410013
        EvCut int mcChannelNumber != 410014
        EvCut int mcChannelNumber != 410001
        EvCut int mcChannelNumber != 410002
        EvCut int mcChannelNumber != 410004
    End_CombCut
End_CombCut
endif

New_MinReader DeltaPhiMin_2
    |dPhiToMetReader| SignalJets[0-1] MetTST
End_MinReader
New_MinReader DeltaPhiMin_3
    |dPhiToMetReader| SignalJets[0-2] MetTST
End_MinReader

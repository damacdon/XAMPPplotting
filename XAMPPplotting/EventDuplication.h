#ifndef XAMPPPLOTTING_EVENTDUPLICATION_H
#define XAMPPPLOTTING_EVENTDUPLICATION_H
#include <XAMPPplotting/Selector.h>
namespace XAMPP {

    //###############################################################
    //      The EventIndexer caches the run & event number on data
    //
    //###############################################################
    class EventIndexer : public Selector {
    public:
        EventIndexer(const std::string &Name, const std::string &TreeName, const std::vector<std::string> &Files);

    private:
        virtual bool AnalyzeEvent();
        virtual void SetupReaders();

        virtual void WriteOutput();

        // The cache path is taken from the ROOTFile name with .root replaced by .txt
        std::string m_cache_path;
        std::ofstream m_cache_stream;

        ITreeVarReader *m_event_number;
        ITreeVarReader *m_run_number;
    };
}  // namespace XAMPP
#endif

#ifndef XAMPPplotting_PlottingUtils_IXX
#define XAMPPplotting_PlottingUtils_IXX

#include <TString.h>
#include <XAMPPplotting/PlottingUtils.h>
namespace XAMPP {

    template <typename T> bool IsElementInList(const std::vector<T> &List, const T &Element) {
        for (const auto &Item : List) {
            if (Item == Element) return true;
        }
        return false;
    }
    template <typename T> void CopyVector(const std::vector<T> &From, std::vector<T> &To, std::function<bool(const T &)> func, bool Clear) {
        if (Clear) To.clear();
        if (To.capacity() < From.size()) To.reserve(From.size() + To.capacity());
        for (auto &Ele : From) {
            if (func(Ele)) To.push_back(Ele);
        }
        To.shrink_to_fit();
    }

    template <typename T> void CopyVector(const std::vector<T> &From, std::vector<T> &To, bool Clear) {
        CopyVector<T>(
            From, To, [&To](const T &ele) { return !IsElementInList(To, ele); }, Clear);
    }
    template <typename T> void CopyVector(const std::vector<T> &From, std::set<T> &To, bool Clear) {
        if (Clear) To.clear();
        for (const auto &F : From) To.insert(F);
    }
    template <typename T> void ClearFromDuplicates(std::vector<T> &toClear) {
        std::vector<T> copy = toClear;
        CopyVector(copy, toClear, true);
    }
    template <typename T>
    unsigned int count(typename std::vector<T>::const_iterator begin, typename std::vector<T>::const_iterator end,
                       std::function<bool(const T &)> func) {
        unsigned int n = 0;
        for (; begin != end; ++begin) {
            if (func(*begin)) ++n;
        }
        return n;
    }

    template <typename T> unsigned int count(const std::vector<T> &vector, std::function<bool(const T &)> func) {
        return count<T>(vector.begin(), vector.end(), func);
    }
    template <typename T> unsigned int count_active(const std::vector<std::future<T>> &threads) {
        return count<std::future<T>>(threads, [](const std::future<T> &th) {
            using namespace std::chrono_literals;
            return th.wait_for(0ms) != std::future_status::ready;
        });
    }
    template <typename T> void EraseFromVector(std::vector<T> &vec, std::function<bool(const T &)> func) {
        typename std::vector<T>::iterator itr = std::find_if(vec.begin(), vec.end(), func);
        while (itr != vec.end()) {
            vec.erase(itr);
            itr = std::find_if(vec.begin(), vec.end(), func);
        }
    }
    template <typename T> void RemoveElement(std::vector<T> &Vec, const T &Ele) {
        EraseFromVector<T>(Vec, [&Ele](const T &in_vec) { return Ele == in_vec; });
    }

    template <class T> std::ostream &operator<<(std::ostream &os, const std::vector<T> &v) {
        os << "{";
        for (const auto &i : v) { os << " " << i; }
        os << " }";
        return os;
    }
    template <class T, class U> std::ostream &operator<<(std::ostream &os, const std::pair<T, U> &e) {
        os << "(" << e.first << "," << e.second << ")";
        return os;
    }
    template <class T> bool operator==(const std::vector<T> &v1, const std::vector<T> &v2) {
        if (v1.size() != v2.size()) return false;
        typename std::vector<T>::const_iterator begin_v1 = v1.begin();
        typename std::vector<T>::const_iterator begin_v2 = v2.begin();
        typename std::vector<T>::const_iterator end_v1 = v1.end();
        for (; begin_v1 != end_v1; ++begin_v1, ++begin_v2) {
            if (*begin_v1 != *begin_v2) return false;
        }
        return true;
    }

    template <typename T> int max_bit(const T &number) {
        for (int bit = sizeof(number) * 8 - 1; bit >= 0; --bit) {
            if (number & (1 << bit)) return bit;
        }
        return -1;
    }
    template <typename T> int min_bit(const T &number) {
        for (unsigned int bit = 0; bit <= sizeof(number) * 8 - 1; ++bit) {
            if (number & (1 << bit)) return bit;
        }
        return -1;
    }
    template <class... Args> void Info(const std::string &method, const std::string &info_str, Args &&...args) {
        unsigned int l = method.size();
        std::cout << method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l)
                  << " -- INFO: " << Form(info_str.c_str(), args...) << std::endl;
    }
    template <class... Args> void Warning(const std::string &method, const std::string &info_str, Args &&...args) {
        unsigned int l = method.size();
        std::cout << method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l)
                  << " -- WARNING: " << Form(info_str.c_str(), args...) << std::endl;
    }
    template <class... Args> void Error(const std::string &method, const std::string &info_str, Args &&...args) {
        unsigned int l = method.size();
        std::cerr << method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l)
                  << " -- ERROR: " << Form(info_str.c_str(), args...) << std::endl;
    }
}  // namespace XAMPP
#endif

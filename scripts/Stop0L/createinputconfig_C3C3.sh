#!/bin/bash

###path='/home/iwsatlas1/jgraw/garching/XAMPP/Plots/MitNeuTMVAAlleVar' 
###path='/ptmp/mpp/grjon/Cluster/Output/2017-03-24/Data'
path='/ptmp/mpp/niko/Stop0L/TMVA/170522/'
for item in  `ls ${path}`; do
	if [ "${item/.root/}" == "${item}" ];then
		continue
	fi
	
	FileName="${item}"
    Sample="${item/.root/}"
    
	if [ "${item/TT_directTT/}" != "${item}" ]|| [ "${item/TT_onestepBB/}" != "${item}" ] || [ "${item/TT_mixedBT/}" != "${item}" ];then 
        
        echo "Input ${path}/${item} " >> ~/InputConfigs/InputConfig_"${Sample}".conf   #DSConfig_${item}
   
    else
		echo "Input ${path}/${item} " >> ~/InputConfigs/InputConfig_"${Sample}".conf
    fi
    echo "IsHistFitter" >> ~/InputConfigs/InputConfig_"${Sample}".conf
done

#~ cd -



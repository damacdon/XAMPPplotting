#!/usr/bin/env python
import os
import sys
import commands
import mimetypes
import time
from ClusterSubmission.Utils import *
from XAMPPbase.RucioListBuilder import *
ROOTCOREBIN = os.getenv("ROOTCOREBIN")


def ExtractMasses(ModelPoint):
    try:
        return (int(ModelPoint.split("_")[1]), int(ModelPoint.split("_")[2]))
    except:
        return (int(ModelPoint.split("_")[1]), -1)


def BuildWinoModel(SignalPoints):
    Winos = []
    for C1N2 in SignalPoints:
        if C1N2.startswith("C1C1"):
            continue
        for C1C1 in SignalPoints:
            if not C1C1.startswith("C1C1"):
                continue
            if ExtractMasses(C1C1) != ExtractMasses(C1N2):
                continue
            Winos.append((C1N2, C1C1))
    return Winos


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description="Script for creating Data input configs")
    parser.add_argument('-i', '--input', help='Input list with all datasets on the groupdisk', required=True)
    options = parser.parse_args()

    InputConfigs = os.listdir(options.input)
    for Signal in sorted(InputConfigs):
        if not Signal.startswith("LV"):
            continue
        ModelName = Signal.split("/")[-1].replace(".conf", "").replace("LV", "RHSlep")
        print ModelName
        NewFile = open("%s/%s.conf" % (options.input, ModelName), "w")
        NewFile.write("xSecDir XAMPPplotting/xSecFiles/RH_Slepton\n")
        NewFile.write("Import %s/%s\n" % (options.input.replace("data/", ""), Signal))
        NewFile.write("SampleName %s\n" % (ModelName))
        NewFile.close()

import sys, os, argparse, commands
from CreateCutFlowTables import setupCutFlowDictionary, setupCutFlowParser, ModelFromSignalPoint, ExtractMasses
from pprint import pprint

BASEFOLD_ACC = "/afs/ipp-garching.mpg.de/home/j/junggjo9/RootCore/TruthFrameWork/Out/Acc/"
BASEFOLD_EFF = "/afs/ipp-garching.mpg.de/home/j/junggjo9/RootCore/TruthFrameWork/Out/Eff/"


class SignalPoint(object):
    def __init__(self, FileName):
        self.__Name = FileName.split(".")[1]
        self.__Name = self.name()[self.name().find("_", self.name().find("_") + 1) + 1:]
        if self.__Name.startswith("C1N2N1_"):
            self.__Name = self.__Name[len("C1N2N1_"):]
        self.__DSID = int(FileName.split(".")[0])
        self.__Acceptances = self.__ReadFile("%s/%s.txt" % (BASEFOLD_ACC, FileName))
        self.__Efficiencies = self.__ReadFile("%s/%s.txt" % (BASEFOLD_EFF, FileName))

    def __ReadFile(self, File):
        Grid = {}
        if not os.path.isfile(File):
            print "Dafuq?!"
            return Grid
        for line in open(File):
            line = line.strip().replace("\n", "")
            Region = line.split(",")[0]
            if Region.rfind("_") == -1: continue
            Region = Region[Region.rfind("_") + 1:]
            Grid[Region] = {}
            Grid[Region]["RawEv"] = int(line.split(",")[1])
            Grid[Region]["WeightEv"] = float(line.split(",")[2])
            Grid[Region]["UncertEffi"] = float(line.split(",")[3])
        return Grid

    def name(self):
        return self.__Name

    def model_name(self):
        return ModelFromSignalPoint(self.__Name)

    def dsid(self):
        return self.__DSID

    def RecoRegions(self):
        return [S for S in self.__Efficiencies.iterkeys()]

    def TruthRegions(self):
        return [S for S in self.__Acceptances.iterkeys()]

    def Acceptance(self, Region):
        if Region not in self.TruthRegions():
            print "ERROR: Region %s is unknown" % (Region)
            return 0.
        return self.__Acceptances[Region]["WeightEv"]

    def EffiTimesAcc(self, Region):
        if Region not in self.RecoRegions():
            print "ERROR: Region %s is unknown" % (Region)
            return 0.
        return self.__Efficiencies[Region]["WeightEv"]

    def TruthEvents(self, Region):
        if Region not in self.RecoRegions():
            print "ERROR: Region %s is unknown" % (Region)
            return 0.
        return self.__Acceptances[Region]["RawEv"]

    def RecoEvents(self, Region):
        if Region not in self.RecoRegions():
            print "ERROR: Region %s is unknown" % (Region)
            return 0.
        return self.__Efficiencies[Region]["RawEv"]

    def Efficiency(self, Region):
        return self.EffiTimesAcc(Region) * (1. / self.Acceptance(Region) if self.Acceptance(Region) > 0 else 0)


def FindModel(Regions, Signal):
    for Grid, SignalPoints in Regions.iteritems():
        for Point in SignalPoints.itervalues():
            if Sig.dsid() in Point["DSIDs"]: return Point["Cuts"]
    return None


if __name__ == "__main__":
    options = setupCutFlowParser().parse_args()
    CutFlows_withDSID = setupCutFlowDictionary(options)

    TruthFrame = os.listdir(BASEFOLD_ACC)
    Signals = {}

    for F in TruthFrame:
        if F.endswith(".root"): continue
        Model = F.replace(".txt", "")
        Blub = SignalPoint(Model)
        Signals[Blub.dsid()] = Blub

    RegionTables = {}
    for DSID in sorted(Signals.iterkeys()):
        Sig = Signals[DSID]
        #if "LLE" in Sig.model_name(): continue
        for R in Sig.TruthRegions():
            #           if not R in Sig.RecoRegions(): continue
            #           Str = "%i  &  %s  & %i & %i &  %.3f & %.3f  & %.3f \\\\\n"%(DSID,Sig.name(), Sig.TruthEvents(R), Sig.RecoEvents(R),  Sig.Acceptance(R), Sig.Efficiency(R), Sig.EffiTimesAcc(R))
            #           try: RegionTables[R] += Str
            R_Reco = R
            model_name = Sig.model_name()
            signal_name = Sig.name()
            final_cut = -1
            if R == "SR0A":
                R_Reco = "SR0B"
                final_cut = -2
            elif R == "SR0C":
                R_Reco = "SR0D"
                final_cut = -2

            if not R_Reco in CutFlows_withDSID.iterkeys(): continue
            CutFlow = FindModel(CutFlows_withDSID[R_Reco], Sig)
            if not CutFlow: continue

            Initial_Reco = CutFlow[0][1]
            Final_Reco = CutFlow[final_cut][1]

            if not Sig.model_name() in CutFlows_withDSID[R_Reco].iterkeys(): continue
            Initial_Reco = CutFlows_withDSID[R_Reco][Sig.model_name()][Sig.name()]["Cuts"][0][1]
            Final_Reco = CutFlows_withDSID[R_Reco][Sig.model_name()][Sig.name()]["Cuts"][final_cut][1]

            NLSP, LSP = ExtractMasses(Sig.name())

            Eff_TimesAcc = float(Final_Reco) / float(Initial_Reco)
            Eff = Eff_TimesAcc / (1. if Sig.Acceptance(R) == 0 else Sig.Acceptance(R))
            Str = "%s  & %d & %d & %i & %.3f &  %.3f & %.3f  & %.3f \\\\\n" % (Sig.model_name(), NLSP, LSP, Sig.TruthEvents(R), Final_Reco,
                                                                               Sig.Acceptance(R), Eff, Eff_TimesAcc)
            try:
                RegionTables[R] += Str
            except:
                RegionTables[R] = Str

    for R, Table in RegionTables.iteritems():
        if R == "All": continue
        FinalTable = """
    \\begin{table}
        \\begin{tabular}{l c | c c | c c c }
                \\mulicolumn{3}{c}{} & \\multicolumn{2}{c}{Number of events} & \\multicolumn{3}{c}{Efficiency} \\\\
                model & m_{\text{NLSP}} & m_{\text{LSP}} & truth & reco & truth & reco & reco$\times$ truth \\\\
                %s
        \\end{tabular}
        \\caption{ Number of MC events, truth acceptance and reconstruction efficiencies for each particular signal point passing %s selection criteria. Numbers are extracted from the \href{https://gitlab.cern.ch/atlas-phys-susy-wg/SimpleAnalysis}{SimpleAnalysis} framework.} 
    \\end{table}   
        
        """ % (Table, R)
        newFile = open("%s.tex" % (R), "w")
        newFile.write(FinalTable)
        newFile.close()
        print FinalTable

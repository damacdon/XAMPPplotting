#!/usr/bin/env python

###################################################

# Script for SF validation
#
# Plotting MC and data efficiency curves
# MC curves corrected by SF

###################################################

from argparse import ArgumentParser
parser = ArgumentParser(description='calculate MET trigger efficiencies')
parser.add_argument("-i", "--InputFolder", help="give path to reduced ntuples", default="")
parser.add_argument("-o", "--OutputFolder", help="where output files should be stored", default=".")
parser.add_argument("-t", "--TextFolder", help="directory containing text files with fit output", default=".")
args = parser.parse_args()

import os, math, array, time
from ROOT import *
from TriggerDefs import *

if not os.path.exists(args.OutputFolder):
    os.makedirs(args.OutputFolder)

# use same input file as for SF calculation
rootfile = TFile.Open(args.InputFolder + "/root/efficiencies.root")

# parameters are read in from text files containing fit parameters
fitErf = TF1("myFit1T1_erf", "0.5*[2] * (1 + TMath::Erf( (x-[0]) / (sqrt(2. * [1] * [1])) ) ) + [3]", 100., 300.)

# activate for all histos the storage of the sum of squares of errors
TH1.SetDefaultSumw2()

c = {}
h_data = {}
h_MC = {}
h_MC_SF = {}
txtFiles = {}

for trig in trigger:
    c[trig] = TCanvas("c", "c", 10, 32, 668, 643)
    pad1 = TPad("pad1", "pad1", .001, .185, .995, .995)
    pad2 = TPad("pad1", "pad2", .001, .001, .995, .3)
    pad1.Draw()
    pad2.Draw()
    pad1.SetBottomMargin(0.15)
    pad1.SetTopMargin(0.1)
    pad1.cd()
    h_MC[trig] = rootfile.Get("h_trig_MC_" + triggerNames[trig])
    h_data[trig] = rootfile.Get("h_trig_data_" + triggerNames[trig])
    h_MC_SF[trig] = h_MC[trig].Clone("h_trig_MC_SF_" + triggerNames[trig])
    txtFiles[trig] = open(args.TextFolder + "/" + triggerNames[trig] + "_nominal_MonoH_Rel21.txt", "r")

    # fit parameters written in first line of text file
    fitParameters = txtFiles[trig].readlines()[0].split()

    ##################################################
    # Applying MET dependent SF on MC efficiency curve
    ##################################################

    # Fit parameters in text file stored in this order:
    # Parameter(0) Parameter(1) ParError(0) ParError(1) covMatrix(0,0) covMatrix(0,1) covMatrix(1,0) covMatrix(1,1) chi2 NDF
    par0 = fitErf.SetParameter(0, float(fitParameters[0]))
    par1 = fitErf.SetParameter(1, float(fitParameters[1]))
    par2 = fitErf.SetParameter(2, float(fitParameters[2]))
    par3 = fitErf.SetParameter(3, float(fitParameters[3]))

    # check how many bins the histogram has
    for i in range(0, h_MC_SF[trig].GetXaxis().GetNbins()):
        binXValue = h_MC[trig].GetBinCenter(i)
        # ScaleFactor (MET) = value of fitErf for certain MET
        SF = fitErf.Eval(binXValue)
        # rescale old MC histogram with SF
        newBinContent = h_MC[trig].GetBinContent(i) * SF
        h_MC_SF[trig].SetBinContent(i, newBinContent)

    ##########################
    # make the efficiency plots
    ##########################

    # use same plotting style as in TriggerEfficiencies.py

    h_data[trig].SetMarkerColor(kBlack)
    h_data[trig].SetLineColor(kBlack)
    h_data[trig].GetYaxis().SetTitle("Efficiency")
    h_data[trig].GetYaxis().SetLabelSize(0.04)
    h_data[trig].GetYaxis().SetRangeUser(-0.02, 1.05)
    h_data[trig].SetMarkerStyle(20)
    h_data[trig].SetMarkerSize(1)
    h_data[trig].SetLineWidth(2)
    h_data[trig].Draw()
    h_MC_SF[trig].SetMarkerColor(kBlue)
    h_MC_SF[trig].SetLineColor(kBlue)
    h_MC_SF[trig].SetMarkerStyle(1)
    h_MC_SF[trig].SetLineWidth(2)
    h_MC_SF[trig].Draw("hist same")
    line = TLine(150, 0, 150, 1.05)
    line.SetLineStyle(2)
    line.SetLineWidth(2)
    line.Draw("same")
    SetAtlasLabel()
    SetLumiLabel(trig)
    SetRegionLabel(trig)
    SetPeriodLabel(trig)
    legend = SetLegend(h_MC_SF[trig], h_data[trig])
    legend.Draw("same")
    pad2.SetTopMargin(0.)
    pad2.SetBottomMargin(0.47)
    pad2.cd()
    h_ratio = h_data[trig].Clone("h_ratio")
    h_ratio.Divide(h_ratio, h_MC_SF[trig])
    h_ratio.GetXaxis().SetTitle("E_{T}^{miss}, #mu invis. [GeV] ")
    h_ratio.GetXaxis().SetTitleFont(42)
    h_ratio.GetXaxis().SetTitleSize(0.14)
    h_ratio.GetXaxis().SetLabelSize(0.13)
    h_ratio.GetYaxis().SetLabelSize(0.09)
    h_ratio.GetYaxis().SetRangeUser(0.5, 1.15)
    h_ratio.GetYaxis().SetTitle("Data / MC")
    h_ratio.GetYaxis().SetTitleSize(0.11)
    h_ratio.GetYaxis().SetTitleOffset(0.5)
    h_ratio.GetYaxis().SetNdivisions(6)
    h_ratio.SetLineWidth(1)
    h_ratio.Draw("P")
    lineAtOne = TLine(50, 1, 300, 1)
    lineAtOne.SetLineStyle(2)
    lineAtOne.Draw("same")
    lineAt150 = TLine(150, 0.5, 150, 1.15)
    lineAt150.SetLineStyle(2)
    lineAt150.SetLineWidth(2)
    lineAt150.Draw("same")
    # save plots
    c[trig].Print(args.OutputFolder + "/validation_%s.pdf" % (triggerNames[trig]))

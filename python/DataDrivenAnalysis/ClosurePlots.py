#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
from XAMPPplotting.FileUtils import CreateDirectory, ReadListFromFile
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
import XAMPPplotting.FileStructureHandler
from XAMPPplotting.YieldsHandler import *


def drawHisto(Options, analysis, reweighted, raw, var, bonusstr=""):
    HistList = []
    HistToDraw = []
    ratios = []

    FullHistoSetRaw = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, analysis, raw, var, UseData=False)
    FullHistoSetWeighted = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, analysis, reweighted, var, UseData=False)

    if not FullHistoSetRaw or not FullHistoSetRaw[0].CheckHistoSet():
        print 'WARNING: Cannot draw stacked background MC histogram without background samples, skipping...'
        return False

    if not FullHistoSetWeighted or not FullHistoSetWeighted[0].CheckHistoSet():
        print 'WARNING: Cannot draw stacked background MC histogram without background samples, skipping...'
        return False

    DefaultSetWeighted = FullHistoSetWeighted[0]
    DefaultSetRaw = FullHistoSetRaw[0]

    pu = PlotUtils(status=Options.label, size=24, lumi=DefaultSet.GetLumi())

    logstr = "" if not Options.doLogY else "_LogY"

    if DefaultSet.isTH2(): return False

    region = "%s_%s" % (reweighted, raw)
    pu.Prepare2PadCanvas("Closure_%s_%s_%s%s%s" % (var, Options.nominalName, region, bonusstr, logstr), 800, 600, Options.quadCanvas)
    pu.GetTopPad().cd()
    if Options.doLogY: pu.GetTopPad().SetLogy()

    pu.CreateLegend(Options.LegendXCoords[0], Options.LegendYCoords[0], Options.LegendXCoords[1], Options.LegendYCoords[1])

    # The sample to which are the ratios made
    RatioSmp = None
    if FullHistoSetWeighted.GetSummedBackground().isValid():
        RatioSmp = FullHistoSetWeighted.GetSummedBackground()
    else:
        print "WARNING: No valid summed background found. Will pick one random signal"
        return False

    FullHistoSetWeighted.GetSummedBackground().SetTitle("reweighted MC")
    FullHistoSetRaw.GetSummedBackground().SetTitle("raw MC")

    HistList += [FullHistoSetWeighted.GetSummedBackground()]
    HistList += [FullHistoSetRaw.GetSummedBackground()]

    ymin, ymax = pu.GetFancyAxisRanges(HistList, Options)
    pu.drawStyling(HistList[0], ymin, ymax, RemoveLabel=not Options.noRatio, TopPad=not Options.noRatio)

    for H in HistToDraw:
        H.SetLineWidth(2)
        H.Draw("histSAME")

    pu.DrawLegend(NperCol=Options.EntriesPerLegendColumn)

    pu.DrawPlotLabels(0.195, 0.83, Options.regionLabel if len(Options.regionLabel) > 0 else "Closure test", analysis, Options.noATLAS)

    pu.GetTopPad().RedrawAxis()
    pu.GetBottomPad().cd()
    pu.GetBottomPad().SetGridy()

    RatioLabel = "Closure"
    if Options.RatioLabel != '': RatioLabel = Options.RatioLabel

    ymin, ymax = pu.GetFancyAxisRanges(ratios, Options, doRatio=True)

    pu.drawRatioStyling(RatioSmp, ymin, ymax, RatioLabel)

    pu.drawRatioErrors(RatioSmp, not Options.noSyst and len(RatioSmp.GetOriginalSystHistos()) > 0, not Options.noRatioSysLegend)

    ratios += [XAMPPplotting.Utils.CreateRatioHisto(FullHistoSetWeighted.GetSummedBackground(), FullHistoSetRaw.GetSummedBackground())]
    for r in ratios:
        r.Draw("sameHist")

    ROOT.gPad.RedrawAxis()

    PreString = region
    if analysis != "": PreString = analysis + "_" + region
    pu.saveHisto("%s/Closure_%s_%s%s" % (Options.outputDir, PreString, var, logstr), Options.OutFileType)
    pu.DrawTLatex(0.5, 0.97, "%s%s in %s (%s analysis)" % (var, logstr, region, analysis), 0.04, 52, 22)

    pu.GetCanvas().SaveAs("%s/AllClosurePlots%s.pdf" % (Options.outputDir, bonusstr))
    return True


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='This script produces MC only histograms from tree files. For more help type \"python MCPlots.py -h\"',
        prog='MCPlots',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = XAMPPplotting.Utils.setupBaseParser(parser)
    parser.add_argument('--RatioSmp', help='Specify the sample label for the ratio to be drawn', default='')
    parser.add_argument('--RatioLabel', help='Specify a y-axis label for the ration panel', default='')
    ### Simple file containing the closure region vs. truth estimate
    ###
    ### <FF weighted region> <FF MC region>
    ###
    ###
    parser.add_argument("--ClosureFile", help="The txt file containing the pairing of the closure region combination", required=True)
    PlottingOptions = parser.parse_args()
    CreateDirectory(PlottingOptions.outputDir, False)
    if not os.path.exists(PlottingOptions.ClosureFile):
        print "ERROR: The closure file does not exist"
        exit(1)
    Closure_Content = ReadListFromFile(PlottingOptions.ClosureFile)
    Closure_Regions = []
    for line in Closure_Content:
        Reweighted = line.split()[0]
        RawMC = line.split()[1]
        Closure_Regions += [(Reweighted, RawMC)]

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(PlottingOptions)

    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)

    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for reweighted, raw in Closure_Regions:
            if reweighted not in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
                print "WARNINIG: %s is not in your histo file" % (reweighted)
                continue
            if raw not in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
                print "WARNING: %s is not in your histo files... Some strange things you want to have" % (raw)
                continue

            bonusstr = '_%s_%s_%s' % (ana, reweighted, raw)
            if PlottingOptions.doLogY: bonusstr += '_LogY'
            bonusstr = XAMPPplotting.Utils.RemoveSpecialChars(bonusstr)

            dummy.SaveAs("%s/AllClosurePlots%s.pdf[" % (PlottingOptions.outputDir, bonusstr))
            Drawn = False
            for var in FileStructure.GetConfigSet().GetVariables(ana, reweighted):
                Drawn = drawHisto(PlottingOptions, ana, reweighted, raw, var, bonusstr=bonusstr) or Drawn

            if Drawn:
                dummy.SaveAs("%s/AllClosurelots%s.pdf]" % (PlottingOptions.outputDir, bonusstr))
            else:
                os.system("rm %s/AllClosurePlots%s.pdf" % (PlottingOptions.outputDir, bonusstr))

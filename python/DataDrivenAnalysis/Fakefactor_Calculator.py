import ROOT
import sys
import argparse
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
import Common as com
import math
import os
from array import array
from ClusterSubmission.Utils import CreateDirectory


# Create an additional bin that contains the overflow
# meant for pt distribution
def ExtendAxis(Histo, histoName, high_bin=1e25):
    xbinedges = []

    for xi in range(Histo.GetNbinsX() + 1):
        xbinedges.append(Histo.GetXaxis().GetBinLowEdge(xi + 1))
    if Histo.GetDimension() >= 2:
        ybinedges = []
        for yi in range(Histo.GetNbinsY() + 1):
            ybinedges.append(Histo.GetYaxis().GetBinLowEdge(yi + 1))
    if Histo.GetDimension() == 3:
        zbinedges = []
        for zi in range(Histo.GetNbinsZ() + 1):
            zbinedges.append(Histo.GetZaxis().GetBinLowEdge(zi + 1))
    if xbinedges[-1] < high_bin:
        xbinedges.append(high_bin)

    if Histo.GetDimension() == 1:
        extended_histo = ROOT.TH1F("Extended_{}".format(histoName), "", len(xbinedges) - 1, array("d", xbinedges))
    if Histo.GetDimension() == 2:
        extended_histo = ROOT.TH2F("Extended_{}".format(histoName), "",
                                   len(xbinedges) - 1, array("d", xbinedges),
                                   len(ybinedges) - 1, array("d", ybinedges))
    if Histo.GetDimension() == 3:
        extended_histo = ROOT.TH3F("Extended_{}".format(histoName), "",
                                   len(xbinedges) - 1, array("d", xbinedges),
                                   len(ybinedges) - 1, array("d", ybinedges),
                                   len(zbinedges) - 1, array("d", zbinedges))

    extended_histo.GetXaxis().SetTitle(Histo.GetXaxis().GetTitle())
    if Histo.GetDimension() >= 2:
        extended_histo.GetYaxis().SetTitle(Histo.GetYaxis().GetTitle())
    if Histo.GetDimension() == 3:
        extended_histo.GetZaxis().SetTitle(Histo.GetZaxis().GetTitle())

    if Histo.GetDimension() == 1:
        for b in range(GetNbins(Histo) + 2):
            extended_histo.SetBinContent(b, Histo.GetBinContent(b))
            extended_histo.SetBinError(b, Histo.GetBinError(b))
        extended_histo.SetEntries(Histo.GetEntries())
    if Histo.GetDimension() == 2:
        for xi in range(Histo.GetNbinsX() + 2):
            for yi in range(Histo.GetNbinsY() + 2):
                extended_histo.SetBinContent(xi, yi, Histo.GetBinContent(xi, yi))
                extended_histo.SetBinError(xi, yi, Histo.GetBinError(xi, yi))
        extended_histo.SetEntries(Histo.GetEntries())
    return extended_histo


class FakeHistogram(object):
    def __init__(self, analysis, region, var):
        self.analysis = analysis
        self.region = region
        self.var = var
        self.Particle = var.split("_")[0]
        self.prong = ""
        if var.split("_")[0] == "Tau":
            self.prong = var.split("_")[-2].replace("prong", "")
        self.Definition = var.split("_")[1].replace("def", "")
        self.FakeType = var.split("_")[2].replace("orig", "")
        self.DiscrVar = var.split("_")[-1]
        self.ProjectTo1D = False  # to create plots of met meff or mu
        if self.DiscrVar == "ptmeff" or self.DiscrVar == "ptmet" or self.DiscrVar == "ptmu":
            self.DiscrVar = self.DiscrVar.replace("pt", "")

            self.ProjectTo1D = True
        # ~ self.DiscrVar = var.split("_")[-1].replace("pt", "") # assuming that the discriminating variable is something like ptmeff, ptmet etc.


class Fakefactor(object):
    def __init__(self, Options, LooseHisto, SignalHisto, Store=False, Plot=False, Projection=False, ProjectToXaxis=False):
        self.__Options = Options
        self.LooseVar = LooseHisto.var
        self.SignalVar = SignalHisto.var
        self.analysis = LooseHisto.analysis
        self.region = LooseHisto.region
        self.Particle = LooseHisto.Particle
        self.prong = LooseHisto.prong
        self.FakeType = LooseHisto.FakeType
        self.DiscrVar = LooseHisto.DiscrVar

        self.NoHistos = False
        self.is2D = False

        self.Store = Store
        self.Plot = Plot
        self.Projection = Projection
        self.ProjectToXaxis = ProjectToXaxis

        if not self.Store and not self.Plot:
            print "Error: specified to neither plot and store fakefactors. Returning"
            return

        print "Calculate Fakefactor for {} {} in region {} for variable {}".format(self.FakeType, self.Particle, self.region, self.DiscrVar)

        if self.Plot:
            self.LooseHistogramsToPlot = self.RetrieveHistograms(self.LooseVar, PlotOverFlowBin=self.__Options.PlotOverFlowBin)
            self.SignalHistogramsToPlot = self.RetrieveHistograms(self.SignalVar, PlotOverFlowBin=self.__Options.PlotOverFlowBin)
        if self.Store:
            if self.__Options.ExtendAxis:
                self.LooseHistogramsToStore = self.RetrieveHistograms(self.LooseVar, Store=True)
                self.SignalHistogramsToStore = self.RetrieveHistograms(self.SignalVar, Store=True)
            else:
                self.LooseHistogramsToStore = self.RetrieveHistograms(self.LooseVar)
                self.SignalHistogramsToStore = self.RetrieveHistograms(self.SignalVar)

        if not self.NoHistos:
            if self.Plot and not self.Projection:
                self.FakeFactorHistosToPlot = self.CalculateFakeFactor(self.LooseHistogramsToPlot, self.SignalHistogramsToPlot)
                self.PlotHisto(self.LooseHistogramsToPlot, "Loose")
                self.PlotHisto(self.SignalHistogramsToPlot, "Signal")
                self.PlotHisto(self.FakeFactorHistosToPlot)

            if self.Store and not self.Projection:
                self.FakeFactorHistosToStore = self.CalculateFakeFactor(self.LooseHistogramsToStore, self.SignalHistogramsToStore)

            if self.Projection and self.Plot:
                self.ProjectedLooseHistogramsToPlot = self.ProjectTo1DHisto(self.LooseHistogramsToPlot, "Loose")
                self.ProjectedSignalHistogramsToPlot = self.ProjectTo1DHisto(self.SignalHistogramsToPlot, "Signal")
                self.ProjectedFakeFactorHistosToPlot = self.CalculateFakeFactor(self.ProjectedLooseHistogramsToPlot,
                                                                                self.ProjectedSignalHistogramsToPlot)
                self.PlotHisto(self.ProjectedFakeFactorHistosToPlot)
            if self.Projection and self.Store:
                self.ProjectedLooseHistogramsToStore = self.ProjectTo1DHisto(self.LooseHistogramsToStore, "Loose")
                self.ProjectedSignalHistogramsToStore = self.ProjectTo1DHisto(self.SignalHistogramsToStore, "Signal")
                self.ProjectedFakeFactorHistosToStore = self.CalculateFakeFactor(self.ProjectedLooseHistogramsToStore,
                                                                                 self.ProjectedSignalHistogramsToStore)

    def RetrieveHistograms(self, var, Store=False, PlotOverFlowBin=False):
        HistoSets = XAMPPplotting.PlottingHistos.CreateHistoSets(self.__Options, self.analysis, self.region, var, False)
        if not HistoSets:
            print "Histos are missing"
            self.NoHistos = True
            return
        HistoSet = HistoSets[0]
        if HistoSet.GetSummedBackground().isTH2():
            self.is2D = True

        histos = HistoSet.GetBackgrounds()
        Histograms = {}
        for histo in histos:
            process = histo.GetName()

            Histo = histo.GetHistogram().Clone()

            if (Store or PlotOverFlowBin) and self.DiscrVar.startswith("pt"):
                histoName = Histo.GetName()
                histoName += "_" + process
                if Store:
                    ExtendedHisto = ExtendAxis(Histo, histoName, self.__Options.ExtendedBinEdge)
                if not Store and PlotOverFlowBin:
                    maxBin = Histo.GetXaxis().GetBinLowEdge(Histo.GetNbinsX() + 1)
                    ExtendedHisto = ExtendAxis(Histo, histoName, maxBin + 50)
                Histograms[process] = ExtendedHisto
            else:
                Histograms[process] = Histo
        if len(Histograms) == 0:
            print "Histos are missing"
            self.NoHistos = True
        return Histograms

    def ProjectTo1DHisto(self, Histograms_2D, definition):
        Histos1D = {}
        if not self.is2D:
            print "FakeFactor.ProjectTo1DHisto() Warning: histogram is not 2D"
            return Histos1D
        for process, Histo2D in Histograms_2D.items():

            if self.ProjectToXaxis:
                Histo1D = Histo2D.ProjectionX(
                    "1DProjection_{}_{}_{}{}_{}_{}_{}".format(self.region, process, definition, self.Particle, self.prong, self.FakeType,
                                                              self.DiscrVar), 0, -1, "e")
            else:
                Histo1D = Histo2D.ProjectionY(
                    "1DProjection_{}_{}_{}{}_{}_{}_{}".format(self.region, process, definition, self.Particle, self.prong, self.FakeType,
                                                              self.DiscrVar), 0, -1, "e")

            Histos1D[process] = Histo1D

        return Histos1D

    def CalculateFakeFactor(self, LooseHistos, SignalHistos):
        FakeFactorHistos = {}

        for process, SignalHisto in SignalHistos.items():
            if process in LooseHistos:
                FakeFactorHisto = XAMPPplotting.Utils.CreateRatioHisto(SignalHisto, LooseHistos[process])

                FakeFactorHistos[process] = FakeFactorHisto

        return FakeFactorHistos

    def PlotHisto(self, FakeFactorHistos, definition=""):
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptTitle(0)

        if definition == "Loose":
            CreateDirectory("{}/{}_{}/{}/".format(self.__Options.outputDir, self.FakeType, self.Particle, definition), False)
            OutputFolder = "{}/{}_{}/{}/".format(self.__Options.outputDir, self.FakeType, self.Particle, definition)
        elif definition == "Signal":
            CreateDirectory("{}/{}_{}/{}/".format(self.__Options.outputDir, self.FakeType, self.Particle, definition), False)
            OutputFolder = "{}/{}_{}/{}/".format(self.__Options.outputDir, self.FakeType, self.Particle, definition)
        else:
            CreateDirectory("{}/{}_{}".format(self.__Options.outputDir, self.FakeType, self.Particle), False)
            OutputFolder = "{}/{}_{}".format(self.__Options.outputDir, self.FakeType, self.Particle)

        for process, histo in FakeFactorHistos.items():
            pu = PlotUtils(status=self.__Options.label, size=24)
            if definition == "Loose":
                CanvasName = "Fakefactor_{}_{}_{}{}_{}_{}_{}".format(self.region, process, definition, self.Particle, self.prong,
                                                                     self.FakeType, self.DiscrVar)

            elif definition == "Signal":
                CanvasName = "Fakefactor_{}_{}_{}{}_{}_{}_{}".format(self.region, process, definition, self.Particle, self.prong,
                                                                     self.FakeType, self.DiscrVar)
            else:
                if self.Particle == "Tau":
                    CanvasName = "{}_{}_{}_{}_{}_{}".format(self.region, process, self.Particle, self.prong, self.FakeType, self.DiscrVar)
                else:
                    CanvasName = "{}_{}_{}_{}_{}".format(self.region, process, self.Particle, self.FakeType, self.DiscrVar)
            pu.Prepare1PadCanvas(CanvasName, 800, 600, self.__Options.quadCanvas)

            can = pu.GetCanvas()
            can.SetBottomMargin(0.15)
            histo.SetFillStyle(0)
            histo.SetLineColor(1)
            histo.SetLineWidth(2)
            histo.SetLineStyle(1)
            histo.SetMarkerColor(1)
            if self.is2D:
                histo.GetZaxis().SetTitle("FakeFactor")
            else:
                histo.GetYaxis().SetTitle("FakeFactor")
                histo.GetYaxis().SetRangeUser(0, 1.8 * histo.GetMaximum())

            # ~ XaxisTitle = histo.GetXaxis().GetTitle()
            # ~ histo.GetXaxis().SetTitle(XaxisTitle)
            # ~ if self.FakeType == "Gluon":
            # ~ print "MaxY Fakefactor: ", histo.GetYaxis().GetBinLowEdge(histo.GetNbinsY()+1)
            if not self.is2D:
                histo.Draw("e hist")
            else:
                histo.Draw("colz")

            pu.DrawAtlas(0.195, 0.83)

            pu.saveHisto("{}/{}".format(OutputFolder, CanvasName), self.__Options.OutFileType)


class Scalefactor(object):
    def __init__(self, Options, LooseHistos, SignalHistos, Store=False, Plot=False, Projection=False, ProjectToXaxis=False):
        self.__Options = Options

        self.LooseHistos = LooseHistos
        self.SignalHistos = SignalHistos

        self.HistosMissing = False

        self.analysis = LooseHistos[0].analysis
        self.region = LooseHistos[0].region
        self.Particle = LooseHistos[0].Particle
        self.prong = LooseHistos[0].prong
        self.DiscrVar = LooseHistos[0].DiscrVar

        self.FakeType = ""

        self.is2D = False

        self.lumi = None

        self.Store = Store
        self.Plot = Plot
        self.Projection = Projection
        self.ProjectToXaxis = ProjectToXaxis

        if not self.Store and not self.Plot:
            print "Error: specified to neither plot and store fakefactors. Returning"
            return

        if self.Plot:
            self.LooseBkgHistogramsToPlot = self.RetrieveBkgHistograms(self.LooseHistos, PlotOverFlowBin=self.__Options.PlotOverFlowBin)
            self.SignalBkgHistogramsToPlot = self.RetrieveBkgHistograms(self.SignalHistos, PlotOverFlowBin=self.__Options.PlotOverFlowBin)
            self.LooseDataHistogramsToPlot = self.RetrieveDataHistograms(self.LooseHistos, PlotOverFlowBin=self.__Options.PlotOverFlowBin)
            self.SignalDataHistogramsToPlot = self.RetrieveDataHistograms(self.SignalHistos, PlotOverFlowBin=self.__Options.PlotOverFlowBin)

        if self.Store:
            if self.__Options.ExtendAxis:
                self.LooseBkgHistogramsToStore = self.RetrieveBkgHistograms(self.LooseHistos, Store=True)
                self.SignalBkgHistogramsToStore = self.RetrieveBkgHistograms(self.SignalHistos, Store=True)
                self.LooseDataHistogramsToStore = self.RetrieveDataHistograms(self.LooseHistos, Store=True)
                self.SignalDataHistogramsToStore = self.RetrieveDataHistograms(self.SignalHistos, Store=True)
            else:
                self.LooseBkgHistogramsToStore = self.RetrieveBkgHistograms(self.LooseHistos)
                self.SignalBkgHistogramsToStore = self.RetrieveBkgHistograms(self.SignalHistos)
                self.LooseDataHistogramsToStore = self.RetrieveDataHistograms(self.LooseHistos)
                self.SignalDataHistogramsToStore = self.RetrieveDataHistograms(self.SignalHistos)

        if not self.HistosMissing:
            if self.Plot and not self.Projection:
                self.CorrectedLooseDataHistogramsToPlot = self.SubstractOtherFakeTypes(self.LooseDataHistogramsToPlot,
                                                                                       self.LooseBkgHistogramsToPlot)
                self.CorrectedSignalDataHistogramsToPlot = self.SubstractOtherFakeTypes(self.SignalDataHistogramsToPlot,
                                                                                        self.SignalBkgHistogramsToPlot)
                self.BkgFakeFactorHistosToPlot = self.CalculateFakeFactor(self.LooseBkgHistogramsToPlot, self.SignalBkgHistogramsToPlot,
                                                                          False)
                self.DataFakeFactorHistosToPlot = self.CalculateFakeFactor(self.CorrectedLooseDataHistogramsToPlot,
                                                                           self.CorrectedSignalDataHistogramsToPlot, True)
                self.ScaleFactorHistoToPlot = self.CalculateScaleFactor(self.DataFakeFactorHistosToPlot, self.BkgFakeFactorHistosToPlot)
                if not self.FakeType == '':
                    self.PlotInputDistribtion(self.CorrectedLooseDataHistogramsToPlot, self.LooseBkgHistogramsToPlot, "Loose")
                    self.PlotInputDistribtion(self.CorrectedSignalDataHistogramsToPlot, self.SignalBkgHistogramsToPlot, "Signal")
                    self.PlotScalefactor(self.DataFakeFactorHistosToPlot, self.BkgFakeFactorHistosToPlot, self.ScaleFactorHistoToPlot)

            if self.Store and not self.Projection:
                self.CorrectedLooseDataHistogramsToStore = self.SubstractOtherFakeTypes(self.LooseDataHistogramsToStore,
                                                                                        self.LooseBkgHistogramsToStore)
                self.CorrectedSignalDataHistogramsToStore = self.SubstractOtherFakeTypes(self.SignalDataHistogramsToStore,
                                                                                         self.SignalBkgHistogramsToStore)
                self.BkgFakeFactorHistosToStore = self.CalculateFakeFactor(self.LooseBkgHistogramsToStore, self.SignalBkgHistogramsToStore,
                                                                           False)
                self.DataFakeFactorHistosToStore = self.CalculateFakeFactor(self.CorrectedLooseDataHistogramsToStore,
                                                                            self.CorrectedSignalDataHistogramsToStore, True)
                self.ScaleFactorHistoToStore = self.CalculateScaleFactor(self.DataFakeFactorHistosToStore, self.BkgFakeFactorHistosToStore)

            # ~ if self.PlotProjection and self.Plot:

            # ~ self.ProjectedLooseBkgHistogramsToPlot = self.ProjectTo1DHisto(self.LooseBkgHistogramsToPlot, "Loose")
            # ~ self.ProjectedSignalBkgHistogramsToPlot = self.ProjectTo1DHisto(self.SignalBkgHistogramsToPlot, "Signal")
            # ~ self.ProjectedLooseDataHistogramsToPlot = self.ProjectTo1DHisto(self.LooseDataHistogramsToPlot, "Loose")
            # ~ self.ProjectedSignalDataHistogramsToPlot = self.ProjectTo1DHisto(self.SignalDataHistogramsToPlot, "Signal")

            # ~ if self.PlotProjection and self.Store:
            # ~ self.ProjectedLooseHistograms = self.ProjectTo1DHisto(self.LooseHistograms, "Loose")
            # ~ self.ProjectedSignalHistograms = self.ProjectTo1DHisto(self.SignalHistograms, "Signal")
            # ~ self.ProjectedFakeFactorHistos = self.CalculateFakeFactor(self.ProjectedLooseHistograms, self.ProjectedSignalHistograms )

    def RetrieveBkgHistograms(self, Fakehistos, Store=False, PlotOverFlowBin=False):
        FakeHistograms = {}
        for fakehisto in Fakehistos:

            if not (fakehisto.FakeType == "Unmatched" or fakehisto.FakeType == "KnownUnmatched" or fakehisto.FakeType == "LF"
                    or fakehisto.FakeType == "HF" or fakehisto.FakeType == "Gluon" or fakehisto.FakeType == "Elec" or
                    (fakehisto.FakeType == "Conv" and fakehisto.Particle == "Electrons")):
                # ~ if fakehisto.FakeType == "Incl" or fakehisto.FakeType == "Fake":
                continue

            HistoSets = XAMPPplotting.PlottingHistos.CreateHistoSets(self.__Options, fakehisto.analysis, fakehisto.region, fakehisto.var,
                                                                     False)
            if not HistoSets:
                print "MC Histos are missing"
                self.HistosMissing = True
                return
            HistoSet = HistoSets[0]
            if HistoSet.GetSummedBackground().isTH2():
                self.is2D = True
            Histo = HistoSet.GetSummedBackground().GetHistogram().Clone()

            if (Store or PlotOverFlowBin) and self.DiscrVar.startswith("pt"):
                histoName = Histo.GetName()
                histoName += "MC_{}".format(self.region)
                if Store:
                    ExtendedHisto = ExtendAxis(Histo, histoName, self.__Options.ExtendedBinEdge)
                if not Store and PlotOverFlowBin:
                    maxBin = Histo.GetXaxis().GetBinLowEdge(Histo.GetNbinsX() + 1)
                    ExtendedHisto = ExtendAxis(Histo, histoName, maxBin + 50)
                FakeHistograms[fakehisto.FakeType] = ExtendedHisto
            else:
                FakeHistograms[fakehisto.FakeType] = Histo

        if len(FakeHistograms) == 0:
            print "MC Histos are missing"
            self.HistosMissing = True
        return FakeHistograms

    def RetrieveDataHistograms(self, Fakehistos, Store=False, PlotOverFlowBin=False):
        DataHisto = None
        for fakehisto in Fakehistos:
            if not fakehisto.FakeType == "Incl":
                continue
            HistoSets = XAMPPplotting.PlottingHistos.CreateHistoSets(self.__Options, fakehisto.analysis, fakehisto.region, fakehisto.var,
                                                                     True)
            if not HistoSets:
                return
            HistoSet = HistoSets[0]
            if not self.lumi:
                self.lumi = HistoSet.GetLumi()

            histos = HistoSet.GetData()

            for histo in histos:
                if not DataHisto:
                    DataHisto = histo.GetHistogram().Clone()
                else:
                    DataHisto.Add(histo.GetHistogram())
        if not DataHisto:
            print "Data Histos are missing"
            self.HistosMissing = True

        if (Store or PlotOverFlowBin) and self.DiscrVar.startswith("pt"):
            histoName = DataHisto.GetName()
            histoName += "Data_{}".format(self.region)
            if Store:
                ExtendedHisto = ExtendAxis(DataHisto, histoName, self.__Options.ExtendedBinEdge)
            if PlotOverFlowBin:
                maxBin = DataHisto.GetXaxis().GetBinLowEdge(DataHisto.GetNbinsX() + 1)
                ExtendedHisto = ExtendAxis(DataHisto, histoName, maxBin + 50)
            return ExtendedHisto
        return DataHisto

    def SubstractOtherFakeTypes(self, DataHisto, MCHistos):
        # substracts the contribution of other faketypes from data
        CorrectedDataHisto = DataHisto.Clone()

        for faketype, histo in MCHistos.items():

            if (self.region == "CRt#bar{t}" and faketype == "HF") or (self.region == "Z#mu#mu" and faketype == "LF") or faketype == "Incl":

                continue
            CorrectedDataHisto.Add(histo, -self.lumi)

        return CorrectedDataHisto

    def CalculateFakeFactor(self, LooseHistos, SignalHistos, isData=False):

        FakeFactorHistos = {}
        if not isData:

            for faketype, SignalHisto in SignalHistos.items():
                if faketype in LooseHistos:

                    FakeFactorHistos[faketype] = XAMPPplotting.Utils.CreateRatioHisto(SignalHisto, LooseHistos[faketype])

            return FakeFactorHistos
        else:
            # no faketypes for data
            DataFakeFactorHisto = XAMPPplotting.Utils.CreateRatioHisto(SignalHistos, LooseHistos)
        return DataFakeFactorHisto

    def CalculateScaleFactor(self, DataHistogram, MCHistograms):

        if self.region == "CRt#bar{t}":
            self.FakeType = "HF"
            ScalefactorHisto = XAMPPplotting.Utils.CreateRatioHisto(DataHistogram, MCHistograms["HF"])
        elif self.region == "Z#mu#mu":
            self.FakeType = "LF"
            ScalefactorHisto = XAMPPplotting.Utils.CreateRatioHisto(DataHistogram, MCHistograms["LF"])
        else:
            return None

        return ScalefactorHisto

    def PlotInputDistribtion(self, DataDistr, MCDistr, Definition):

        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptTitle(0)
        if not DataDistr or not MCDistr:
            return

        if not self.is2D:

            CreateDirectory("{}/{}_{}/{}/".format(self.__Options.outputDir, self.FakeType, self.Particle, Definition), False)
            OutputFolder = "{}/{}_{}/{}/".format(self.__Options.outputDir, self.FakeType, self.Particle, Definition)

            pu = PlotUtils(status=self.__Options.label, size=24, lumi=self.lumi)
            if self.Particle == "Tau":
                CanvasName = "Scalefactor_{}_{}{}_{}_{}_{}".format(self.region, Definition, self.Particle, self.prong, self.FakeType,
                                                                   self.DiscrVar)
            else:
                CanvasName = "Scalefactor_{}_{}{}_{}_{}".format(self.region, Definition, self.Particle, self.FakeType, self.DiscrVar)

            if MCDistr[self.FakeType].GetEntries() == 0:
                print "Warning: MC Histogram is empty for Scalefactorcalculation"
                return

            if DataDistr.GetEntries() == 0:
                print "Warning: Data Histogram is empty for Scalefactorcalculation"
                return
            pu.Prepare2PadCanvas(CanvasName, 800, 600)
            pu.GetTopPad().cd()
            legend_x1 = 0.7
            legend_x2 = 0.9
            legend_y1 = 0.7
            legend_y2 = 0.85
            MCHist = MCDistr[self.FakeType].Clone()

            MCHist.Scale(self.lumi)
            legend = ROOT.TLegend(legend_x1, legend_y1, legend_x2, legend_y2)
            legend.AddEntry(MCHist, "MC", "l")
            legend.AddEntry(DataDistr, "Data", "lp")

            MCHist.SetFillStyle(0)
            MCHist.SetLineColor(2)
            MCHist.SetLineWidth(2)
            MCHist.SetLineStyle(1)
            MCHist.GetYaxis().SetTitle("Events")
            MCHist.GetYaxis().SetRangeUser(0, 1.8 * DataDistr.GetMaximum())

            MCHist.Draw("hist e")
            DataDistr.SetFillStyle(0)
            DataDistr.SetLineColor(1)
            DataDistr.SetLineWidth(2)
            DataDistr.SetLineStyle(1)
            DataDistr.Draw("e same")

            pu.DrawPlotLabels(0.195, 0.83, self.region, self.analysis, self.__Options.noATLAS)

            legend.SetBorderSize(0)
            legend.Draw()
            # ~ pu.DrawAtlas(0.195, 0.88)
            pu.GetBottomPad().cd()
            pu.GetBottomPad().SetGridy()
            ScalefactorHisto = XAMPPplotting.Utils.CreateRatioHisto(DataDistr, MCHist)
            ScalefactorHisto.GetYaxis().SetTitle("Data/MC")
            ScalefactorHisto.Draw()

            pu.saveHisto("{}/{}".format(OutputFolder, CanvasName), self.__Options.OutFileType)

    def PlotScalefactor(self, DataFakefactor, MCFakeFactor, ScaleFactor):

        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptTitle(0)
        if not ScaleFactor:
            return
        if not self.is2D:
            CreateDirectory("{}/{}_{}/".format(self.__Options.outputDir, self.FakeType, self.Particle), False)
            OutputFolder = "{}/{}_{}/".format(self.__Options.outputDir, self.FakeType, self.Particle)

            pu = PlotUtils(status=self.__Options.label, size=24, lumi=self.lumi)
            if self.Particle == "Tau":
                CanvasName = "Scalefactor_{}_{}_{}_{}_{}".format(self.region, self.Particle, self.prong, self.FakeType, self.DiscrVar)
            else:
                CanvasName = "Scalefactor_{}_{}_{}_{}".format(self.region, self.Particle, self.FakeType, self.DiscrVar)

            if MCFakeFactor[self.FakeType].GetEntries() == 0:
                print "Warning: MC Histogram is empty for Scalefactorcalculation"
                return

            if DataFakefactor.GetEntries() == 0:
                print "Warning: Data Histogram is empty for Scalefactorcalculation"
                return
            pu.Prepare2PadCanvas(CanvasName, 800, 600)
            pu.GetTopPad().cd()
            legend_x1 = 0.7
            legend_x2 = 0.9
            legend_y1 = 0.7
            legend_y2 = 0.85
            legend = ROOT.TLegend(legend_x1, legend_y1, legend_x2, legend_y2)
            legend.AddEntry(MCFakeFactor[self.FakeType], "MC", "l")
            legend.AddEntry(DataFakefactor, "Data", "lp")
            MCFakeFactor[self.FakeType].SetFillStyle(0)
            MCFakeFactor[self.FakeType].SetLineColor(2)
            MCFakeFactor[self.FakeType].SetLineWidth(2)
            MCFakeFactor[self.FakeType].SetLineStyle(1)
            MCFakeFactor[self.FakeType].GetYaxis().SetTitle("FakeFactor")
            MCFakeFactor[self.FakeType].GetYaxis().SetRangeUser(0, 1.8 * DataFakefactor.GetMaximum())
            MCFakeFactor[self.FakeType].Draw("hist e")

            DataFakefactor.SetFillStyle(0)
            DataFakefactor.SetLineColor(1)
            DataFakefactor.SetLineWidth(2)
            DataFakefactor.SetLineStyle(1)
            DataFakefactor.Draw("e same")

            pu.DrawPlotLabels(0.195, 0.83, self.region, self.analysis, self.__Options.noATLAS)

            legend.SetBorderSize(0)
            legend.Draw()
            # ~ pu.DrawAtlas(0.195, 0.88)
            pu.GetBottomPad().cd()
            pu.GetBottomPad().SetGridy()

            ScaleFactor.GetYaxis().SetTitle("Scalefactor")
            ScaleFactor.Draw()

            pu.saveHisto("{}/{}".format(OutputFolder, CanvasName), self.__Options.OutFileType)


class Processfraction(object):
    def __init__(self, Options, FakeComponents, Store=False, Plot=False, Projection=False, ProjectToXaxis=False):
        self.__Options = Options
        self.FakeComponents = FakeComponents

        self.analysis = FakeComponents[0].analysis
        self.region = FakeComponents[0].region

        self.Particle = FakeComponents[0].Particle
        self.Definition = FakeComponents[0].Definition
        self.prong = FakeComponents[0].prong

        self.DiscrVar = FakeComponents[0].DiscrVar

        self.is2D = False

        self.Store = Store
        self.Plot = Plot
        self.Projection = Projection
        self.ProjectToXaxis = ProjectToXaxis

        if not self.Store and not self.Plot:
            print "Error: specified to neither plot and store fakefactors. Returning"
            return

        print "Calculate processfraction for {}{} in region {} for variable {}".format(self.Definition, self.Particle, self.region,
                                                                                       self.DiscrVar)

        self.__colorscheme = {
            "LF": ROOT.kCyan,
            "HF": ROOT.kRed,
            "Gluon": ROOT.kYellow,
            "Real": ROOT.kBlue,
            "Conv": ROOT.kGreen,
            "CONV": ROOT.kGreen,
            "Elec": ROOT.kMagenta,
            "Unmatched": ROOT.kMagenta,
            "Muon": ROOT.kYellow,
            "KnownUnmatched": ROOT.kMagenta + 2
        }

        if self.Plot and not self.Projection:
            self.FakeHistogramsToPlot = self.RetrieveHistograms(PlotOverFlowBin=self.__Options.PlotOverFlowBin)
            self.NormalizationHistosToPlot = self.GetNormalizationHisto(self.FakeHistogramsToPlot)
            self.ProcessfractionsToPlot = self.CalculateProcessfractions(self.FakeHistogramsToPlot, self.NormalizationHistosToPlot, True)
            self.PlotProcessFraction(self.ProcessfractionsToPlot)
            self.ProcessfractionsSumBkg = self.CalculateProcessfractions(self.FakeHistogramsToPlot, self.NormalizationHistosToPlot)
            self.PlotFullProcessfraction(self.ProcessfractionsSumBkg)
            # ~ if self.is2D:
            # ~ self.PlotNormalizationHisto(self.NormalizationHistosToPlot)
        if self.Store and not self.Projection:
            if self.__Options.ExtendAxis:
                self.FakeHistogramsToStore = self.RetrieveHistograms(Store=True)

            else:
                self.FakeHistogramsToStore = self.RetrieveHistograms()
            self.NormalizationHistosToStore = self.GetNormalizationHisto(self.FakeHistogramsToStore)
            self.ProcessfractionsToStore = self.CalculateProcessfractions(self.FakeHistogramsToStore, self.NormalizationHistosToStore)

        if self.Projection:
            self.FakeHistogramsToPlot = self.RetrieveHistograms(PlotOverFlowBin=self.__Options.PlotOverFlowBin)
            self.ProjectedHistos = self.ProjectTo1DHisto(self.FakeHistogramsToPlot)
            self.ProjectedNormalizationhistos = self.GetNormalizationHisto(self.ProjectedHistos)
            self.ProjectedProcessfractions = self.CalculateProcessfractions(self.ProjectedHistos, self.ProjectedNormalizationhistos, True)
            self.PlotProcessFraction(self.ProjectedProcessfractions)
            self.ProcessfractionsSumBkg = self.CalculateProcessfractions(self.ProjectedHistos, self.ProjectedNormalizationhistos)
            self.PlotFullProcessfraction(self.ProcessfractionsSumBkg)

    def RetrieveHistograms(self, Store=False, PlotOverFlowBin=False):
        FakeHistograms = {}
        for component in self.FakeComponents:

            HistoSets = XAMPPplotting.PlottingHistos.CreateHistoSets(self.__Options, component.analysis, component.region, component.var,
                                                                     False)
            if not HistoSets:
                continue
            HistoSet = HistoSets[0]
            if not HistoSet.GetSummedBackground().isValid():
                continue
            if HistoSet.GetSummedBackground().isTH2():
                self.is2D = True

            histos = HistoSet.GetBackgrounds()
            Histograms = {}
            FakeType = component.FakeType

            for hist in histos:

                process = hist.GetName()
                histo = self.RemoveNegativeEntries(hist.GetHistogram())
                if not process in FakeHistograms:
                    FakeHistograms[process] = {}

                if (Store or PlotOverFlowBin) and (self.DiscrVar.startswith("pt") or self.DiscrVar.startswith("met")
                                                   or self.DiscrVar.startswith("meff")):
                    Histo = histo.Clone()
                    histoName = Histo.GetName()
                    histoName += "_" + process + "_" + self.region
                    if Store:
                        ExtendedHisto = ExtendAxis(Histo, histoName, self.__Options.ExtendedBinEdge)
                    if not Store and PlotOverFlowBin:
                        maxBin = Histo.GetXaxis().GetBinLowEdge(Histo.GetNbinsX() + 1)
                        ExtendedHisto = ExtendAxis(Histo, histoName, maxBin + 50)
                    FakeHistograms[process][FakeType] = ExtendedHisto
                else:
                    FakeHistograms[process][FakeType] = histo.Clone()

        return FakeHistograms

    def RemoveNegativeEntries(self, histo):
        for xi in range(histo.GetNbinsX() + 1):
            if self.is2D:
                for yi in range(histo.GetNbinsY() + 1):
                    if histo.GetBinContent(xi + 1, yi + 1) < 0:
                        # ~ print "removed negative bin for %s" % (faketype)
                        histo.SetBinContent(xi + 1, yi + 1, 0)
                        histo.SetBinError(xi + 1, yi + 1, 0)
            else:
                if histo.GetBinContent(xi + 1) < 0:
                    histo.SetBinContent(xi + 1, 0)
                    histo.SetBinError(xi + 1, 0)
        return histo

    def ProjectTo1DHisto(self, FakeHistograms):
        Histos1D = {}

        for process, faketypes in FakeHistograms.items():
            if not process in Histos1D:
                Histos1D[process] = {}
            for faketype, histo in faketypes.items():
                if self.ProjectToXaxis:
                    Histo1D = histo.ProjectionX(
                        "1DProjection_{}_{}_{}{}_{}_{}_{}".format(self.region, process, self.Definition, self.Particle, self.prong,
                                                                  faketype, self.DiscrVar), 0, -1, "e")
                else:

                    Histo1D = histo.ProjectionY(
                        "1DProjection_{}_{}_{}{}_{}_{}_{}".format(self.region, process, self.Definition, self.Particle, self.prong,
                                                                  faketype, self.DiscrVar), 0, -1, "e")
                Histos1D[process][faketype] = Histo1D
        return Histos1D

    def GetNormalizationHisto(self, FakeHistograms):
        Normalizationhistos = {}

        for process, faketypes in FakeHistograms.items():
            for faketype, histo in faketypes.items():
                if not process in Normalizationhistos:
                    Normalizationhisto = histo.Clone()
                    Normalizationhisto.Reset()
                    Normalizationhistos[process] = Normalizationhisto
                Normalizationhistos[process].Add(histo)
        for process, histo in Normalizationhistos.items():
            if not "SumBG" in Normalizationhistos:
                SumBGNormalizationHisto = histo.Clone()
                SumBGNormalizationHisto.Reset()
                Normalizationhistos["SumBG"] = SumBGNormalizationHisto
            Normalizationhistos["SumBG"].Add(histo)

        return Normalizationhistos

    def CalculateProcessfractions(self, FakeHistograms, Normalizationhisto, ToPlot=False):
        Processfractions = {}

        for process, faketypes in FakeHistograms.items():
            if not process in Processfractions:
                Processfractions[process] = {}
            for faketype, histo in faketypes.items():

                Processfractionhisto = histo.Clone()
                if ToPlot:
                    Processfractionhisto.Divide(Normalizationhisto[process])
                else:
                    Processfractionhisto.Divide(Normalizationhisto["SumBG"])
                Processfractions[process][faketype] = Processfractionhisto
        return Processfractions

    def PlotProcessFraction(self, ProcessfractionHistos):

        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptTitle(0)

        if self.is2D and not self.Projection:
            self.Plot2DHistos(ProcessfractionHistos)
            return

        for process, faketypes in ProcessfractionHistos.items():
            CreateDirectory("{}/{}/{}/{}{}/".format(self.__Options.outputDir, self.region, process, self.Definition, self.Particle), False)
            OutputFolder = "{}/{}/{}/{}{}/".format(self.__Options.outputDir, self.region, process, self.Definition, self.Particle)
            pu = PlotUtils(status=self.__Options.label, size=24)
            CanvasName = "Processfraction_{}_{}_{}{}_{}_{}".format(self.region, process, self.Definition, self.Particle, self.prong,
                                                                   self.DiscrVar)

            pu.Prepare1PadCanvas(CanvasName, 800, 600, self.__Options.quadCanvas)
            can = pu.GetCanvas()
            can.SetBottomMargin(0.15)
            ProcFracStack = ROOT.THStack("ProcFrac_{}".format(CanvasName), "")
            legend_x1 = 0.7
            legend_x2 = 0.9
            legend_y1 = 0.6
            legend_y2 = 0.85
            legend = ROOT.TLegend(legend_x1, legend_y1, legend_x2, legend_y2)
            completeEmptyHistos = True  # if all histograms in the region are empty, they are not plotted
            XaxisTitle = None
            for faketype, histo in faketypes.items():
                # ~ if histo.GetEntries() == 0:  # empty faketypes should not appear in the legend
                # ~ continue
                if not XaxisTitle:
                    XaxisTitle = histo.GetXaxis().GetTitle()
                completeEmptyHistos = False
                histo.SetLineColor(self.__colorscheme[faketype])
                histo.SetMarkerColor(self.__colorscheme[faketype])
                histo.SetFillColor(self.__colorscheme[faketype])
                if faketype == "LF" and self.Particle == "Tau":
                    legend.AddEntry(histo, "QJ", "f")
                else:
                    legend.AddEntry(histo, faketype, "f")
                ProcFracStack.Add(histo)
            # ~ if completeEmptyHistos:
            # ~ continue
            ProcFracStack.Draw("hist")
            ProcFracStack.SetMaximum(1.8)

            # ~ XaxisTitle = self.NormalizationHistos[process].GetXaxis().GetTitle()
            ProcFracStack.GetXaxis().SetTitle(XaxisTitle)
            ProcFracStack.GetYaxis().SetTitle("Processfraction")
            legend.SetBorderSize(0)
            legend.Draw()

            xCoord = 0.195
            yCoord = 0.83
            if not self.__Options.noATLAS:
                pu.DrawAtlas(xCoord, yCoord)
                yCoord -= 0.08
            pu.DrawRegionLabel(self.analysis, self.region, xCoord, yCoord)
            yCoord -= 0.08
            if self.Particle == "Tau":
                pu.DrawTLatex(xCoord, yCoord, "%s %s" % (self.Particle, self.prong))
            else:
                pu.DrawTLatex(xCoord, yCoord, "%s" % (self.Particle))
            pu.saveHisto("%s/%s" % (OutputFolder, CanvasName), Options.OutFileType)

    def Plot2DHistos(self, ProcessfractionHistos):
        for process, faketypes in ProcessfractionHistos.items():
            for faketype, histo in faketypes.items():
                # ~ if histo.GetEntries() == 0:
                # ~ continue
                CreateDirectory("{}/{}/{}/{}{}/".format(self.__Options.outputDir, self.region, process, self.Definition, self.Particle),
                                False)
                OutputFolder = "{}/{}/{}/{}{}/".format(self.__Options.outputDir, self.region, process, self.Definition, self.Particle)
                pu = PlotUtils(status=self.__Options.label, size=24)
                CanvasName = "Processfraction_{}_{}_{}{}_{}_{}_{}".format(self.region, process, self.Definition, self.Particle, faketype,
                                                                          self.prong, self.DiscrVar)

                pu.Prepare1PadCanvas(CanvasName, 800, 600, self.__Options.quadCanvas)
                can = pu.GetCanvas()
                can.SetBottomMargin(0.15)

                histo.Draw("colz")

                xCoord = 0.195
                yCoord = 0.83
                if not self.__Options.noATLAS:
                    pu.DrawAtlas(xCoord, yCoord)
                    yCoord -= 0.08
                pu.DrawRegionLabel(self.analysis, self.region, xCoord, yCoord)
                yCoord -= 0.08
                if self.Particle == "Tau":
                    pu.DrawTLatex(xCoord, yCoord, "%s %s" % (self.Particle, self.prong))
                else:
                    pu.DrawTLatex(xCoord, yCoord, "%s" % (self.Particle))
                pu.saveHisto("%s/%s" % (OutputFolder, CanvasName), Options.OutFileType)

    def PlotNormalizationHisto(self, Normalizationhisto):
        histo = Normalizationhisto["SumBG"]
        CreateDirectory("{}/{}/{}{}/".format(self.__Options.outputDir, self.region, self.Definition, self.Particle), False)
        OutputFolder = "{}/{}/{}{}/".format(self.__Options.outputDir, self.region, self.Definition, self.Particle)
        pu = PlotUtils(status=self.__Options.label, size=24)
        CanvasName = "Normalization_{}_{}{}_{}_{}".format(self.region, self.Definition, self.Particle, self.prong, self.DiscrVar)

        pu.Prepare1PadCanvas(CanvasName, 800, 600, self.__Options.quadCanvas)
        can = pu.GetCanvas()
        can.SetBottomMargin(0.15)

        histo.Draw("colz")

        xCoord = 0.195
        yCoord = 0.83
        if not self.__Options.noATLAS:
            pu.DrawAtlas(xCoord, yCoord)
            yCoord -= 0.08
        pu.DrawRegionLabel(self.analysis, self.region, xCoord, yCoord)
        yCoord -= 0.08
        if self.Particle == "Tau":
            pu.DrawTLatex(xCoord, yCoord, "%s %s" % (self.Particle, self.prong))
        else:
            pu.DrawTLatex(xCoord, yCoord, "%s" % (self.Particle))
        pu.saveHisto("%s/%s" % (OutputFolder, CanvasName), Options.OutFileType)

    def PlotFullProcessfraction(self, Processfractionhistos):
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetOptTitle(0)

        if self.is2D and not self.Projection:
            return

        CreateDirectory("{}/{}/SumBkg/{}{}/".format(self.__Options.outputDir, self.region, self.Definition, self.Particle), False)
        OutputFolder = "{}/{}/SumBkg/{}{}/".format(self.__Options.outputDir, self.region, self.Definition, self.Particle)
        pu = PlotUtils(status=self.__Options.label, size=24)
        CanvasName = "Processfraction_{}_{}{}_{}_{}".format(self.region, self.Definition, self.Particle, self.prong, self.DiscrVar)

        pu.Prepare1PadCanvas(CanvasName, 800, 600, self.__Options.quadCanvas)
        can = pu.GetCanvas()
        can.SetBottomMargin(0.15)
        ProcFracStack = ROOT.THStack("ProcFrac_{}".format(CanvasName), "")
        legend_x1 = 0.7
        legend_x2 = 0.9
        legend_y1 = 0.6
        legend_y2 = 0.85
        legend = ROOT.TLegend(legend_x1, legend_y1, legend_x2, legend_y2)
        completeEmptyHistos = True  # if all histograms in the region are empty, they are not plotted
        XaxisTitle = None
        for process, faketypes in Processfractionhistos.items():
            for faketype, histo in faketypes.items():
                if histo.GetEntries() == 0:  # empty faketypes should not appear in the legend
                    continue
                if not XaxisTitle:
                    XaxisTitle = histo.GetXaxis().GetTitle()
                completeEmptyHistos = False
                if process == "ttbar":
                    histo.SetLineColor(self.__colorscheme[faketype])
                    histo.SetMarkerColor(self.__colorscheme[faketype])
                    histo.SetFillColor(self.__colorscheme[faketype])
                else:
                    histo.SetLineColor(self.__colorscheme[faketype] + 1)
                    histo.SetMarkerColor(self.__colorscheme[faketype] + 1)
                    histo.SetFillColor(self.__colorscheme[faketype] + 1)
                if faketype == "LF" and self.Particle == "Tau":
                    legend.AddEntry(histo, process + ", " + "QJ", "f")
                else:
                    legend.AddEntry(histo, process + ", " + faketype, "f")
                ProcFracStack.Add(histo)
        if completeEmptyHistos:
            return
        ProcFracStack.Draw("hist")
        ProcFracStack.SetMaximum(1.8)

        # ~ XaxisTitle = self.NormalizationHistos[process].GetXaxis().GetTitle()
        ProcFracStack.GetXaxis().SetTitle(XaxisTitle)
        ProcFracStack.GetYaxis().SetTitle("Processfraction")
        legend.SetBorderSize(0)
        legend.Draw()

        xCoord = 0.195
        yCoord = 0.83
        if not self.__Options.noATLAS:
            pu.DrawAtlas(xCoord, yCoord)
            yCoord -= 0.08
        pu.DrawRegionLabel(self.analysis, self.region, xCoord, yCoord)
        yCoord -= 0.08
        if self.Particle == "Tau":
            pu.DrawTLatex(xCoord, yCoord, "%s %s" % (self.Particle, self.prong))
        else:
            pu.DrawTLatex(xCoord, yCoord, "%s" % (self.Particle))
        pu.saveHisto("%s/%s" % (OutputFolder, CanvasName), Options.OutFileType)


class FakeCalculator(object):
    def __init__(self, Options):
        self.__Options = Options
        self.__FakeHistograms = self.ReadInputVars()
        if self.__Options.CalculateFakefactor:
            self.__FakeFactors = self.CalculateFakeFactors()
            self.__FakefactorFile = None
            self.StoreFakefactors()
        if self.__Options.CalculateScalefactor:
            self.__ScaleFactors = self.CalculateScaleFactors()
            self.__ScalefactorFile = None
            self.StoreScaleFactors()
        if self.__Options.CalculateProcessfraction:
            self.__ProcessFractions = self.CalculateProcessFractions()
            self.__ProcessfractionFile = None
            self.StoreProcessFractions()

    def ReadInputVars(self):
        FakeHistograms = []
        FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(Options)
        for ana in FileStructure.GetConfigSet().GetAnalyses():
            for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
                for var in FileStructure.GetConfigSet().GetVariables(ana, region):

                    if var.split("_")[-1] == "pt" or var.split("_")[-1] == "pteta" or var.split("_")[-1] == "eta" or var.split(
                            "_")[-1] == "ptmu" or var.split("_")[-1] == "ptmet" or var.split("_")[-1] == "ptmeff" or var.split(
                                "_")[-1] == "fineeta":

                        FakeHisto = FakeHistogram(ana, region, var)
                        FakeHistograms.append(FakeHisto)
        if len(FakeHistograms) == 0:
            print "no histograms found"
        return FakeHistograms

    def CalculateFakeFactors(self):
        print "Calculate Fakefactors"
        FakeFactors = {"Plot": [], "Store": []}

        for i in self.__FakeHistograms:

            if not i.Definition == "Loose":
                continue

            # ~ if not i.FakeType == "LF" and not i.FakeType == "HF" and not i.FakeType == "Gluon" and not i.FakeType == "Elec" and not ( i.FakeType == "Conv" and i.Particle == "Electrons") and not (i.FakeType == "Muon" and i.Particle == "Electrons"):
            if not i.FakeType == "LF" and not i.FakeType == "HF" and not i.FakeType == "Gluon" and not i.FakeType == "Elec" and not (
                    i.FakeType == "Conv" and i.Particle == "Electrons"):
                continue

            for j in self.__FakeHistograms:
                if j.Definition == "Signal":
                    if i.region == j.region and i.Particle == j.Particle and i.prong == j.prong and i.FakeType == j.FakeType and i.DiscrVar == j.DiscrVar:
                        if i.DiscrVar in self.__Options.VariablesToPlot:

                            if not i.ProjectTo1D:
                                FakeFactors["Plot"].append(Fakefactor(self.__Options, i, j, Plot=True))
                            else:
                                FakeFactors["Plot"].append(Fakefactor(self.__Options, i, j, Plot=True, Projection=True))
                        if i.DiscrVar in self.__Options.VariablesToStore:
                            if not i.ProjectTo1D:
                                FakeFactors["Store"].append(Fakefactor(self.__Options, i, j, Store=True))

        if len(FakeFactors["Plot"]) == 0 and len(FakeFactors["Store"]) == 0:
            print "No Signal/Loose pairs found"
        return FakeFactors

    def CalculateScaleFactors(self):
        print "Calculate Scalefactors"
        ScaleFactors = {"Plot": [], "Store": []}
        alreadyconsidered = []
        for i in self.__FakeHistograms:
            LooseFakeHistos = []
            SignalFakeHistos = []
            fakeoriginindependentvar = [i.region, i.Particle, i.Definition, i.prong, i.DiscrVar]
            if fakeoriginindependentvar in alreadyconsidered:
                continue
            alreadyconsidered.append(fakeoriginindependentvar)
            if not i.Definition == "Loose":
                continue
            for j in self.__FakeHistograms:
                if i.region == j.region and i.Particle == j.Particle and i.prong == j.prong and i.DiscrVar == j.DiscrVar:
                    if j.Definition == "Loose":
                        LooseFakeHistos.append(j)
                    if j.Definition == "Signal":
                        SignalFakeHistos.append(j)
            if not len(LooseFakeHistos) == 0 or not len(SignalFakeHistos) == 0:
                if not len(LooseFakeHistos) == len(SignalFakeHistos):
                    print "Warning: Number of Signal and Loose faketypes do not match"
                    continue

                if i.DiscrVar in self.__Options.VariablesToPlot:
                    if not i.ProjectTo1D:
                        ScaleFactors["Plot"].append(Scalefactor(self.__Options, LooseFakeHistos, SignalFakeHistos, Plot=True))
                    else:
                        ScaleFactors["Plot"].append(
                            Scalefactor(self.__Options, LooseFakeHistos, SignalFakeHistos, Plot=True, Projection=True))
                if i.DiscrVar in self.__Options.VariablesToStore:
                    if not i.ProjectTo1D:
                        ScaleFactors["Store"].append(Scalefactor(self.__Options, LooseFakeHistos, SignalFakeHistos, Store=True))

        if len(ScaleFactors["Plot"]) == 0 and len(ScaleFactors["Store"]) == 0:
            print "No Histos for Scalefactors found"
        return ScaleFactors

    def CalculateProcessFractions(self):
        print "Calculate Processfractions"
        ProcessFractions = {"Plot": [], "Store": []}
        alreadyconsidered = []
        for i in self.__FakeHistograms:
            # ~ if i.Definition == "Signal":
            # ~ continue
            if i.region == "WZ(#mu#mu)" or i.region == "WZ(ee)":
                continue

            fakeoriginindependentvar = [i.region, i.Particle, i.Definition, i.prong, i.DiscrVar]
            if fakeoriginindependentvar in alreadyconsidered:
                continue
            alreadyconsidered.append(fakeoriginindependentvar)
            FakeComponents = []
            for j in self.__FakeHistograms:
                if i.region == j.region and i.Particle == j.Particle and i.Definition == j.Definition and i.prong == j.prong and i.DiscrVar == j.DiscrVar:
                    # ~ if j.FakeType == "LF" or j.FakeType == "HF" or j.FakeType == "Gluon" or j.FakeType == "Elec" or (j.FakeType == "Conv" and j.Particle == "Electrons") or (j.FakeType == "Muon" and j.Particle == "Electrons") or j.FakeType == "Unmatched" or j.FakeType == "KnownUnmatched" or j.FakeType == "Real":
                    if j.FakeType == "LF" or j.FakeType == "HF" or j.FakeType == "Gluon" or j.FakeType == "Elec" or (
                            j.FakeType == "Conv" and j.Particle == "Electrons"):

                        FakeComponents.append(j)
            if not len(FakeComponents) == 0:
                if i.DiscrVar in self.__Options.VariablesToPlot:
                    if not i.ProjectTo1D:
                        ProcessFractions["Plot"].append(Processfraction(self.__Options, FakeComponents, Plot=True))
                    else:
                        ProcessFractions["Plot"].append(Processfraction(self.__Options, FakeComponents, Plot=True, Projection=True))
                if i.DiscrVar in self.__Options.VariablesToStore:
                    if not i.ProjectTo1D:
                        ProcessFractions["Store"].append(Processfraction(self.__Options, FakeComponents, Store=True))

        if len(ProcessFractions["Plot"]) == 0 and len(ProcessFractions["Store"]) == 0:
            print "No Histos for Processfractions found"
        return ProcessFractions

    def StoreFakefactors(self):
        if len(self.__FakeFactors["Store"]) == 0:
            print "No Histos to save"
            return True
        self.__FakefactorFile = ROOT.TFile("{}/{}".format(self.__Options.outputDir, self.__Options.FakefactorFileName), "RECREATE")
        for fakefactor in self.__FakeFactors["Store"]:
            if fakefactor.region not in self.__FakefactorFile.GetListOfKeys():
                self.__FakefactorFile.mkdir(fakefactor.region)
            if fakefactor.region in self.__FakefactorFile.GetListOfKeys():
                folder = self.__FakefactorFile.Get(fakefactor.region)
                for process, histo in fakefactor.FakeFactorHistosToStore.items():
                    if process not in folder.GetListOfKeys():
                        folder.mkdir(process)
                    if process in folder.GetListOfKeys():
                        subfolder = folder.Get(process)
                        if fakefactor.FakeType not in subfolder.GetListOfKeys():
                            subfolder.mkdir(fakefactor.FakeType)
                        if fakefactor.FakeType in subfolder.GetListOfKeys():
                            subsubfolder = subfolder.Get(fakefactor.FakeType)
                            directory = self.__FakefactorFile.GetDirectory("{}/{}/{}".format(fakefactor.region, process,
                                                                                             fakefactor.FakeType))
                            if fakefactor.Particle == "Tau":
                                directory.WriteObject(
                                    histo, "FakeFactor_{}_orig{}_prong{}_{}".format(fakefactor.Particle, fakefactor.FakeType,
                                                                                    fakefactor.prong, fakefactor.DiscrVar))
                            else:
                                directory.WriteObject(
                                    histo, "FakeFactor_{}_orig{}_{}".format(fakefactor.Particle, fakefactor.FakeType, fakefactor.DiscrVar))
        self.__FakefactorFile.Close()
        return True

    def StoreScaleFactors(self):
        if len(self.__ScaleFactors["Store"]) == 0:
            print "No Histos to save"
            return True
        self.__ScalefactorFile = ROOT.TFile("{}/{}".format(self.__Options.outputDir, self.__Options.ScalefactorFileName), "RECREATE")
        for sf in self.__ScaleFactors["Store"]:
            # ~ if sf.Store == True and not sf.FakeType == '':
            if sf.FakeType not in self.__ScalefactorFile.GetListOfKeys():
                self.__ScalefactorFile.mkdir(sf.FakeType)
            if sf.FakeType in self.__ScalefactorFile.GetListOfKeys():
                sffolder = self.__ScalefactorFile.Get(sf.FakeType)
                if "ScaleFactor" not in sffolder.GetListOfKeys():
                    sffolder.mkdir("ScaleFactor")
                if "ScaleFactor" in sffolder.GetListOfKeys():
                    folder = sffolder.Get("ScaleFactor")
                    directory = self.__ScalefactorFile.GetDirectory("{}/ScaleFactor".format(sf.FakeType))
                    if sf.Particle == "Tau":
                        directory.WriteObject(sf.ScaleFactorHistoToStore,
                                              "ScaleFactor_{}_orig{}_prong{}_{}".format(sf.Particle, sf.FakeType, sf.prong, sf.DiscrVar))
                    else:
                        directory.WriteObject(sf.ScaleFactorHistoToStore,
                                              "ScaleFactor_{}_orig{}_{}".format(sf.Particle, sf.FakeType, sf.DiscrVar))

        self.__ScalefactorFile.Close()
        return True

    def StoreProcessFractions(self):
        if len(self.__ProcessFractions["Store"]) == 0:
            print "No Histos to save"
            return True
        self.__ProcessfractionFile = ROOT.TFile("{}/{}".format(self.__Options.outputDir, self.__Options.ProcessfractionFileName),
                                                "RECREATE")
        for procfrac in self.__ProcessFractions["Store"]:
            # ~ if procfrac.Store == True:
            if procfrac.region not in self.__ProcessfractionFile.GetListOfKeys():
                self.__ProcessfractionFile.mkdir(procfrac.region)
            if procfrac.region in self.__ProcessfractionFile.GetListOfKeys():
                regionfolder = self.__ProcessfractionFile.Get(procfrac.region)
                for process, faketypes in procfrac.ProcessfractionsToStore.items():
                    if process not in regionfolder.GetListOfKeys():
                        regionfolder.mkdir(process)
                    if process in regionfolder.GetListOfKeys():
                        processfolder = regionfolder.Get(process)
                        for faketype, histo in faketypes.items():

                            if faketype not in processfolder.GetListOfKeys():
                                processfolder.mkdir(faketype)
                            if faketype in processfolder.GetListOfKeys():
                                faketypefolder = processfolder.Get(faketype)
                                directory = self.__ProcessfractionFile.GetDirectory("{}/{}/{}".format(procfrac.region, process, faketype))
                                if procfrac.Particle == "Tau":
                                    directory.WriteObject(
                                        histo, "ProcessFraction_{}{}_orig{}_prong{}_{}".format(procfrac.Definition, procfrac.Particle,
                                                                                               faketype, procfrac.prong, procfrac.DiscrVar))
                                else:
                                    directory.WriteObject(
                                        histo, "ProcessFraction_{}{}_orig{}_{}".format(procfrac.Definition, procfrac.Particle, faketype,
                                                                                       procfrac.DiscrVar))
        self.__ProcessfractionFile.Close()
        return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This script produces fake ratio files \"python CalculateFakeFactors.py -h\"',
                                     prog='CalculateFakeRatios',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = XAMPPplotting.Utils.setupBaseParser(parser)
    parser.add_argument('--CalculateFakefactor', help='Fakefactors are calculated and plotted', action='store_true', default=False)
    parser.add_argument('--CalculateScalefactor', help='Scalefactors are calculated and plotted', action='store_true', default=False)
    parser.add_argument('--CalculateProcessfraction',
                        help='Processfractions are calculated and plotted',
                        action='store_true',
                        default=False)
    parser.add_argument('--FakefactorFileName', help='Name of the File where Fakefactors are stored', default="Fakefactors.root")
    parser.add_argument('--ScalefactorFileName', help='Name of the File where Fakefactors are stored', default="Scalefactors.root")
    parser.add_argument('--ProcessfractionFileName', help='Name of the File where Fakefactors are stored', default="Processfractions.root")
    parser.add_argument('--VariablesToPlot', help='Variables that are plotted', nargs='+', default=["pt", "eta", "pteta"])
    parser.add_argument('--VariablesToStore', help='Variables that are stored', nargs='+', default=[])
    parser.add_argument('--ExtendedBinEdge',
                        help='Defines the higher binedge for the stored histograms. For debug only',
                        default=1e25,
                        type=float)
    parser.add_argument('--ExtendAxis', help='Axis are extended for stored histograms', action='store_true', default=False)
    parser.add_argument('--PlotOverFlowBin', help='also Plot the overflow bin', action='store_true', default=False)

    Options = parser.parse_args()
    CreateDirectory(Options.outputDir, False)
    FakeCalculator(Options)

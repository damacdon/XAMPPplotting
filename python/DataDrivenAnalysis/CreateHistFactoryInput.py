##
# What is does:
# ------------
# Prepares the path structure with templates and creates the input root file for HistFactory
#
# How it runs:
#-------------
# python XAMPPplotting/python/CreateHistFactoryInput.py  -c XAMPPplotting/python/DSConfigs/FourLeptonSF.py -o HistFactory --noSignal --noSyst --lumi 36.5
#

import ROOT
from array import array
import argparse
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos

global_allowed_taujet_variables = ['width', 'ntrks', 'bdt']

global_tau_definition = "Loose"


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def FindBinTrusted(h=None, x=0):

    for i in xrange(h.GetNbinsX() + 1):
        lowedge = h.GetBinLowEdge(i)
        bw = h.GetBinWidth(i)
        if (x > lowedge or isclose(lowedge, x)) and x < lowedge + bw:
            return i

    return -1


def get_structure(pdf, sample, analysis, region, variable):
    if pdf == 'taujet':
        return get_structure_taujet(sample, analysis, region, variable)

    if pdf == 'mass':
        return get_structure_mass(sample, analysis, region, variable)

    return "", ""


def get_structure_mass(sample, analysis, region, variable):
    """
    template: Zmumu_mass
    """
    if not sample or not analysis or not region or not variable:
        sys.exit("Error: get_structure - check your inputs '{}' '{}' '{}' '{}'".format(sample, analysis, region, variable))

    tokens = variable.split("_")
    quantity = tokens[-1] if tokens else ""

    if not quantity:
        sys.exit("Error: get_structure - unable to get quantity for the sample {} with variable {}".format(sample, variable))

    folder = "%s_%s" % (region.replace("#", ""), quantity)

    subfolder = "%s" % (sample)
    #if sample != "Data":
    #    subfolder = "%s"%(origin.replace('orig',''))

    return folder, subfolder


def get_structure_taujet(sample, analysis, region, variable):
    """
    template: Tau_defSignal_origCONV_etaIncl_ptIncl_prongIncl_pt
    """
    if not sample or not analysis or not region or not variable:
        sys.exit("Error: get_structure - check your inputs '{}' '{}' '{}' '{}'".format(sample, analysis, region, variable))

    prong_field = 'prong'
    ptrange_field = 'pt'
    origin_field = 'orig'
    definition_field = 'def'

    tokens = variable.split("_")
    prongness = ""
    ptrange = ""
    origin = ""
    ntokens = len(tokens)
    quantity = tokens[-1] if tokens else ""
    for i, token in enumerate(tokens):
        if prong_field in token:
            prongness = token
        if ptrange_field in token and i < ntokens:
            ptrange = token
        if origin_field in token:
            origin = token
        if definition_field in token:
            definition = token.replace(definition_field, '')
    if definition != global_tau_definition:
        sys.exit("Error: get_structure - expected a signal definition at this stage for the case {}, {}, {}".format(
            prongness, ptrange, origin))

    if not prongness or not ptrange or not origin:
        sys.exit("Error: get_structure - unable to define at least one for the following fields {}, {}, {}".format(
            prongness, ptrange, origin))

    if not quantity:
        sys.exit("Error: get_structure - unable to get quantity for the sample {} with {}, {}, {}".format(
            sample, prongness, ptrange, origin))

    folder = "%s_%s_%s_%s" % (region.replace("#", ""), quantity, prongness, ptrange)

    subfolder = "%s" % (sample)
    if sample != "Data":
        subfolder = "%s" % (origin.replace('orig', ''))

    return folder, subfolder


def CreateFileStructure(File, folder, subfolder):

    if folder not in File.GetListOfKeys():
        File.mkdir(folder)

    Folder = File.Get(folder)

    if not Folder:
        return False

    if subfolder not in Folder.GetListOfKeys():
        Folder.mkdir(subfolder)

    Subfolder = File.Get(subfolder)

    if not subfolder:
        return False

    return True


def IsDirectory(obj):
    return obj and issubclass(type(obj), ROOT.TObject) and "TDirectoryFile" in obj.IsA().GetName()


def IsHistogram(obj):
    return obj and \
        (issubclass(type(obj), ROOT.TObject) or issubclass(type(obj.get()), ROOT.TObject) ) and \
        obj.InheritsFrom("TH1") and \
        ("TH1D" in obj.IsA().GetName() or "TH1F" in obj.IsA().GetName())


def NewRange(histo=None, lowx=-1., highx=-1., keepOverflowBin=False):

    if not histo:
        sys.exit("Error: null histogram")

    if lowx < 0 and highx < 0:
        return histo

    nx = histo.GetNbinsX()
    minX = histo.GetBinLowEdge(1)
    bw = histo.GetBinWidth(nx)
    maxX = histo.GetBinLowEdge(nx) + bw

    if lowx < minX:
        sys.exit("Error: new x-axis range is not possible due to the requested minimum x-range bound...")

    if highx > maxX:
        sys.exit("Error: new x-axis range is not possible because the requested upper bin exceeds the current histo limit")

    LowBin = FindBinTrusted(histo, lowx)
    HighBin = FindBinTrusted(histo, highx)
    HighBin -= 1  # so that the requested upper x value is really corresponding to the rightmost value in the x-axis

    if HighBin < LowBin:
        sys.exit("Error: incompatible high %i and low %i bins for histo %s ..." % (HighBin, LowBin, histo.GetName()))

    LowX = histo.GetBinLowEdge(LowBin)
    HighX = histo.GetBinLowEdge(HighBin) + histo.GetBinWidth(HighBin)
    N = HighBin - LowBin + 1
    Hnew = ROOT.TH1F('%s_new' % (histo.GetName()), '%s new' % (histo.GetTitle()), N, LowX, HighX)

    for i in xrange(LowBin, HighBin + 1):
        if histo.IsBinOverflow(i):
            sys.exit("New Range: including an overflow bin...")

        if histo.IsBinUnderflow(i):
            sys.exit("New Range: including an underflow bin...")

        x = histo.GetBinCenter(i)
        y = histo.GetBinContent(i)
        dy = histo.GetBinError(i)
        b = FindBinTrusted(Hnew, x)
        if not isclose(histo.GetXaxis().GetBinLowEdge(i), Hnew.GetXaxis().GetBinLowEdge(b), 1e-6):
            sys.exit("Error: failure in matching original bin %f to new %f" %
                     (histo.GetXaxis().GetBinLowEdge(i), Hnew.GetXaxis().GetBinLowEdge(b)))
        Hnew.SetBinContent(b, y)
        Hnew.SetBinError(b, dy)

    return Hnew


def FillVariable(pdf, File, sample, analysis, region, variable, inputhisto, newRange=False, lowx=-1., highx=-1.):
    #check object type
    if not IsHistogram(inputhisto):
        print "Error: object '%s' is not a ROOT histogram or not a valid histogram pointer..." % (inputhisto)
        return False

    #set new range for histo including under- and overflow bins
    if newRange:
        histo = NewRange(inputhisto, lowx, highx)
    else:
        histo = inputhisto

    #get folder and subfolder
    folder, subfolder = get_structure(pdf, sample, analysis, region, variable)

    if not folder or not subfolder:
        print "Error: cannot create structure for case:", pdf, sample, analysis, region, variable

    #create structure in file
    if not CreateFileStructure(File, folder, subfolder):
        print "Error: cannot create structure {}/{}" % (folder, subfolder)
        return False

    #get full directory and store histos
    directory = File.GetDirectory("%s/%s" % (folder, subfolder))

    if IsDirectory(directory):
        print "Storing %s in %s/%s" % (histo.GetName(), folder, subfolder)
        directory.WriteObject(histo.get(), "nominal")
    else:
        print "Error: object '%s' is not a ROOT directory or not a valid path..." % (directory)
        return False

    return True


def StoreLumiHistos(lumi, f):
    h = ROOT.TH1F("h", "", 1, 0, 1)
    h.Fill(0, lumi)

    f.cd()
    for fold in getfolders(ROOT.gDirectory):
        if not ROOT.gDirectory.cd(fold.GetName()):
            print "Error: Cannot enter in {} in order to store the lumi plot" % (fold.GetName())
            return False
        ROOT.gDirectory.WriteObject(h, "lumiininvpb")
        ROOT.gDirectory.cd("../")

    return True


def ParsedOptions():

    parser = argparse.ArgumentParser(description='This script produces fake ratio files \"python CalculateFakeFactors.py -h\"',
                                     prog='CalculateFakeRatios',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = XAMPPplotting.Utils.setupBaseParser(parser)
    parser.add_argument('--pdf-taujet', help='Construct the tau-jet PDFs', action='store_true', default=False)
    parser.add_argument('--pdf-mass', help='Construct the invariant mass PDFs', action='store_true', default=False)
    parser.add_argument("--out-file", help='Name of output root file', default="FakeFactors.root")
    parser.add_argument('--new-range', help='Apply new range to PDFs', action='store_true', default=False)
    parser.set_defaults(outputDir="./FakeFactorFiles/")
    return parser.parse_args()


def Output(Options):

    if os.path.isdir(Options.outputDir) == False:
        os.system("mkdir -p " + Options.outputDir)

    if os.path.exists("%s/%s" % (Options.outputDir, Options.out_file)):
        os.system("rm %s/%s" % (Options.outputDir, Options.out_file))

    if not Options.out_file.endswith(".root"):
        print "ERROR: No valid outfile name %s. The file must end with .root" % (Options.out_file)
        return False

    return True


def getall(d, basepath="/"):
    "Generator function to recurse into a ROOT file/dir and yield (path, obj) pairs"
    for key in d.GetListOfKeys():
        kname = key.GetName()
        if key.IsFolder():
            # TODO: -> "yield from" in Py3
            for i in getall(d.Get(kname), basepath + kname + "/"):
                yield i
        else:
            yield basepath + kname, d.Get(kname)


def getfolders(dire):

    folders = []
    for key in dire.GetListOfKeys():
        if "TDirectoryFile" in key.GetClassName():
            folders.append(key)
    return folders


def gethistos(dire):

    histos = []
    for key in dire.GetListOfKeys():
        if "TH1" in key.GetClassName():
            histos.append(key)
    return histos


def PrintStructure(f):

    if not f.cd():
        print "Error: cannot enter in file :", f.GetName()

    for fold in getfolders(ROOT.gDirectory):
        print fold.GetName()
        if not ROOT.gDirectory.cd(fold.GetName()):
            print "Cannot enter in ", fold.GetName()
        for subfold in getfolders(ROOT.gDirectory):
            if not ROOT.gDirectory.cd(subfold.GetName()):
                print "Cannot enter in ", subfold.GetName()
            histos = gethistos(ROOT.gDirectory)
            if len(histos) > 1:
                print "Warning: PrintStructure - this folder has multiple histos %s / %s" % (subfold.GetName(), subfold.GetName())
            for histo in histos:
                #print ROOT.gDirectory.GetPathStatic()
                h = f.Get(ROOT.gDirectory.GetPathStatic() + "/" + histo.GetName())
                print "\tHisto=%s Entries=%i Area=%f" % (subfold.GetName(), int(h.GetEntries()), h.GetEntries())
            ROOT.gDirectory.cd("../")
        ROOT.gDirectory.cd("../")


def allowed_analysis(analysis):
    return analysis == "FakeFactor_Nominal"


def allowed_region(region):
    return region == "Z#mu#mu"


def allowed_sample(samplename):
    allowedHistoSetSamples = ['SumBG', 'Data']
    return (samplename in allowedHistoSetSamples)


def allowed_taujet_particle(varname):
    return varname.startswith("Tau_")


def allowed_taujet_quantity(varname):

    for var in global_allowed_taujet_variables:
        if varname.endswith(var):
            return True
    return False


def allowed_taujet_variable(varname, use_data=False):
    allowed_definition = global_tau_definition
    allowed_data_origin = 'Incl'
    tokens = varname.split("_")

    if tokens[0] != "Tau":
        return False

    if tokens[-1] not in global_allowed_taujet_variables:
        return False

    for token in tokens:
        if 'def' in token:
            if allowed_definition not in token:
                return False
        if use_data:
            if 'orig' in token:
                if allowed_data_origin not in token:
                    return False

    return True


def allowed_for_taujet_pdf(variable, UseData):
    if not allowed_taujet_particle(variable):
        return False

    if not allowed_taujet_quantity(variable):
        return False

    if not allowed_taujet_variable(variable, UseData):
        return False

    return True


def allowed_mass_source(varname):
    return varname.startswith("ZBosonWide_")


def allowed_mass_quantity(variable):
    return variable.split("_")[-1] == "mass"


def allowed_for_mass_pdf(variable):
    if not allowed_mass_source(variable):
        return False

    if not allowed_mass_quantity(variable):
        return False

    return True


def var_is_width(var):
    return var.split('_')[-1] == 'width'


def var_is_ntrks(var):
    return var.split('_')[-1] == 'ntrks'


def var_is_mass(var):
    return var.split('_')[-1] == 'mass'


def CreateHistoSet(options, samples, analysis, region, variable, useData):
    hset = XAMPPplotting.PlottingHistos.CreateHistoSets(Options=options,
                                                        analysis=analysis,
                                                        region=region,
                                                        var=variable,
                                                        UseData=useData,
                                                        skip_consistency=True)

    return hset[0] if hset else None  #Note: element 0


def GetFS(Options=None):
    return XAMPPplotting.FileStructureHandler.GetStructure(Options)


def MakeHistogramsTauJet(outfile, Samples, Options, UseData=False):

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = GetFS(Options)

    #analyze structure
    if not FileStructure:
        print "Error: cannot create file structure for UseData = ", UseData
        return False

    for analysis in FileStructure.GetConfigSet().GetAnalyses():
        if not allowed_analysis(analysis):
            continue
        else:
            print "Info: allowed analysis '%s' is allowed" % (analysis)
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(analysis):
            if not allowed_region(region):
                continue
            else:
                print "Info: allowed region '%s' is allowed" % (region)

            for variable in FileStructure.GetConfigSet().GetVariables(analysis, region):

                # a tau-jet variable
                if not allowed_for_taujet_pdf(variable, UseData):
                    continue
                else:
                    print "Info: allowed variable '%s' is allowed" % (variable)

                #get histo set
                HistoSet = CreateHistoSet(Options, Samples, analysis, region, variable, UseData)

                if not HistoSet:
                    print "Warning: MakeHistograms - skipping set for", analysis, region, variable, UseData
                    continue

                #get histos
                Histo = None
                if UseData:
                    Histo = HistoSet.GetData()[0] if HistoSet.GetData() else None  # Note: 0 element of SampleHisto list
                else:
                    Histo = HistoSet.GetSummedBackground()

                if not Histo:
                    print "Wanring: MakeHistograms - skipping set for", analysis, region, variable, UseData
                    continue

                #sample name
                sample = Histo.GetName()

                if not allowed_sample(sample):
                    continue

                #range
                xmin = 0.
                xmax = 0.
                if var_is_width(variable):
                    xmin = 0.
                    xmax = 0.22
                elif var_is_ntrks(variable):
                    xmin = 4.
                    xmax = 32.

                #fill variable
                if not FillVariable('taujet', outFile, sample, analysis, region, variable, Histo.GetHistogram(), Options.new_range, xmin,
                                    xmax):
                    print "Error: cannot fill histo of sample %s and variable %s" % (sample, variable)
                    return False
    return True


def MakeHistogramsMass(outfile, Samples, Options, UseData=False):

    print "Analyzing mass histograms..."

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = GetFS(Options)

    if not FileStructure:
        print "Error: cannot create file structure for UseData = ", UseData
        return False

    for analysis in FileStructure.GetConfigSet().GetAnalyses():
        if not allowed_analysis(analysis):
            continue

        print "Allowed analysis:", analysis

        for region in FileStructure.GetConfigSet().GetAnalysisRegions(analysis):
            if not allowed_region(region):
                continue

            print "Allowed region:", region

            for variable in FileStructure.GetConfigSet().GetVariables(analysis, region):

                # a mass variable
                if not allowed_for_mass_pdf(variable):
                    continue

                print "Allowed variable:", variable

                #get histo set
                HistoSet = CreateHistoSet(Options, Samples, analysis, region, variable, UseData)

                if not HistoSet:
                    print "Warning: MakeHistograms - skipping set for", analysis, region, variable, UseData
                    continue

                #get histos in range
                xmin = 0.
                xmax = 0.
                if var_is_mass(variable):
                    xmin = 60.
                    xmax = 120.
                #data
                if UseData:
                    print "TMP data"
                    Histo = None
                    Histo = HistoSet.GetData()[0] if HistoSet.GetData() else None  # Note: 0 element of SampleHisto list
                    if not Histo:
                        print "Warning: MakeHistograms - skipping set for", analysis, region, variable, UseData
                        continue
                    sample = Histo.GetName()
                    if not FillVariable('mass', outFile, sample, analysis, region, variable, Histo.GetHistogram(), Options.new_range, xmin,
                                        xmax):
                        print "Error: cannot fill DATA histo of sample %s and variable %s" % (sample, variable)
                        return False
                #mc
                else:
                    print "TMP mc"
                    #Histo = HistoSet.GetSummedBackground()
                    sample_names = HistoSet.GetSampleNames()
                    print "TMP", sample_names
                    for sample in sample_names:
                        Histo = None
                        Histo = HistoSet.GetSample(sample)
                        if not Histo:
                            print "Warning: MakeHistograms - skipping set for", analysis, region, variable, sample, UseData
                            continue

                        if not FillVariable('mass', outFile, sample, analysis, region, variable, Histo.GetHistogram(), Options.new_range,
                                            xmin, xmax):
                            print "Error: cannot fill DATA histo of sample %s and variable %s" % (sample, variable)
                            return False

    return True


def MakeHistograms(outfile, Samples, Options, UseData=False):

    st = False

    if Options.pdf_taujet:
        st = MakeHistogramsTauJet(outfile, Samples, Options, UseData)

    if Options.pdf_mass:
        st = MakeHistogramsMass(outfile, Samples, Options, UseData)

    return st


def SetRange(Samples, lowx, lowy):
    return True


if __name__ == "__main__":

    Options = ParsedOptions()

    if not Output(Options):
        sys.exit("Error: cannot set up output. Exiting...")

    if not Options.pdf_taujet and not Options.pdf_mass:
        sys.exit("Error: a PDF type must be defined. Exiting...")

    #prepare output file
    outFile = ROOT.TFile("%s/%s" % (Options.outputDir, Options.out_file), "RECREATE")

    # if one or more DSConfigs are given from outside, use them
    Samples = XAMPPplotting.Utils.getSamplesToUse(Options.config)

    if not MakeHistograms(outFile, Samples, Options, UseData=False):
        sys.exit("Error: cannot create histos for MC. Exiting...")

    if not MakeHistograms(outFile, Samples, Options, UseData=True):
        sys.exit("Error: cannot create histos for MC and Data. Exiting...")

    if not StoreLumiHistos(Options.lumi * 1e3, outFile):  # lumi in pb^-1
        sys.exit("Error: cannot store the Lumi histos needed for HistFactory. Exiting...")

    if outFile:
        PrintStructure(outFile)
        outFile.Close()
        print "Info: output file '%s' " % (outFile.GetName())

    print "End!"

import ROOT
import argparse
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
import collections

##
# @brief global default particles, fake types and quantities
# Can be overriden
#
analyses = ['FakeFactor']

analysis_regions = [
    "Z#mu#mu", "Zee", "CRt#bar{t}", "Z#mu#mu_tau", "ttbar", "FakeFactor", "2L1l1t", "2L1l1tnoZ", "2L1l1tZ", "2L1T1l", "2L1T1lnoZ",
    "2L1T1lZ", "2L1T1t", "2L1T1tnoZ", "2L1T1tZ", "2L2t", "2L2tnoZ", "2L2tZ", "3L1t", "3L1tnoZ", "3L1tZ", "2L2l", "2L2lnoZ", "2L2lZ", "3L1l",
    "3L1lnoZ", "3L1lZ", "2L1l", "2L1lZ", "2L1lnoZ", "1L2l", "1L2lZ", "1L2lnoZ", "4L", "4L-ZZ", "4LZ", "4LnoZ", "3L1T", "3LZ", "3LnoZ",
    "2L2T", "2L2TZ", "2L2TnoZ"
]
fake_types = ["Incl", "HF", "LF", "Gluon", "Unmatched", "Conv", "Real", "Fake", "Elec", "Muon", "CONV"]

fake_type_dupes = []
fake_quantities = [
    "pt", "eta", "phi", "bdt", "pteta", "bdtTrans", "ptplot", "ptetaplot", "ptmu", "ptmeff", "ptcheck", "etacheck", "meffcheck", "mucheck",
    "metcheck", "ptetacheck", "ptmeffcheck", "ptmucheck", "ptmetcheck", "sfeta", "sfpteta", "sfptetaplot", "lfpt", "lfpteta", "lfptplot",
    "lfptetaplot", "hfpt", "hfpteta", "hfptplot", "hfptetaplot"
]
particles = ["Electrons", "Muons", "Tau"]
prongs = ["1P", "3P", "Incl"]
definitions = ["Loose", "Signal"]


##
# @brief Root setup
#
def SetRoot():
    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")
    return True


#
##
#  Cross check input helper function
#
def CrossCheck(inlist=[], alist=[], lname=''):
    if inlist:
        for x in inlist:
            if not x in alist:
                sys.exit("'{}' is not a valid choice for {}! Choose something from {}".format(x, lname, str(alist)))


def CrossCheckItem(item='', alist=[], lname=''):
    if not item in alist:
        sys.exit("'{}' is not a valid choice for {}! Choose something from {}".format(item, lname, str(alist)))


##
# options
#
def GetOptions():
    global analyses
    global fake_types
    global fake_type_dupes
    global fake_quantities
    global particles
    global analysis_regions
    global prongs
    global definitions
    parser = argparse.ArgumentParser(description='This script produces fake ratio files \"python CalculateFakeFactors.py -h\"',
                                     prog='CalculateFakeRatios',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = XAMPPplotting.Utils.setupBaseParser(parser)
    parser.add_argument("--outFile", help='Output root file', default="FakeFactors.root")
    parser.add_argument('--fake-types', help='Fake types. Possible options: ' + str(fake_types), type=str, nargs='+')
    parser.add_argument('--exclude-fake-types', help='Exclude Fake types. Possible options: ' + str(fake_types), type=str, nargs='+')
    parser.add_argument('--fake-type-dupes-target',
                        help='Duplicate the result to these fake types. Possible options: ' + str(fake_types),
                        type=str,
                        nargs='+')
    parser.add_argument("--fake-type-dupes-source",
                        help='Duplicate this fake type. Possible options: ' + str(fake_types),
                        default="",
                        type=str)
    parser.add_argument('--fake-quantities', help='Fake quantities. Possible options: ' + str(fake_quantities), type=str, nargs='+')
    parser.add_argument('--particles', help='Particles. Possible options: ' + str(particles), type=str, nargs='+')
    parser.add_argument('--analyses', help='Analyses. Possible options: ' + str(analyses), type=str, nargs='+')
    parser.add_argument('--analysis-regions', help='Analysis regions. Possible options: ' + str(analysis_regions), type=str, nargs='+')
    parser.add_argument('--prongs', help='Prongs. Possible options: ' + str(prongs), type=str, nargs='+')
    parser.add_argument('--definitions', help='Definitions. Possible options: ' + str(definitions), type=str, nargs='+')
    parser.add_argument('--anti-scale-mc', help='Scale MC by the inverse data lumi', action='store_true', default=False)
    parser.add_argument('--background-subtraction', help='Scale MC by the inverse data lumi', action='store_true', default=False)
    parser.add_argument('--only-sum-bkg', help='Consider only the SumBG in plotting', action='store_true', default=False)
    parser.add_argument('--CalculateFakefactor', help='Fakefactors are calculated and plotted', action='store_true', default=False)
    parser.add_argument('--CalculateScalefactor', help='Scalefactors are calculated and plotted', action='store_true', default=False)
    parser.add_argument(
        '--performrebinning',
        help=
        'fakefactor histograms are rebinned in such a way that the uncertainty in each bin is below the threshold defined with --rebinning_maxUncertainty',
        action='store_true',
        default=False)
    parser.add_argument('--rebinning_maxUncertainty',
                        help='defines the maximum uncertainty in each bin when performing histogram rebinning',
                        default=0.2,
                        type=float)

    parser.set_defaults(outputDir="./FakeFactorFiles/")

    #parse options
    try:
        options = parser.parse_args()
    except:
        parser.print_help()
        exit(1)

    #check and override type dupes (goes first)
    if options.fake_type_dupes_target:
        CrossCheck(options.fake_type_dupes_target, fake_types, "Type Duplications - target")
        fake_type_dupes_target = options.fake_type_dupes_target

    #check and override type dupes (goes first)
    if options.fake_type_dupes_source:
        CrossCheckItem(options.fake_type_dupes_source, fake_types, "Type Duplications- source")
        fake_type_dupes_source = options.fake_type_dupes_source

    #check and override analyses
    if options.analyses:
        CrossCheck(options.analyses, analyses, "Analyses")
        analyses = options.analyses

    #check and override types
    if options.definitions:
        CrossCheck(options.definitions, definitions, "Definitions")
        definitions = options.definitions

    #check and override types
    if options.fake_types:
        CrossCheck(options.fake_types, fake_types, "Types")
        fake_types = options.fake_types

    #check and override types
    if options.exclude_fake_types:
        for itype in options.exclude_fake_types:
            if itype in fake_types:
                fake_types.remove(itype)

    #check and override prongs
    if options.prongs:
        CrossCheck(options.prongs, prongs, "Prongs")
        prongs = options.prongs

    #check and override quantities
    if options.fake_quantities:
        CrossCheck(options.fake_quantities, fake_quantities, "Quantities")
        fake_quantities = options.fake_quantities

    #check and override particles
    if options.particles:
        CrossCheck(options.particles, particles, "Particles")
        particles = options.particles

    #check and override analysis regions
    if options.analysis_regions:
        CrossCheck(options.analysis_regions, analysis_regions, "analysis regions")
        analysis_regions = options.analysis_regions

    return options


# output folder
def SetOutputFolder(Options):

    if os.path.isdir(Options.outputDir) == False:
        os.system("mkdir -p " + Options.outputDir)

    if os.path.exists("%s/%s" % (Options.outputDir, Options.outFile)):
        os.system("rm %s/%s" % (Options.outputDir, Options.outFile))

    if not Options.outFile.endswith(".root"):
        print "ERROR: No valid outfile name %s. The file must end with .root" % (Options.outFile)
        return False

    return True


#get file structure
def FileStructure(Options):

    FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(Options)

    if not FileStructure:
        sys.exit("Error: Exiting at FileStructure ...")

    if not issubclass(type(FileStructure), XAMPPplotting.FileStructureHandler.FileStructHandler):
        sys.exit("Error: Object is not FileStructure but '{}'...".format(type(self.file_structure)))

    print "Info: file structure looks OK..."

    return FileStructure

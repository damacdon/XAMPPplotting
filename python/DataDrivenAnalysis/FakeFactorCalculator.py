from ClusterSubmission.Utils import FillWhiteSpaces
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
from XAMPPplotting.FileUtils import ResolvePath, CreateDirectory
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
import collections

# #
# @brief global default particles, fake types and quantities
# Can be overriden
#

fake_types = ["LF", "CONV", "HF", "Unmatched", "Real"]

fake_quantities = ["pt", "eta", "phi"]
particles = ["Electrons", "Muons"]


# #
# @brief Root setup
#
def SetRoot():
    try:
        ROOT.gROOT.SetBatch(True)
        ROOT.gROOT.Macro("rootlogon.C")
        ROOT.gROOT.SetStyle("ATLAS")
    except:
        return False
    return True


def GetOptions():
    parser = argparse.ArgumentParser(description='This script produces fake ratio files \"python CalculateFakeFactors.py -h\"',
                                     prog='CalculateFakeRatios',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = XAMPPplotting.Utils.setupBaseParser(parser)
    parser.add_argument("--outFile", help='Output root file', default="FakeFactors.root")
    parser.add_argument('--anti-scale-mc', help='Scale MC by the inverse data lumi', action='store_true', default=False)
    parser.add_argument('--background-subtraction', help='Subtract the background from the data', action='store_true', default=False)
    parser.add_argument('--fake-type', help="Calculate the  desired fake-type", default="HF")
    parser.add_argument('--SignalSample', help="Which sample is the signal one", required=True)
    parser.set_defaults(outputDir="./FakeFactorFiles/")

    options = parser.parse_args()
    return options


def LoadHistoSet(Options, Analysis, Region, Variable, UseData):
    Sets = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, Analysis, Region, Variable, UseData=UseData)
    if Sets:
        return Sets[0]
    return None


# output folder
def SetOutputFolder(Options):

    CreateDirectory(Options.outputDir, False)
    if not Options.outFile.endswith(".root"):
        print "ERROR: No valid outfile name %s. The file must end with .root" % (Options.outFile)
        return False

    return True


def PairVariables(UnPaired):
    Pairs = []
    for Signal in UnPaired:
        if not "defSignal" in Signal: continue
        Found = False
        for Loose in UnPaired:
            if not Loose == Signal and Loose.replace("Loose", "Signal") == Signal:
                Pairs.append((Signal, Loose))
                Found = True
                break
        if not Found:
            print "WARNING: Could not find a loose variable for " + Signal
    return Pairs


class FakeFactors(object):
    def __init__(self, Options, Analysis, Region, SignalVar, LooseVar):
        self.__LooseVarSet, self.__LooseTruthTypes = self.__CreateSets(Options, Analysis, Region, LooseVar)
        self.__SignalVarSet, self.__SignalTruthTypes = self.__CreateSets(Options, Analysis, Region, SignalVar)
        self.__Options = Options
        self.__MCRatioHist = None
        self.__DataRatioHist = None

    def __CreateSets(self, Options, Analysis, Region, Variable):
        InclusiveSet = LoadHistoSet(Options, Analysis, Region, Variable, True)
        TruthTypeSet = {}
        global fake_types
        for Type in fake_types:
            if "Incl" not in Type:
                Set = LoadHistoSet(Options, Analysis, Region, Variable.replace("origIncl", "orig" + Type), False)
                if Set and InclusiveSet and InclusiveSet.GetLumi() > 0.:
                    Set.SetLumi(InclusiveSet.GetLumi())
                    TruthTypeSet[Type] = Set
        return InclusiveSet, TruthTypeSet

    def __SubtractBkg(self, InclusiveSet, TruthTypeSet):
        if not self.__Options.background_subtraction or not self.IsValid():
            return
        for data in InclusiveSet.GetData():
            for bkg in InclusiveSet.GetBackgrounds():
                if bkg.GetName() == self.SignalSmp():
                    continue
                print "Bin 6 %.2f and Bin 7  %.2f before subtraction of %s " % (data.GetBinContent(6), data.GetBinContent(7), bkg.GetName())
                data.Add(bkg, -1)
                print "Bin 6 %.2f and Bin 7 %.2f after subtraction of %s " % (data.GetBinContent(6), data.GetBinContent(7), bkg.GetName())

        if Options.fake_type == "":
            return
        for Type, Set in TruthTypeSet.iteritems():
            if Type == Options.fake_type:
                continue
            TypeHisto = Set.GetSample(self.SignalSmp())
            for data in InclusiveSet.GetData():
                print "Bin 6 %.2f and Bin 7 %.2f before subtraction of %s " % (data.GetBinContent(6), data.GetBinContent(7), Type)
                data.Add(TypeHisto, -1)
                print "Bin 6 %.2f and Bin 7  %.2f after subtraction of %s " % (data.GetBinContent(6), data.GetBinContent(7), Type)

            InclusiveSet.GetSample(self.SignalSmp()).Add(TypeHisto, -1)

    def IsValid(self):
        if not self.__LooseVarSet or not self.__SignalVarSet:
            return False
        if not self.__LooseVarSet.CheckHistoSet() or not self.__SignalVarSet.CheckHistoSet():
            return False
        if not self.__LooseVarSet.GetSample(self.SignalSmp()) or not self.__LooseVarSet.GetSample(self.SignalSmp()).isValid():
            return False
        if not self.__SignalVarSet.GetSample(self.SignalSmp()) or not self.__SignalVarSet.GetSample(self.SignalSmp()).isValid():
            return False
        return True

    def Purity(self):
        Loose = self.__PrintFakePurity(self.__LooseVarSet, self.__LooseTruthTypes)
        Signal = self.__PrintFakePurity(self.__SignalVarSet, self.__SignalTruthTypes)

        Max_WidthValS = Signal["Styling"]["Val"]
        Max_WidthValL = Loose["Styling"]["Val"]
        Max_WidthErrS = Signal["Styling"]["Err"]
        Max_WidthErrL = Loose["Styling"]["Err"]
        Max_WidthDatS = Signal["Styling"]["Data"]
        Max_WidthDatL = Loose["Styling"]["Data"]
        Max_BinWidth = Loose["Styling"]["Width"]

        print "##################################################################################"
        print "                    %s" % (self.GetName())
        print "##################################################################################"

        for i in range(self.GetLooseMCHisto().GetNbins()):
            BinWidth = Loose[i]["Width"]
            LooseVal = Loose[i]["Val"]
            SignalVal = Signal[i]["Val"]
            LooseData = Loose[i]["Data"]
            SignalData = Signal[i]["Data"]
            Loose_WidthVal = Max_WidthValL - len("%.2f" % (LooseVal))
            Signal_WidthVal = Max_WidthValS - len("%.2f" % (SignalVal))

            LooseErr = Loose[i]["Err"]
            SignalErr = Signal[i]["Err"]
            Loose_WidthErr = Max_WidthErrL - len("%.2f" % (LooseErr))
            Signal_WidthErr = Max_WidthErrS - len("%.2f" % (SignalErr))
            Loose_WidthData = Max_WidthDatL - len("%.2f" % (LooseData))
            Signal_WidthData = Max_WidthDatS - len("%.2f" % (SignalData))

            Str = "Bin: %02d (%.2f)%s  %.2f%s (%.2f)%s     pm  %.2f%s (%.2f)%s  | %.2f%s (%.2f)%s  |" % (
                i,
                BinWidth,
                FillWhiteSpaces(Max_BinWidth - len("%.2f" % BinWidth)),
                LooseVal,
                FillWhiteSpaces(Loose_WidthVal),
                SignalVal,
                FillWhiteSpaces(Signal_WidthVal),
                LooseErr,
                FillWhiteSpaces(Loose_WidthErr),
                SignalErr,
                FillWhiteSpaces(Signal_WidthErr),
                LooseData,
                FillWhiteSpaces(Loose_WidthData),
                SignalData,
                FillWhiteSpaces(Signal_WidthData),
            )
            for j in range(len(Loose[i]["Comp"])):
                Str += "     %s:  %02.2f%% ( %02.2f%%)" % (Loose[i]["Comp"][j][0], Loose[i]["Comp"][j][1], Signal[i]["Comp"][j][1])
            print Str

    def __PrintFakePurity(self, Set, HistoSet):
        PurityMap = {"Styling": {"Val": 0, "Err": 0, "Comp": 0, "Data": 0, "Width": 0}}
        for i in range(Set.GetSample(self.SignalSmp()).GetNbins()):
            PurityMap[i] = {}
            #             Content = Set.GetSample(self.SignalSmp()).GetBinContent(i)
            #             Error = Set.GetSample(self.SignalSmp()).GetBinError(i)
            Content = Set.GetSummedBackground().GetBinContent(i)
            Error = Set.GetSummedBackground().GetBinError(i)
            Data = Set.GetData()[-1].GetBinContent(i)
            PurityMap[i]["Val"] = Content
            PurityMap[i]["Err"] = Error
            PurityMap[i]["Data"] = Data
            PurityMap[i]["Width"] = Set.GetSummedBackground().GetHistogram().GetXaxis().GetBinWidth(i)

            PurityMap["Styling"]["Val"] = XAMPPplotting.Utils.GetMaximumWidth("%.2f" % (Content), PurityMap["Styling"]["Val"])
            PurityMap["Styling"]["Err"] = XAMPPplotting.Utils.GetMaximumWidth("%.2f" % (Error), PurityMap["Styling"]["Err"])
            PurityMap["Styling"]["Data"] = XAMPPplotting.Utils.GetMaximumWidth("%.2f" % (Data), PurityMap["Styling"]["Data"])
            PurityMap["Styling"]["Width"] = XAMPPplotting.Utils.GetMaximumWidth(
                "%.2f" % (Set.GetSummedBackground().GetHistogram().GetXaxis().GetBinWidth(i)), PurityMap["Styling"]["Width"])

            PurityMap[i]["Comp"] = []
            TotFraction = 0.
            for type, FakSet in HistoSet.iteritems():
                PurityMap["Styling"]["Comp"] = XAMPPplotting.Utils.GetMaximumWidth(type, PurityMap["Styling"]["Comp"])
                FakeContent = FakSet.GetSample(self.SignalSmp()).GetBinContent(i)
                #                 FakeContent = FakSet.GetSummedBackground().GetBinContent(i)

                SmpFraction = 100 * FakeContent / Content if Content > 0 else 0.
                PurityMap[i]["Comp"].append((type, SmpFraction))
                TotFraction += SmpFraction
            PurityMap[i]["TotFrac"] = str(TotFraction)
        return PurityMap

    def GetName(self):
        return self.__LooseVarSet.GetVariable().replace("defLoose", "defRatio")

    def SignalSmp(self):
        return self.__Options.SignalSample

    def CalculateRatio(self):
        if self.__MCRatioHist:
            return

        self.__SubtractBkg(self.__LooseVarSet, self.__LooseTruthTypes)
        self.__SubtractBkg(self.__SignalVarSet, self.__SignalTruthTypes)

        self.__MCRatioHist = XAMPPplotting.Utils.CreateRatioHisto(
            self.__SignalVarSet.GetSample(self.SignalSmp()), self.__LooseVarSet.GetSample(self.SignalSmp()),
            1. if not self.__Options.anti_scale_mc else 1. / self.__LooseVarSet.GetLumi())
        self.__MCRatioHist.GetYaxis().SetTitle(self.__MCRatioHist.GetYaxis().GetTitle().replace("Entries", "%s - FF" % (Options.fake_type)))
        self.__DataRatioHist = XAMPPplotting.Utils.CreateRatioHisto(self.__SignalVarSet.GetData()[0], self.__LooseVarSet.GetData()[0])
#        self.TakeAbsRatioValue(self.__MCRatioHist)
#        self.TakeAbsRatioValue(self.__DataRatioHist)

    def TakeAbsRatioValue(self, Histo):
        for bin in range(XAMPPplotting.Utils.GetNbins(Histo)):
            if Histo.GetBinContent(bin) < 0:
                Histo.SetBinContent(bin, 1.)
                Histo.SetBinError(bin, 0.25)

    def GetMCRatio(self):
        return self.__MCRatioHist

    def GetDataRatio(self):
        return self.__DataRatioHist

    def GetLooseDataHisto(self):
        return self.__LooseVarSet.GetData()[0]

    def GetSignalDataHisto(self):
        return self.__SignalVarSet.GetData()[0]

    def GetLooseMCHisto(self):
        return self.__LooseVarSet.GetSample(self.SignalSmp())

    def GetSignalMCHisto(self):
        return self.__SignalVarSet.GetSample(self.SignalSmp())


# run main
if __name__ == "__main__":

    Options = GetOptions()
    FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(Options)

    if not SetOutputFolder(Options):
        sys.exit("Error: Exiting at SetOutputFolder ...")

    if not SetRoot():
        sys.exit("Error: Exiting at SetRoot ...")

    OutFileData = ROOT.TFile("%s/Data.root" % (Options.outputDir), "RECREATE")
    OutFileMC = ROOT.TFile("%s/MC.root" % (Options.outputDir), "RECREATE")

    for ana in FileStructure.GetConfigSet().GetAnalyses():
        folder_name = ana.replace("_Nominal", "")
        OutFileData.mkdir("%s_Nominal" % (folder_name))
        OutFileMC.mkdir("%s_Nominal" % (folder_name))

        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            OutFileData.mkdir("%s_Nominal/%s/" % (folder_name, region))
            OutFileMC.mkdir("%s_Nominal/%s/" % (folder_name, region))

            Pairs = PairVariables(FileStructure.GetConfigSet().GetVariables(ana, region))
            for Signal, Loose in Pairs:
                #### Ignore the inclusive origin
                if not "origIncl" in Signal: continue
                if "Tau" in Signal: continue
                Calculator = FakeFactors(Options, ana, region, Signal, Loose)

                if not Calculator.IsValid():
                    continue

                Calculator.Purity()
                Calculator.CalculateRatio()
                OutFileMC.cd("%s_Nominal/%s/" % (folder_name, region))
                Calculator.GetMCRatio().Write(Calculator.GetName())

                Calculator.GetLooseMCHisto().SetLumi(1.)
                AxisTitle = Calculator.GetLooseMCHisto().GetHistogram().GetYaxis().GetTitle().replace("Entries", "Loose")
                Calculator.GetLooseMCHisto().GetHistogram().GetYaxis().SetTitle(AxisTitle)
                Calculator.GetSignalMCHisto().SetLumi(1.)
                AxisTitle = Calculator.GetSignalMCHisto().GetHistogram().GetYaxis().GetTitle().replace("Entries", "Signal")
                Calculator.GetSignalMCHisto().GetHistogram().GetYaxis().SetTitle(AxisTitle)

                Calculator.GetLooseMCHisto().Write()
                Calculator.GetSignalMCHisto().Write()

                OutFileData.cd("%s_Nominal/%s/" % (folder_name, region))
                Calculator.GetDataRatio().Write(Calculator.GetName())
                Calculator.GetLooseDataHisto().Write()
                Calculator.GetSignalDataHisto().Write()

    OutFileData.Close()
    OutFileMC.Close()

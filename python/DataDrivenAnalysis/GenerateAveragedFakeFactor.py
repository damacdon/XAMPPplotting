#! /usr/bin/env python
import ROOT
from array import array
import argparse
from XAMPPplotting.Defs import *

from XAMPPplotting.FakeFactorCalculator import SetRoot, SetOutputFolder, LoadHistoSet
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
import XAMPPplotting.FileStructureHandler

########################################################################################
# #  This class is written to calculate final averaged fake factors for the analysis
# #  It uses the python infrastructure of the XAMPPplotting package developed at MPP
# #  In order to use the macro the user has to create a XAMPPplotting DSconfig file
# #  Each process is saved in a seperate ROOT file which has these three directories
# #      FakeFactor_Nominal/
# #      ScaleFactor_Nominal/
# #      ProcessFractions_Nominal/
# #  The fake-factor and scale-factor histogram go in to the two first directories which
# #  is clear, I think. The ProcessFraction directory may contain either the actual process
# #  -fractions distributional histograms for this particular process & fake-type. The code
# #  takes care of the proper normalization.
# #  The directories contain further subdiectories indicating specific analysis selection
# #  where the fake-factors are calculated for e.g.
# #        FakeFactor_Nominal/
# #                CR1/
# #                CR2/
# #                etc.
# #  This is the directory where the fake-factor histograms are stored to be combined.
# #  The histograms should follow some kind of naming convention:
# #      <Particle>_Orig<FakeType>_<VariableConfigurations>
# #  The user can provide diffrent kinds of configurations to the code:
# #       1.) The histograms have already the final format and should only be combined:
# #                <Particle>_Orig<FakeType>_(x/y/z)Var_<Variable1>_(y/z)Var_<Variable2>
# #            Out of the FakeFactor/ScaleFactor/ProcessFractions the code searches for the
# #            histogram with the highest dimensionality and tries to fold the other histograms
# #            to fold into this histo along the correspondig directions E.g.
# #                FakeFactorHisto:
# #                    Taus_OrigHF_xVar_Pt_yVar_Prong_zVar_Eta
# #                ProcessFractionHisto:
# #                    Taus_OrigHF_xVar_Pt_yVar_Eta
# #                ScaleFactorHisto:
# #                    Taus_OrigHF_xVar_Prong
# #            The process fraction histogram is folded in the xy plane with the ProcessFractions
# #            and along the z-direction with the ScaleFactors
# #        2.) The script should build the histogram out of 1D/2D histograms:
# #                Orig<FakeType>_<Particle>_xVar<Variable>_(y/z)Binned_<Variable>_<LowEdge>_<HighEdge>
# #            The histogram is then assembled for the particular type and the code proceeds then with
# #            Option 1.)
# #
# #


############################################################################################
# #                                Setup the argument parser                               ##
############################################################################################
def SetupParser():
    parser = argparse.ArgumentParser(description='This script produces fake ratio files \"python CalculateFakeFactors.py -h\"',
                                     prog='CalculateFakeRatios',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = XAMPPplotting.Utils.setupBaseParser(parser)
    parser.add_argument("--outFile", help='Output root file', default="WeightedFF.root")
    parser.add_argument("--FakeFactorAnalysis", help="Name of the analysis folder where the fake factors are stored", default="FakeFactor")
    parser.add_argument("--ScaleFactorAnalysis",
                        help="Name of the analysis folder where the scale factors are stored",
                        default="ScaleFactor")
    parser.add_argument("--ProcessFractionAnalysis",
                        help="Name of the analysis folder where the process fractions are stored",
                        default="ProcessFractions")
    parser.set_defaults(outputDir="./AveragedFF/")
    #     parser.set_defaults(noSyst=True)
    parser.set_defaults(noSignal=True)
    return parser.parse_args()


def SplitIntoParticles(InputVarList):
    SplitVars = {}
    for Var in InputVarList:
        Part = Var.split("_")[0]
        if Part not in SplitVars.iterkeys():
            SplitVars[Part] = []
        SplitVars[Part].append(Var)

    return SplitVars


################################################################################
#   This class decodes all properties in the histogram string as
#   described above and forms the basis of the macro
################################################################################
class FakeHistoVariable(object):
    def __init__(self, Variable):
        self.__Var = Variable
        self.__FakeType = None
        self.__Process = None
        self.__xVariable = None
        self.__yVariable = None
        self.__zVariable = None
        self.__BinnedInY = None
        self.__BinnedInZ = None
        Properties = Variable.split("_")
        for idx in range(len(Properties)):
            property = Properties[idx]
            if property.startswith("Orig"):
                self.__FakeType = property[4:]
            if property.startswith("xVar"):
                self.__xVariable = Properties[idx + 1]
                idx = idx + 1
            elif property.startswith("yVar"):
                self.__yVariable = Properties[idx + 1]
                idx = idx + 1
            elif property.startswith("zVar"):
                self.__zVariable = Properties[idx + 1]
                idx = idx + 1
            elif property.startswith("yBinned"):
                self.__yVariable = Properties[idx + 1]
                self.__BinnedInY = (float(Properties[idx + 2]), float(Properties[idx + 3]))
                idx = idx + 3
            elif property.startswith("zBinned"):
                self.__zVariable = Properties[idx + 1]
                self.__BinnedInZ = (float(Properties[idx + 2]), float(Properties[idx + 3]))
                idx = idx + 3

    def FakeType(self):
        return self.__FakeType

    def GetName(self):
        return self.__Var

    def xAxis(self):
        return self.__xVariable

    def yAxis(self):
        return self.__yVariable

    def zAxis(self):
        return self.__zVariable

    ###################################################
    #  Get the axes dependent on an input integer     #
    ###################################################
    def GetAxis(self, x):
        if x == 0:
            return self.xAxis()
        elif x == 1:
            return self.yAxis()
        elif x == 2:
            return self.zAxis()
        print "Unknown axis " + str(x)
        return None

    ###################################################################
    #    Check in which direction the histogram is dependent          #
    #    as a function of the particle variable e.g. pt, eta, prongs  #
    ###################################################################
    def iDependentOn(self, Var):
        if Var == self.xAxis():
            return 0
        elif Var == self.yAxis():
            return 1
        elif Var == self.zAxis():
            return 2
        return -1

    ##############################################################
    #  Functionallty to assemble the fake-distribution from different
    #  inputs
    #############################################################
    def isSliceInY(self):
        return self.__BinnedInY is not None

    def isSliceInZ(self):
        return self.__BinnedInZ is not None

    def isSlice(self):
        return self.isSliceInY() or self.isSliceInY()

    def YsliceEdges(self):
        return self.__BinnedInY

    def ZsliceEdge(self):
        return self.__BinnedInZ

    def Dimension(self):
        if self.yAxis() and self.zAxis() and self.xAxis():
            return 3
        elif self.xAxis() and self.yAxis():
            return 2
        elif self.xAxis():
            return 1
        return 0

    def Dependencies(self):
        Dep = []
        if self.xAxis(): Dep.append(self.xAxis())
        if self.yAxis(): Dep.append(self.yAxis())
        if self.zAxis(): Dep.append(self.zAxis())
        return Dep


class HistogramAssembler(object):
    def __init__(self, Component, Region, ListOfVars):
        self.__Component = Component
        self.__Region = Region
        self.__HistoSets = {}
        self.__Ysliced = False
        self.__Zsliced = False
        Yslices = []
        Zslices = []

        for Var in ListOfVars:
            self.__HistoSets[Var] = XAMPPplotting.PlottingHistos.HistoSet(self.__Component, Var.GetName(), self.__Region, False,
                                                                          XAMPPplotting.FileStructureHandler.GetStructure().GetConfigSet())
            if Var.isSliceInY():
                if Var.YsliceEdges()[0] not in Yslices:
                    Yslices.append(Var.YsliceEdges()[0])
                elif Var.YsliceEdges()[1] not in Yslices:
                    Yslices.append(Var.YsliceEdges()[1])
            if Var.isSliceInZ and self.HistoSets[Var].isTH3():
                print "Deine  mutter"
            if Var.isSliceInZ():
                if Var.ZsliceEdges()[0] not in Zslices:
                    Zslices.append(Var.ZsliceEdges()[0])
                elif Var.ZsliceEdges()[1] not in Zslices:
                    Zslices.append(Var.ZsliceEdges()[1])


###########################################################################
#
###########################################################################
class WeightedFakeFactorComponent(object):
    def __init__(self, Component, Region, ListOfVars):
        self.__Component = Component
        self.__Region = Region
        self.__HistoVars = []
        self.__HistoSets = {}
        self.__Dependencies = []
        self.__FakeTypes = {}

        # Use the fake fakeHistoVariable class to obtain all information from
        # the Histo name strings
        Splitted = []
        for Var in ListOfVars:
            FF = FakeHistoVariable(Var)
            if not FF.isSlice():
                self.__HistoVars.append(FF)
            else:
                Splitted.append(Var)
        for Var in self.__HistoVars:
            # Extract the dependence of the input on the particle variables under
            # consideration
            for i in range(0, 3):
                if Var.GetAxis(i) and Var.GetAxis(i) not in self.__Dependencies:
                    self.__Dependencies.append(Var.GetAxis(i))

            # Lets see what fake-ypes are defined and split
            # the variables according to their types


#              if not Var.FakeType() in self.__FakeTypes.iterkeys():
#                 self.__FakeTypes [Var.FakeType()] = []
            self.__FakeTypes[Var.FakeType()] = Var
            # Load the histograms
            self.__HistoSets[Var] = XAMPPplotting.PlottingHistos.HistoSet(self.__Component, Var.GetName(), self.__Region, False,
                                                                          XAMPPplotting.FileStructureHandler.GetStructure().GetConfigSet())

    def GetName(self):
        return self.__Component

    def FakeTypes(self):
        FakeTypes = []
        for type in self.__FakeTypes.iterkeys():
            FakeTypes.append(type)
        return FakeTypes

    def Dimension(self):
        return len(self.__Dependencies)

    def Dependencies(self):
        return self.__Dependencies

    def DependsOn(self, In):
        for i, Var in enumerate(self.Dependencies()):
            if Var == In:
                return i
        return -1

    def InputVars(self, Idx=-1):
        if Idx > -1 and Idx < len(self.__HistoVars):
            return self.__HistoVars[Idx]
        return self.__HistoVars

    def IsValid(self):
        # Reject the fake-factor depends on too many variables or none
        if self.Dimension() > 3 or self.Dimension() == 0:
            return False
        # Fake types were not encoded in the strings
        if len(self.FakeTypes()) == 0:
            return False
        # Inquire that all input variables share the same dimensions
        for Var, Set in self.__HistoSets.iteritems():
            if Var.Dimension() != self.Dimension():
                return False
            # Check whether the Set has the dimensionality we are claiming for
            if Set.Dimension() != self.Dimension():
                return False
            # Histogram could not be loaded from the input somehow
            if not Set.CheckHistoSet():
                return False
            # Processes were not loaded...
            if len(Set.GetSampleNames()) != len(XAMPPplotting.FileStructureHandler.GetStructure().GetConfigSet().DSConfigs()):
                return False
            # Finally check the binning of the components
            for Smp in Set.GetBackgrounds():
                for i in range(self.Dimension()):
                    if not XAMPPplotting.Utils.SameAxisBinning(self.GetAxis(i), Smp.GetAxis(i)):
                        return False
        return True

    def GetAxis(self, Idx):
        if Idx >= self.Dimension():
            print "ERROR: No valid axis index ", Idx
            return None
        return self.__HistoSets[self.InputVars(0)].GetSummedBackground().GetAxis(Idx)

    def GetNBins(self, Idx):
        return self.__HistoSets[self.InputVars(0)].GetSummedBackground().GetNbins(Idx)

    def Samples(self):
        return self.__HistoSets[self.InputVars(0)].GetSampleNames()

    def Histo(self):
        return self.__HistoSets[self.InputVars(0)].GetSummedBackground().GetHistogram()

    def GetSet(self, type):
        for Var, Set in self.__HistoSets.iteritems():
            if Var.FakeType() == type:
                return Set
        return None

    def GetBinContent(self, FakeType, Sample, (x, y, z)):
        return self.GetSet(FakeType).GetSample(Sample).GetBinContent(x, y, z)

    # Which kind of error the user wants to retrieve.
    # 0 = Statistical, 1 = Syst_Up, 2 = Syst_Down
    def GetBinError(self, FakeType, Sample, (x, y, z), Kind):
        if Kind == 1 or Kind == 2:
            SystDn, SystUp = self.GetSet(FakeType).GetSample(Sample).GetSystematics()
            Hist = SystUp if Kind == 2 else SystDn
            if z > -1:
                return Hist.GetBinContent(x, y, z)
            elif y > -1:
                return Hist.GetBinContent(x, y)
            else:
                return Hist.GetBinContent(x)
        elif Kind == 0:
            return self.GetSet(FakeType).GetSample(Sample).GetBinError(x, y, z)
        print "ERROR: Unkown error kind ", Kind, " return 'nan'."
        return float("NAN")


class ProcessFraction(WeightedFakeFactorComponent):
    def __init__(self, Component, Region, ListOfVars):
        WeightedFakeFactorComponent.__init__(self, Component, Region, ListOfVars)

    def Norm(self, x, y, z):
        Norm = 0.
        for type in self.FakeTypes():
            Norm += self.GetSet(type).GetSummedBackground().GetBinContent(x, y, z)
        if Norm == 0.:
            return 0.
        return 1. / Norm

    def GetBinContent(self, FakeType, Sample, (x, y, z)):
        return WeightedFakeFactorComponent.GetBinContent(self, FakeType, Sample, (x, y, z)) * self.Norm(x, y, z)

    def GetBinError(self, FakeType, Sample, (x, y, z), Kind):
        Norm = self.Norm(x, y, z)
        if Norm == 0:
            return 0
        ErrNorm = Norm * Norm * (1. / Norm - WeightedFakeFactorComponent.GetBinContent(self, FakeType, Sample, (x, y, z)))
        return ErrNorm * WeightedFakeFactorComponent.GetBinError(self, FakeType, Sample, (x, y, z), Kind)


class WeightedFakeFactorAssembler(object):
    def __init__(self, Options, Particle, Region, InputFF, InputSF, InputPF):
        self.__Particle = Particle
        self.__Region = Region
        self.__DoSyst = not Options.noSyst
        self.__FakeFactor = WeightedFakeFactorComponent(Options.FakeFactorAnalysis, Region, InputFF)
        self.__ScaleFactor = WeightedFakeFactorComponent(Options.ScaleFactorAnalysis, Region, InputSF)
        self.__ProcessFractions = ProcessFraction(Options.ProcessFractionAnalysis, Region, InputPF)
        self.__Correlation = {}
        self.__NominalAvgFF = None
        self.__StatUpAvgFF = None
        self.__StatDownAvgFF = None
        self.__SystUpAvgFF = None
        self.__SystDownAvgFF = None

    def GetName(self):
        return self.__Particle

    def FF(self):
        return self.__FakeFactor

    def SF(self):
        return self.__ScaleFactor

    def PF(self):
        return self.__ProcessFractions

    def IsValid(self):
        if not self.FF().IsValid() or not self.SF().IsValid() or not self.PF().IsValid():
            return False
        # Check if the same fake types are loaded
        if not XAMPPplotting.Utils.SameContent(self.FF().FakeTypes(), self.PF().FakeTypes()):
            print "ERROR: %s does not have the same fake-types for FF and PF" % (self.GetName())
            return False
        if not XAMPPplotting.Utils.SameContent(self.FF().FakeTypes(), self.SF().FakeTypes()):
            print "ERROR: %s does not have the same fake-types for FF and SF" % (self.GetName())
            return False
        # Check if the mapping between the components could be made sucessfully
        self.__CreateCorrelationTensor()
        for FFaxis, SFaxis, PFaxis in self.__Correlation.itervalues():
            # We require same axis binning
            if FFaxis != -1 and SFaxis != -1 and not XAMPPplotting.Utils.SameAxisBinning(self.FF().GetAxis(FFaxis),
                                                                                         self.SF().GetAxis(SFaxis)):
                print "ERROR: %s could not match the axis binning of FF & SF" % (self.GetName())
                return False
            if FFaxis != -1 and PFaxis != -1 and not XAMPPplotting.Utils.SameAxisBinning(self.FF().GetAxis(FFaxis),
                                                                                         self.PF().GetAxis(PFaxis)):
                print "ERROR: %s could not match the axis binning of FF & Process Fractions" % (self.GetName())
                return False
            if SFaxis != -1 and PFaxis != -1 and not XAMPPplotting.Utils.SameAxisBinning(self.SF().GetAxis(SFaxis),
                                                                                         self.PF().GetAxis(PFaxis)):
                print "ERROR: %s could not match the axis binning of SF & Process Fractions" % (self.GetName())
                return False
        return True

    def MaxDim(self):
        return max([self.FF().Dimension(), self.PF().Dimension(), self.SF().Dimension()])

    def Component(self, Idx):
        if Idx == 0:
            return self.FF()
        if Idx == 1:
            return self.SF()
        return self.PF()

    def __GetCorrelationColumn(self, Idx):
        Col = []
        for i in range(self.MaxDim()):
            Col.append(self.__Correlation[i][Idx])
        return Col

    def __OrderBins(self, x, y, z, ColNum):
        Ord = []
        NeededOrder = self.__GetCorrelationColumn(ColNum)
        for Idx in NeededOrder:
            if Idx == 0:
                Ord.append(x)
            if Idx == 1:
                Ord.append(y)
            if Idx == 2:
                Ord.append(z)
        if self.Component(ColNum).Dimension() == 3:
            return Ord[0], Ord[1], Ord[2]
        elif self.Component(ColNum).Dimension() == 2:
            return Ord[0], Ord[1], -1
        return Ord[0], -1, -1

    # This method creates the tensor describing which axis of which component is mapped
    # to each other
    def __CreateCorrelationTensor(self):
        if len(self.__Correlation) > 0:
            return
        for Idx, Dep in enumerate(self.__MaxDimComponent().Dependencies()):
            t_FF = self.FF().DependsOn(Dep)
            t_SF = self.SF().DependsOn(Dep)
            t_PF = self.PF().DependsOn(Dep)
            self.__Correlation[Idx] = (t_FF, t_SF, t_PF)

    def __MaxDimComponent(self):
        for i in range(self.MaxDim()):
            if self.Component(i).Dimension() == self.MaxDim():
                return self.Component(i)

    def __CreateHistoTemplate(self):
        if self.__NominalAvgFF:
            return
        self.__NominalAvgFF = self.__MaxDimComponent().Histo().Clone("AverageFF-%s__Nominal" % (self.GetName()))
        self.__NominalAvgFF.Reset()

        self.__StatUpAvgFF = self.__NominalAvgFF.Clone("AverageFF-%s__STAT_Up" % (self.GetName()))
        self.__StatDownAvgFF = self.__NominalAvgFF.Clone("AverageFF-%s__STAT_Down" % (self.GetName()))
        if self.__DoSyst:
            self.__SystUpAvgFF = self.__NominalAvgFF.Clone("AverageFF-%s__SYST_Up" % (self.GetName()))
            self.__SystDownAvgFF = self.__NominalAvgFF.Clone("AverageFF-%s__SYST_Down" % (self.GetName()))

    def __AverageContribution(self, type, smp, x, y, z):
        AverageFF = 1.
        for Comp in range(0, 3):
            Order = self.__OrderBins(x, y, z, Comp)
            AverageFF *= self.Component(Comp).GetBinContent(type, smp, Order)
        return AverageFF

    # The error kind tells which error the user want to retrieve
    # 0 = statistical, 1 = Syst_Down, 2 = Syst_Up
    def __AverageError(self, type, smp, x, y, z, kind=0):
        # Some kind of nasty, I know but the error is not as simple
        FF_Bins = self.__OrderBins(x, y, z, 0)
        SF_Bins = self.__OrderBins(x, y, z, 1)
        PF_Bins = self.__OrderBins(x, y, z, 2)
        FF_err = self.FF().GetBinError(type, smp, FF_Bins, kind) * self.SF().GetBinContent(type, smp, SF_Bins) * self.PF().GetBinContent(
            type, smp, PF_Bins)
        SF_err = self.FF().GetBinContent(type, smp, FF_Bins) * self.SF().GetBinError(type, smp, SF_Bins, kind) * self.PF().GetBinContent(
            type, smp, PF_Bins)
        PF_err = self.FF().GetBinContent(type, smp, FF_Bins) * self.SF().GetBinContent(type, smp, SF_Bins) * self.PF().GetBinError(
            type, smp, PF_Bins, kind)
        return math.sqrt(FF_err * FF_err + SF_err * SF_err + PF_err * PF_err)

    def __Contract(self, x, y=-1, z=-1):
        Value = 0.
        Error = 0.
        SystDown = 0.
        SystUp = 0.
        for type in self.FF().FakeTypes():
            for smp in self.FF().Samples():
                Value = Value + self.__AverageContribution(type, smp, x, y, z)
                Error = Error + self.__AverageError(type, smp, x, y, z)
                if self.__DoSyst:
                    SystDown = SystDown + self.__AverageError(type, smp, x, y, z, 1)
                    SystUp = SystUp + self.__AverageError(type, smp, x, y, z, 2)

        if z > -1:
            self.__NominalAvgFF.SetBinContent(x, y, z, Value)
            self.__NominalAvgFF.SetBinError(x, y, z, Error)
            self.__StatUpAvgFF.SetBinContent(x, y, z, Value + Error)
            self.__StatDownAvgFF.SetBinContent(x, y, z, Value - Error)
            if self.__DoSyst:
                self.__SystUpAvgFF.SetBinContent(x, y, z, Value + SystUp)
                self.__SystDownAvgFF.SetBinContent(x, y, z, Value - SystDown)

        elif y > -1:
            self.__NominalAvgFF.SetBinContent(x, y, Value)
            self.__NominalAvgFF.SetBinError(x, y, Value)
            self.__StatUpAvgFF.SetBinContent(x, y, Value + Error)
            self.__StatDownAvgFF.SetBinContent(x, y, Value - Error)
            if self.__DoSyst:
                self.__SystUpAvgFF.SetBinContent(x, y, Value + SystUp)
                self.__SystDownAvgFF.SetBinContent(x, y, Value - SystDown)

        else:
            self.__NominalAvgFF.SetBinContent(x, Value)
            self.__NominalAvgFF.SetBinError(x, Value)
            self.__StatUpAvgFF.SetBinContent(x, Value + Error)
            self.__StatDownAvgFF.SetBinContent(x, Value - Error)
            if self.__DoSyst:
                self.__SystUpAvgFF.SetBinContent(x, Value + SystUp)
                self.__SystDownAvgFF.SetBinContent(x, Value - SystDown)

    def CalculateFF(self):
        self.__CreateCorrelationTensor()
        self.__CreateHistoTemplate()
        # Calculate the averaged FF
        for BinX in range(self.__MaxDimComponent().GetNBins(0)):
            if self.MaxDim() == 1:
                self.__Contract(BinX)
            else:
                for BinY in range(self.__MaxDimComponent().GetNBins(1)):
                    if self.MaxDim() == 2:
                        self.__Contract(BinX, BinY)
                    else:
                        for BinZ in range(self.__MaxDimComponent().GetNBins(2)):
                            self.__Contract(BinX, BinY, BinZ)

    def SaveToFile(self, File):
        File.cd()
        if not File.GetDirectory(self.__Region):
            File.mkdir(self.__Region)
        File.GetDirectory(self.__Region).cd()
        self.__NominalAvgFF.Write()
        self.__StatDownAvgFF.Write()
        self.__StatUpAvgFF.Write()
        if self.__DoSyst:
            self.__SystDownAvgFF.Write()
            self.__SystUpAvgFF.Write()
        print "Coffee and shipment"


def HaveSameRegions(analysis1, analysis2):
    ConfigSet = XAMPPplotting.FileStructureHandler.GetStructure(Options).GetConfigSet()
    Region1 = ConfigSet.GetAnalysisRegions(analysis1)
    Region2 = ConfigSet.GetAnalysisRegions(analysis2)
    return XAMPPplotting.Utils.SameContent(Region1, Region2)


if __name__ == "__main__":
    Options = SetupParser()

    ConfigSet = XAMPPplotting.FileStructureHandler.GetStructure(Options).GetConfigSet()
    if not Options.ScaleFactorAnalysis in ConfigSet.GetAnalyses():
        print "ERROR: The scale-factors (%s) are not stored in the input " % (Options.ScaleFactorAnalysis)
        exit(1)
    if not Options.FakeFactorAnalysis in ConfigSet.GetAnalyses():
        print "ERROR: The fake-factors (%s) are not stored in the input " % (Options.FakeFactorAnalysis)
        exit(1)
    if not Options.ProcessFractionAnalysis in ConfigSet.GetAnalyses():
        print "ERROR: The process-fractions (%s) are not stored in the input " % (Options.ProcessFractionAnalysis)
        exit(1)
    # Now check whether we have for each component of the fake factor the same regions
    if not HaveSameRegions(Options.ProcessFractionAnalysis, Options.FakeFactorAnalysis) or not HaveSameRegions(
            Options.FakeFactorAnalysis, Options.ScaleFactorAnalysis):
        print "ERROR: Please make sure that you have defined the same analysis selctions for all components of the weighted FF"
        exit(1)

    if not os.path.exists(Options.outputDir):
        os.mkdir(Options.outputDir)
    OutFile = ROOT.TFile.Open("%s/%s" % (Options.outputDir, Options.outFile), "RECREATE")
    ###################################################################################
    #  The input seems to be consistent thus far now we can start assembling the SFs
    ###################################################################################
    for Region in ConfigSet.GetAnalysisRegions(Options.FakeFactorAnalysis):

        # Split the stored histogram for all three analysis into the particles they belong to
        InVarsFF = SplitIntoParticles(ConfigSet.GetVariables(Options.FakeFactorAnalysis, Region))
        InVarsSF = SplitIntoParticles(ConfigSet.GetVariables(Options.ScaleFactorAnalysis, Region))
        InVarsPF = SplitIntoParticles(ConfigSet.GetVariables(Options.ProcessFractionAnalysis, Region))

        for Particle in InVarsFF.iterkeys():
            if Particle not in InVarsSF.iterkeys() or Particle not in InVarsPF.iterkeys():
                print "WARNING: The weighted fake-factor is not completely available for " + Particle
                continue
            Assembler = WeightedFakeFactorAssembler(Options, Particle, Region, InVarsFF[Particle], InVarsSF[Particle], InVarsPF[Particle])
            if not Assembler.IsValid():
                continue
            Assembler.CalculateFF()
            Assembler.SaveToFile(OutFile)

    OutFile.Close()

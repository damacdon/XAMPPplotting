import ROOT
from XAMPPplotting import TauLogger as TL

##
# @class TauHisto
##


class TauHisto(object):
    def __init__(
            self,
            name="histo holder",
            tau_info=None,  # type: TauInfo
            tau_sample_histo=None  # type: XAMPPplotting.PlottingHistos.SampleHisto
    ):
        "Hold histos composed by HistoInfo and TH1"
        #verbosity
        self.__msg = TL.msg(2)
        #parsed
        try:
            self.__tau_info = tau_info
            self.__msg.debug("TauHisto", "Setting TauInfo '%s'" % (tau_info.whatami))
        except ValueError:
            self.__msg.fatal("TauHisto", "Null TauInfo object...")

        try:
            self.__tau_sample_histo = tau_sample_histo
        except ValueError:
            self.__msg.fatal("TauHisto", "Null SampleHisto object...")
            self.__msg.debug("TauHisto", "Setting XAMPPplotting.PlottingHistos.SampleHisto '%s'" % (tau_sample_histo.GetName()))

        self.__cached = False
        #derived
        self.__tau_histo = None
        self.__tau_name = ""
        self.__lumi = 0.

    def __is_1d_histogram(self, obj):
        return issubclass(type(obj), ROOT.TObject) and "TH1" in obj.IsA().GetName()

    def __cache(self):
        if not self.__cached:
            self.__cached = True

            self.__tau_histo = self.__tau_sample_histo.GetHistogram()
            self.__tau_name = self.__tau_sample_histo.GetName()
            self.__lumi = self.__tau_sample_histo.GetLumi()
            self.__msg.debug(
                "TauHisto", "Setting TauHisto properties: histo OK=%i Name=%s Lumi=%s" %
                (self.__is_1d_histogram(self.__tau_histo), self.__tau_name, self.__lumi))

    @property
    def info(self):
        return self.__tau_info

    @property
    def histo(self):
        self.__cache()
        return self.__tau_sample_histo

    @property
    def name(self):
        return self.__tau_name

    @property
    def lumi(self):
        return self.__lumi

    @property
    def is_sum_bg(self):
        return self.__tau_name == "SumBG"


##
# @class TauHistoHolder
##
class TauHistoHolder(object):
    def __init__(self, name="histo holder"):
        "Hold histos"
        self.__tau_histo_list = []
        self.__msg = TL.msg(2)
        self.__report_file = 'TauHolderReport.txt'

    def __exists(self, tau_histo=None):
        return any(tau_histo.info.same(elem.info) for elem in self.__tau_histo_list)

    def add(self, tau_histo=None):
        if tau_histo and not self.__exists(tau_histo):
            self.__tau_histo_list.append(tau_histo)
        else:
            self.__msg.debug("TauHistoHolder", "To-be-inserted TauHisto\n  %s \nalready exists!" % (tau_histo.info.whatami))

    def count_matches(self,
                      flavor="",
                      channel="",
                      SR="",
                      CR="",
                      definition="",
                      origin="",
                      prongs=[],
                      eta=[],
                      ID="",
                      lepton="",
                      observable=""):
        counter = 0
        for tau_histo in self.__tau_histo_list:
            if tau_histo.info.match(flavor, channel, SR, CR, definition, origin, prongs, eta, ID, lepton, observable):
                counter += 1
        return counter

    def find(self, flavor="", channel="", SR="", CR="", definition="", origin="", prongs=[], eta=[], ID="", lepton="", observable=""):

        for tau_histo in self.__tau_histo_list:
            if tau_histo.info.match(flavor, channel, SR, CR, definition, origin, prongs, eta, ID, lepton, observable):
                return tau_histo
        if "Iso" in lepton:
            return self.find(flavor, channel, SR, CR, definition, origin, prongs, eta, ID, lepton.replace("Iso", ""), observable)
        best_n = 0
        best_match = None
        for tau_histo in self.__tau_histo_list:
            bit_true = len([
                b for b in tau_histo.info.matching_bits(flavor, channel, SR, CR, definition, origin, prongs, eta, ID, lepton, observable)
                if b == True
            ])
            if bit_true > best_n:
                best_n = bit_true
                best_match = tau_histo

        print "Stonjek.... Stonjek... Stonjek... "
        print best_match.info.whatami, best_match.info.matching_bits(flavor, channel, SR, CR, definition, origin, prongs, eta, ID, lepton,
                                                                     observable)
        self.__msg.error(
            "TauHistoHolder", "Couldn't find element: flavor=%s ch=%s sr=%s cr=%s orig=%s def=%s p=%s eta=%s id=%s lep=%s obs=%s" %
            (flavor, channel, SR, CR, origin, definition, " ".join(str(p) for p in prongs), " ".join(str(e)
                                                                                                     for e in eta), ID, lepton, observable))
        return None

    def size(self):
        return len(self.__tau_histo_list)

    def create_report(self):
        with open(self.__report_file, 'a') as ofile:
            for tau_histo in self.__tau_histo_list:
                ofile.write("%s\n\n" % (tau_histo.info.whatami))
        ofile.close()
        self.__msg.info("TauHistoHolder", "Report file '%s' has been created..." % (self.__report_file))
        return True

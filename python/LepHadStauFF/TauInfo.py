from XAMPPplotting import TauLogger as TL


class TauInfo(object):
    def __init__(self, analysis="", region="", variable="", flavor=""):
        #verbosity
        self.__msg = TL.msg(2)
        #parsed data
        self.__analysis = analysis
        self.__region = region  #eg. Chan_EleTau_CR_Wjets_SR_0jet_TauID_fail or Chan_EleTau_SR_1jet_TauID_fail
        self.__variable = variable  # eg. TauFF_defBase_origNotJetFake_eta0p0to0p8_prongIncl_rangeIncl_leptonIncl_varPt
        self.__flavor = flavor
        #fixed properties
        self.__origin_inclusive_name = "Incl"
        self.__lepton_inclusive_name = "Incl"

        self.__origin_accepted = ["Real", "Incl"]

        self.__inclusive_prongs = [1, 3]
        self.__inclusive_eta = [0.0, 2.5]
        #derived data
        self.__region_tokens = self.__tokenize(region)
        self.__variable_tokens = self.__tokenize(variable)
        self.__isFF = self.__is_FF_like_variable()
        self.__definition = ""
        self.__origin = ""
        self.__eta_range = []
        self.__prongs = []
        self.__runs_range = []
        self.__observable = ""
        self.__channel = ""
        self.__control_region = ""
        self.__signal_region = ""
        self.__tau_id = ""
        self.__lepton_def = ""
        self.__is_ff_ready = self.__fill_fields() if self.__isFF else False
        self.__is_data_friendly = self.__is_data_friendly()
        self.__is_data = self.__flavor.upper() == "DATA"
        self.__is_mc = self.__flavor.upper() == "MC"

    def __str__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __tokenize(self, string=""):
        return string.split("_")

    def __is_FF_like_variable(self):
        return self.__variable_tokens[0] == "TauFF"

    def __get_field_value(self, field=""):
        for token in self.__variable_tokens:
            if field in token:
                return token.replace(field, "")

        self.__msg.warning("TauInfo", "Field value '%s' not found in variable name '%s'" % (field, self.__variable))
        return "None"

    def __get_eta_range(self, var=""):  #format: 0p0to0p8
        return self.__inclusive_eta if "Incl" in var else [float(inum) for inum in var.replace("p", ".").split("to")]

    def __get_prongs(self, var=""):  #format: 3P
        return self.__inclusive_prongs if "Incl" in var else [int(var.replace("P", ""))]

    def __get_run_range(self, var=""):  #format: 312312to540540
        return [100000, 900000] if "Incl" in var else [int(inum) for inum in var.split("to")]

    def __get_field_element(self, elem=""):  #assuming Chan_EleTau_CR_Wjets_SR_0jet_TauID_fail   or  Chan_EleTau_SR_1jet_TauID_fail
        index = self.__region_tokens.index(elem) if elem in self.__region_tokens else -1
        if index >= 0 and index < len(self.__region_tokens):
            return self.__region_tokens[index + 1]

        self.__msg.warning("TauCommon", "Element %s not found in item %s with size %i" % (elem, self.__region, len(self.__region_tokens)))

        return "None"

    def __get_signal_region(self):
        return self.__get_field_element("SR")

    def __get_control_region(self):
        if not "_CR_" in self.__region:
            self.__msg.debug("TauInfo", "CR is NotApplied of %s %s" % (self.__region, self.__variable))
            return "NotApplied"
        return self.__get_field_element("CR")

    def __fields_are_valid(self):
        if "None" in self.__definition: return False
        if "None" in self.__origin: return False
        if "None" in self.__observable: return False
        if not self.__prongs: return False
        if not self.__runs_range: return False
        if "None" in self.__lepton_def: return False
        if "None" in self.__channel: return False
        if "None" in self.__control_region: return False
        if "None" in self.__signal_region: return False
        if "None" in self.__tau_id: return False
        return True

    def __fill_fields(self):
        self.__msg.debug("TauInfo", "Registering %s %s %s" % (self.__region, self.__variable, self.__flavor))

        self.__definition = self.__get_field_value('def')
        self.__origin = self.__get_field_value('orig')
        self.__eta_range = self.__get_eta_range(self.__get_field_value('eta'))
        self.__prongs = self.__get_prongs(self.__get_field_value('prong'))
        self.__runs_range = self.__get_run_range(self.__get_field_value('range'))
        self.__lepton_def = self.__get_field_value("lepton")
        self.__observable = self.__get_field_value('var')

        self.__channel = self.__get_field_element("Chan")
        self.__control_region = self.__get_control_region()
        self.__signal_region = self.__get_signal_region()
        self.__tau_id = self.__get_field_element("TauID")

        return self.__fields_are_valid()

    def __is_origin_inclusive(self):
        return self.__origin == self.__origin_inclusive_name

    def __is_lepton_inclusive(self):
        return self.__lepton_def == self.__lepton_inclusive_name

    def __is_data_friendly(self):
        return self.__is_origin_inclusive() and self.__is_lepton_inclusive()

    def __is_mc_friendly(self):
        return True

    @property
    def analysis(self):
        return self.__analysis

    @property
    def region(self):
        return self.__region

    @property
    def variable(self):
        return self.__variable

    @property
    def channel(self):
        return self.__channel

    @property
    def definition(self):
        return self.__definition

    @property
    def origin(self):
        return self.__origin

    @property
    def control_region(self):
        return self.__control_region

    @property
    def signal_region(self):
        return self.__signal_region

    @property
    def tau_id(self):
        return self.__tau_id

    @property
    def lepton_def(self):
        return self.__lepton_def

    @property
    def prongs(self):
        return self.__prongs

    @property
    def eta_range(self):
        return self.__eta_range

    @property
    def observable(self):
        return self.__observable

    @property
    def flavor(self):
        return self.__flavor

    @flavor.setter
    def flavor(self, value):
        self.__flavor = value

    @property
    def is_data_friendly(self):
        return self.__is_data_friendly

    @property
    def is_mc_friendly(self):
        return self.__is_mc_friendly

    @property
    def is_ff_ready(self):
        return self.__is_ff_ready

    @property
    def is_data(self):
        return self.__is_data

    @property
    def is_mc(self):
        return self.__is_mc

    def is_SR(self):
        return self.__signal_region != "None" and self.__control_region == "NotApplied"

    def is_CR(self):
        return self.__signal_region != "None" and self.__control_region != "None" and self.__control_region != "NotApplied"

    def is_anti_iso_lepton(self):
        return "AntiIso" in self.__lepton_def

    def is_iso_lepton(self):
        return "Iso" in self.__lepton_def and not "AntiIso" in self.__lepton_def

    def is_IsoSS_CR(self):
        return "IsoSS" in self.__control_region and not "AntiIsoSS" in self.__control_region

    def is_AntiIsoSS_CR(self):
        return "AntiIsoSS" in self.__control_region

    def is_Top_CR(self):
        return "Top" in self.__control_region

    def is_QCD_CR(self):
        return "QCD" in self.__control_region

    def is_Wjets_CR(self):
        return "Wjets" in self.__control_region

    def is_Dilepton_CR(self):
        return "Dilepton" in self.__control_region

    def is_truth_info(self):
        return self.__lepton_def != self.__lepton_inclusive_name or self.__origin != self.__origin_inclusive_name

    def is_needed_mc_template(self):
        return self.__origin in self.__origin_accepted

    def is_prong_incl(self):
        return set(self.__inclusive_prongs) == set(self.__prongs)

    def is_eta_incl(self):
        return set(self.__inclusive_eta) == set(self.__eta_range)

    def display(self):
        print self.whatami

    def match(self,
              flavor="",
              channel="",
              signal_region="",
              control_region="",
              definition="",
              origin="",
              prongs=[],
              eta_range=[],
              tau_id="",
              lepton_def="",
              observable=""):
        bit_mask = self.matching_bits(flavor=flavor,
                                      channel=channel,
                                      signal_region=signal_region,
                                      control_region=control_region,
                                      definition=definition,
                                      origin=origin,
                                      prongs=prongs,
                                      eta_range=eta_range,
                                      tau_id=tau_id,
                                      lepton_def=lepton_def,
                                      observable=observable)
        return len([b for b in bit_mask if b == True]) == len(bit_mask)

    def matching_bits(self,
                      flavor="",
                      channel="",
                      signal_region="",
                      control_region="",
                      definition="",
                      origin="",
                      prongs=[],
                      eta_range=[],
                      tau_id="",
                      lepton_def="",
                      observable=""):
        return [
            flavor == self.__flavor, channel == self.__channel, signal_region == self.__signal_region,
            control_region == self.__control_region, definition == self.__definition, origin == self.__origin,
            set(prongs) == set(self.__prongs),
            set(eta_range) == set(self.__eta_range), tau_id == self.__tau_id, lepton_def == self.__lepton_def,
            observable == self.__observable
        ]

    def same(self, other=None):
        if not other:
            return False

        return self.match(other.flavor, other.channel, other.signal_region, other.control_region, other.definition, other.origin,
                          other.prongs, other.eta_range, other.tau_id, other.lepton_def, other.observable)

    @property
    def whatami(self):
        fields = "flavor=%s, analysis=%s, region=%s, variable=%s, " \
            + "data friendly=%i, definition=%s, origin=%s, prongs=%s, eta=%s, " \
            + "runs=%s, observable=%s,  channel=%s, CR=%s, SR=%s, ID=%s, lepton=%s, obs=%s\n "

        return fields % (self.__flavor, self.__analysis, self.__region, self.__variable, self.__is_data_friendly, self.__definition,
                         self.__origin, ' '.join(str(p) for p in self.__prongs), ' '.join(str(e) for e in self.__eta_range), ' '.join(
                             str(r) for r in self.__runs_range), self.__observable, self.__channel, self.__control_region,
                         self.__signal_region, self.__tau_id, self.__lepton_def, self.__observable)

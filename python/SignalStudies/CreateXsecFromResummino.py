import os, sys, math, argparse, logging, ROOT
from pprint import pprint


def CreateArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--ResumminoIn", help="Directory where the input files from Resummino are located")
    parser.add_argument("--OutFile", help="The outfile", default="xSections.txt")
    parser.add_argument('--JobOptionsDir', help="Directory of the files containing the 1-line JO", required=True)
    parser.add_argument("--Models", help="To which models the xSection can be applied", nargs="+", default=[])
    parser.add_argument("--RHValues", help="Store the RH values as LH ones", action="store_true", default=False)
    return parser


def MassFromFileName(Name):
    return int(Name.replace(".txt", "").split("_")[-1])


def ReadInResumminoFile(Location):
    FileContent = {}
    logging.info("Open resummino file %s" % (Location))

    with open(Location, "r") as Read:
        for Line in Read:
            Line = Line.strip()
            if "nan" in Line or Line.startswith("#"): continue
            print Line
            FinalState = int(Line.split()[0])
            Central = float(Line.split()[1])
            CalcError = float(Line.split()[2])
            if Central == 0.: FileContent[FinalState] = (0., 1.)
            else:
                Error = (CalcError / Central)**2
                PDFUncert = [float(E) for E in Line.split("|")[-1].split()]
                for i in range(0, len(PDFUncert), 2):
                    if i + 1 < len(PDFUncert):
                        Error += max(PDFUncert[i]**2, PDFUncert[i + 1]**2)
                    else:
                        Error *= PDFUncert[i]**2

                Error = math.sqrt(Error)
                #Cross-ections are saved in fb -> divide by 1.e3
                FileContent[FinalState] = (Central / 1.e3, Error)

    #Overwrite missing stau prodictions
    #if 201 in FileContent.iterkeys(): FileContent[206] = FileContent[201]
    #if 204 in FileContent.iterkeys(): FileContent[210] = FileContent[204]
    #if 205 in FileContent.iterkeys(): FileContent[211] = FileContent[205]
    return FileContent


def GetResumminoDictionary(Input):
    Dict = {}
    if os.path.isdir(Input):
        for F in os.listdir(Input):
            if not F.endswith(".txt"): continue
            Mass = MassFromFileName(F)
            try:
                Dict[Mass].update(ReadInResumminoFile("%s/%s" % (Input, F)))
            except:
                Dict[Mass] = ReadInResumminoFile("%s/%s" % (Input, F))
    return Dict


def GetDSIDs(Options):
    DSIDs = {}
    for File in sorted(os.listdir(Options.JobOptionsDir)):
        if not File.endswith(".py"):
            continue
        ##All JOS have the form
        #MC15.XXXXXX.<Gen>_<FineTune>_<Model>_<NLSP>
        if File.split("_")[2] not in Options.Models:
            continue
        DS = int(File[5:11])
        try:
            NLSPMass = int(float(File.split("_")[3].replace("p", ".")))
        except:
            continue
        print File
        DSIDs[DS] = (NLSPMass, File[5:-3])
    return DSIDs


def WriteXsectionFile(File, Samples, DB):
    x_sec_db = ROOT.SUSY.CrossSectionDB()
    with open(File, "w") as OutFile:
        for DSID in sorted(Samples.iterkeys()):
            Model = Samples[DSID]
            Mass = Model[0]
            Description = Model[1]
            if Mass not in DB.iterkeys():
                logging.warning("There was some strange noise out there %s" % (Description))
                continue
            OutFile.write("### %s\n" % (Description))
            Total_XS = 0.
            for FinalState, Values in DB[Mass].iteritems():
                xs = Values[0]
                err = Values[1]
                filter_eff = x_sec_db.efficiency(DSID) if x_sec_db.efficiency(DSID) > 0 else 1.
                OutFile.write("%i   %i   %.10f   %.10f  1.  %.10f \n" % (DSID, FinalState, xs, filter_eff, err))
                Total_XS += xs
            #print "%s\t%.10f" % (Description, Total_XS)


def GetRHsleptonDict(Input):
    OrigDB = GetResumminoDictionary(Input)
    DB = {}
    #Replace the lefthanded xsec values by the righthanded ones
    RH_LHMapping = {
        202: 201,  #selecton
        207: 206,  #smuon -> 
        217: 216,  #stau -> 
    }
    #Insert 0 else every where
    Sneu_Prod = [203, 204, 205, 210, 211, 218, 219, 220]
    for Mass, CrossSections in OrigDB.iteritems():
        DB[Mass] = {}
        for FinalState, Values in CrossSections.iteritems():
            if FinalState in RH_LHMapping.iterkeys():
                nFS = RH_LHMapping[FinalState]
                DB[Mass][nFS] = Values
        for S in Sneu_Prod:
            DB[Mass][S] = (0., 1.)

    return DB


if __name__ == '__main__':
    Options = CreateArgumentParser().parse_args()
    xSecDB = GetResumminoDictionary(Options.ResumminoIn) if not Options.RHValues else GetRHsleptonDict(Options.ResumminoIn)
    DSIDs = GetDSIDs(Options)
    print DSIDs
    WriteXsectionFile(Options.OutFile, DSIDs, xSecDB)

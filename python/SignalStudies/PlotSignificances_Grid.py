from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.FileStructureHandler import GetStructure
from XAMPPplotting.StatisticalFunctions import *
from pprint import pprint
from XAMPPplotting.PlotLabels import DrawProcessLabel, DrawXaxisMassLabel, DrawYaxisMassLabel
from ClusterSubmission.Utils import ExecuteThreads
from XAMPPplotting.Utils import GetPlotRangeX, GetPlotRangeY, setupBaseParser, id_generator
import threading


class SignalPoint(threading.Thread):
    ######################################
    # The Sample is an instance of the PlottingHisto class
    ########################################
    def __init__(self, Sample, SummedBackground, Data=None, BackgroundUncertainty=0.3, UseSUSYSignificance=False):
        threading.Thread.__init__(self)
        self.__Sample = Sample
        self.__Parameters = {}
        self.__SummedBackground = SummedBackground
        self.__Data = Data
        self.__UseSUSYSignificance = UseSUSYSignificance
        self.__NumberOfIntParameters = 0
        self.__BackgroundUncertainty = BackgroundUncertainty
        for abc, Param in enumerate(self.__Sample.GetName().split("_")):
            if Param.isdigit():
                self.__Parameters[abc] = int(Param)
                self.__NumberOfIntParameters = self.__NumberOfIntParameters + 1
            else:
                self.__Parameters[abc] = Param
        self.__sig_dict = {}
        self.__sig_staterr = {}
        self.__signal_dict = {}
        self.__background_dict = {}
        self.__is_calculated = False
        self.__isDecreasing = False

    def run(self):
        if self.__is_calculated: return
        self.__is_calculated = (self.CalculateSignificanceValues() and self.CalculateSignalEvents())

    def GetNumberOfParameters(self):
        return len(self.__Parameters)

    def GetNumberOfIntParameters(self):
        return self.__NumberOfIntParameters()

    def GetName(self):
        return self.__Sample.GetName()

    def GetParameter(self, N):
        if not isinstance(N, int):
            logging.error("Invalid parameter " + str(N))
            return float("nan")
        if N >= self.GetNumberOfParameters():
            logging.warning("Invalid parameter %i. The sample %s actually has %i parameters. Return nan" %
                            (N, self.GetName(), self.GetNumberOfParameters()))
            return float("nan")
        return self.__Parameters[N]

    def GetHistogram(self):
        return self.__Sample.GetHistogram()

    def GetNbins(self):
        return int(self.__Sample.GetNbins())

    def GetNbinsX(self):
        return int(self.__Sample.GetNbinsX())

    def GetSummedBackground(self):
        return self.__SummedBackground.GetSummedBackground()

    def GetBkgHisto(self):
        return self.__SummedBackground.GetHistogram()

    def GetDataHisto(self):
        if self.__Data: return self.__Data.GetHistogram()
        return None

    def CalculateSignalEvents(self):
        if not self.GetHistogram().GetNbinsX():
            return False
        lastBinContent = None
        for b in range(self.GetNbins()):
            currentBinContent = self.GetHistogram().GetBinContent(b)
            if lastBinContent and currentBinContent < lastBinContent: self.__isDecreasing = True
            self.__signal_dict[self.GetHistogram().GetXaxis().GetBinLowEdge(b)] = currentBinContent
            self.__background_dict[self.GetBkgHisto().GetXaxis().GetBinLowEdge(b)] = self.GetBkgHisto().GetBinContent(b)
            if not lastBinContent: lastBinContent = currentBinContent
        return True

    def CalculateSignificanceValues(self):
        if not self.GetNbinsX():
            return False
        for b in range(self.GetNbins()):
            self.__sig_dict[self.GetHistogram().GetXaxis().GetBinLowEdge(b)] = self.CalculateSignificance(self.GetHistogram(),
                                                                                                          self.GetBkgHisto(),
                                                                                                          b,
                                                                                                          -1,
                                                                                                          DataHist=self.GetDataHisto())
            self.__sig_staterr[self.GetHistogram().GetXaxis().GetBinLowEdge(b)] = self.CalculateSignificance(self.GetHistogram(),
                                                                                                             self.GetBkgHisto(),
                                                                                                             b,
                                                                                                             -1,
                                                                                                             DataHist=self.GetDataHisto(),
                                                                                                             AddStatError=True)
        return True

    def CalculateSignificance(self, SHist, BHist, BinStart=0, BinEnd=-1, DataHist=None, AddStatError=False):
        S = SHist.GetBinContent(BinStart)
        SErr = SHist.GetBinError(BinStart)
        B = BHist.GetBinContent(BinStart)
        BErr = BHist.GetBinError(BinStart)
        if S == 0 or B == 0: return 0.
        if SErr / S > 0.5 or BErr / B > 0.5: return 0.
        if DataHist:
            Data = DataHist.GetBinContent(BinStart)
        if AddStatError:
            if B == 0: return 10.
            if B < 0:
                logging.error('WTF? Negative background yield %s?, returning stat error of %s' % (B, 10.))
                return 10.
            if B > 0:
                # let's try a numerical derivative, since we cannot find a function for the error of the significance:
                stepSize = 1.e-5
                if DataHist:
                    derivativeNumeratorB = math.fabs((
                        ButtingerLefebvreSignificance(Data, B + stepSize, self.__BackgroundUncertainty * B) -
                        ButtingerLefebvreSignificance(Data, B - stepSize, self.__BackgroundUncertainty * B)
                    ) if self.__UseSUSYSignificance else (
                        ROOT.RooStats.NumberCountingUtils.BinomialObsZ(Data, B + stepSize, self.__BackgroundUncertainty) -
                        ROOT.RooStats.NumberCountingUtils.BinomialObsZ(Data, B - stepSize, self.__BackgroundUncertainty)))
                else:
                    derivativeNumeratorB = math.fabs((
                        ButtingerLefebvreSignificance(S, B + stepSize, self.__BackgroundUncertainty * B) -
                        ButtingerLefebvreSignificance(S, B - stepSize, self.__BackgroundUncertainty * B)
                    ) if self.__UseSUSYSignificance else (
                        ROOT.RooStats.NumberCountingUtils.BinomialExpZ(S, B + stepSize, self.__BackgroundUncertainty) -
                        ROOT.RooStats.NumberCountingUtils.BinomialExpZ(S, B - stepSize, self.__BackgroundUncertainty)))
                return (derivativeNumeratorB) / (2. * stepSize * math.sqrt(B))
        if DataHist:
            significance = ButtingerLefebvreSignificance(
                Data, B, self.__BackgroundUncertainty *
                B) if self.__UseSUSYSignificance else ROOT.RooStats.NumberCountingUtils.BinomialObsZ(Data, B, self.__BackgroundUncertainty)
        else:
            significance = ButtingerLefebvreSignificance(
                S, B, self.__BackgroundUncertainty *
                B) if self.__UseSUSYSignificance else ROOT.RooStats.NumberCountingUtils.BinomialExpZ(S, B, self.__BackgroundUncertainty)

        significance = max(significance, 0.)
        if math.isnan(significance):
            print "Signal : %f     Background: %f " % (S, B)
            return 0.
        if significance > 10.:
            return 10.
        return significance

    def get_sig(self, cut):
        return self.__sig_dict[cut] if cut in self.__sig_dict else 0

    def get_sig_staterr(self, cut):
        return self.__sig_staterr[cut]

    def get_signalevents(self, cut):
        return self.__signal_dict[cut] if cut in self.__signal_dict else 0

    def get_backgroundevents(self, cut):
        return self.__background_dict[cut] if cut in self.__background_dict else 0

    def get_nlsp(self):
        return self.__nlsp

    def get_lsp(self):
        return self.__lsp

    def is_point(self, nlsp, lsp):
        if lsp > nlsp:
            print "Error. ..."
        return self.__lsp == lsp and self.__nlsp == nlsp

    def isDecreasing(self):
        return self.__isDecreasing


class ErstellungListe(object):
    def __init__(self, ListeDateienParNIJ):
        self.__ListeDateienParNIJ = ListeDateienParNIJ

    def ErstelleListeDateienParNIJ(self, parameterposition, i, j, n, Parametervektor, ListeWerteIParN):  # ##ListeDateienParNIJ rauslassen
        ######n: Anzahl an Parametern im Dateinamen, i,j: Parameter i wird gegen
        # ##Parameter j geplottet, sprich eine Liste mit i und j konst. wird erstellt
        # # ListeWerteIParN gibt die Liste von Werten an, die bei N Parametern der ite Parameter
        # #/Variable annehmen kann
        # #Parametervektor: Gibt die Werte von den Parametern 1,...,n aus, wobei der Wert -2 der Setzwert ist und im finalen
        # # Vektor nicht mehr zu finden sein sollte und -1 falls es sich um die ite bzw. jte Koordinate handelt
        # #-3 ist der Wert, wenn diese Variable als Option ignoriert werden soll (z.B. wenn diese abhaengig ist von anderen Variablen)
        if parameterposition != -1:
            if Parametervektor[parameterposition] != "_-3_":
                if parameterposition != i and parameterposition != j:
                    if n in ListeWerteIParN.iterkeys():
                        if parameterposition in ListeWerteIParN[n].iterkeys(
                        ):  ######## d.h es gibt eine Liste von Werte von dem i-ten Parameter bei N Parametern,
                            for g in ListeWerteIParN[n][parameterposition]:
                                a1 = []
                                a1 += Parametervektor
                                a1[parameterposition] = g
                                parameterposition1 = parameterposition - 1
                                self.ErstelleListeDateienParNIJ(parameterposition1, i, j, n, a1, ListeWerteIParN)
                else:
                    if n in ListeWerteIParN.iterkeys():
                        if parameterposition in ListeWerteIParN[n].iterkeys():
                            a1 = []
                            a1 += Parametervektor
                            g = -1
                            a1[parameterposition] = g
                            parameterposition1 = parameterposition - 1
                            self.ErstelleListeDateienParNIJ(parameterposition1, i, j, n, a1, ListeWerteIParN)

            else:  # # that means if Parametervektor[Parameterposition] == "_-3_"
                a1 = []
                a1 += Parametervektor
                parameterposition1 = parameterposition - 1
                self.ErstelleListeDateienParNIJ(parameterposition1, i, j, n, a1, ListeWerteIParN)

        else:
            if n in ListeWerteIParN.iterkeys():
                if n in self.__ListeDateienParNIJ.iterkeys():
                    if i in ListeWerteIParN[n].iterkeys():
                        if i in self.__ListeDateienParNIJ[n].iterkeys():
                            if j in ListeWerteIParN[n].iterkeys():
                                if j in self.__ListeDateienParNIJ[n][i].iterkeys():
                                    if not Parametervektor in self.__ListeDateienParNIJ[n][i][j]:
                                        self.__ListeDateienParNIJ[n][i][j].append(Parametervektor)
                                else:
                                    self.__ListeDateienParNIJ[n][i][j] = [Parametervektor]
                        else:
                            if j in ListeWerteIParN[n].iterkeys():
                                self.__ListeDateienParNIJ[n][i] = {}
                                if not j in self.__ListeDateienParNIJ[n][i].iterkeys():
                                    self.__ListeDateienParNIJ[n][i][j] = [Parametervektor]
                else:
                    if i in ListeWerteIParN[n]:
                        if j in ListeWerteIParN[n]:
                            if not n in self.__ListeDateienParNIJ.iterkeys():
                                self.__ListeDateienParNIJ[n] = {}
                                if not i in self.__ListeDateienParNIJ[n].iterkeys():
                                    self.__ListeDateienParNIJ[n][i] = {}
                                    if not j in self.__ListeDateienParNIJ[n][i].iterkeys():
                                        self.__ListeDateienParNIJ[n][i][j] = [Parametervektor]


def DrawLabels(pu, i, j, Parametervektor, Options, variableName, region, CutValue):
    # feel free to extend the dictionaries in PlotLabels.py to support your signal-model.
    # draw the actual signal process at top of the histogram.
    # Feel free to extend the dictionaries in PlotLabels.py if your process is not supported yet
    DrawProcessLabel(pu, Parametervektor, x=0.42, y=0.87)

    pu.DrawLumiSqrtS(pu.GetCanvas().GetLeftMargin(), 0.87)

    if not Options.noATLAS:
        pu.DrawAtlas(pu.GetCanvas().GetLeftMargin(), 0.92)
    if "Culumative" in variableName: variableName = variableName.replace("Culumative", "")
    if "Cumulative" in variableName: variableName = variableName.replace("Cumulative", "")

    while len(variableName) and variableName[0] == " ":
        variableName = variableName[1:]
    # we know that we have a cumulative histogram, the question is whether we are trying to find a lower bound cut (bin content is decreasing)
    # or if we are trying to find an upper bound cut (bin content is increasing)
    cut_val_str = ""
    if Options.GreaterThan:
        cut_val_str = "%s > %.0f %s" % (variableName[:variableName.rfind("[")], CutValue, "" if variableName.rfind("[") == -1 else
                                        variableName[variableName.rfind("[") + 1:variableName.rfind("]")])
    elif Options.LessThan:
        cut_val_str = "%s > %.0f %s" % (variableName[:variableName.rfind("[")], CutValue, "" if variableName.rfind("[") == -1 else
                                        variableName[variableName.rfind("[") + 1:variableName.rfind("]")])

    pu.DrawTLatex(0.42, 0.92, "%s, %s" % (region if len(Options.regionLabel) == 0 else Options.regionLabel, cut_val_str))
    ROOT.gPad.RedrawAxis()


def PreparePlotUtils(Options, name):
    ROOT.gStyle.SetPaintTextFormat(".3f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetPalette(ROOT.kViridis)

    pu = PlotUtils(status=Options.label)
    pu.size = 24
    pu.Lumi = Options.lumi

    pu.Prepare1PadCanvas(name, isQuad=Options.quadCanvas)
    can = pu.GetCanvas()

    can.SetTopMargin(0.15)
    can.SetRightMargin(0.18)
    can.SetLeftMargin(0.18)

    return pu


def styleHisto(GrdHisto, Options, i, j, Parametervektor):
    x_min = GrdHisto.GetXaxis().GetBinLowEdge(1)
    x_max = x_min + GetPlotRangeX(GrdHisto)

    y_min = GrdHisto.GetYaxis().GetBinLowEdge(1)
    y_max = y_min + GetPlotRangeY(GrdHisto)

    StylingHisto = ROOT.TH2D(id_generator(40), "Styling", GrdHisto.GetNbinsX(), x_min, x_max * Options.ExtraXoffset, GrdHisto.GetNbinsY(),
                             y_min, y_max * Options.ExtraYoffset)
    for h in [StylingHisto, GrdHisto]:
        if len(Options.xlabel) > 0:
            h.GetXaxis().SetTitle(Options.xlabel)
        else:
            DrawXaxisMassLabel(h, i, Parametervektor)

        if len(Options.ylabel):
            h.GetYaxis().SetTitle(Options.ylabel)
        else:
            DrawYaxisMassLabel(h, j, Parametervektor)
        TickX = 8
        TickY = 9
        h.GetXaxis().SetNdivisions(TickX)
        h.GetYaxis().SetNdivisions(TickY)
        if Options.ZaxisMax > 0:
            h.GetZaxis().SetRangeUser(0, Options.ZaxisMax)
        h.GetZaxis().SetLabelOffset(1.4 * h.GetZaxis().GetLabelOffset())
    StylingHisto.SetMaximum(GrdHisto.GetMaximum())
    return StylingHisto


def CreateSignificanceHistogram(Options,
                                analysis,
                                var,
                                region,
                                variableName,
                                signalpointlist,
                                CutValue,
                                n,
                                i,
                                j,
                                Parametervektor,
                                bonusstr=""):
    if len(signalpointlist) < Options.MinimumGridPoints:
        return

    CanvasName = "%s_%s_%s_%s_%s%s" % (analysis, Options.nominalName, region, var, CutValue, bonusstr)
    pu = PreparePlotUtils(Options, CanvasName)
    can = pu.GetCanvas()

    Graph = ROOT.TGraph2D()
    minLSP = 1.e8
    minNLSP = 1.e8
    maxLSP = 0
    maxNLSP = 0

    for point in signalpointlist:
        mN = point.GetParameter(i)
        mL = point.GetParameter(j)
        significance = point.get_sig(CutValue)
        Graph.SetPoint(Graph.GetN(), mN, mL, significance)
        minLSP = mL if mL < minLSP else minLSP
        minNLSP = mN if mN < minNLSP else minNLSP
        maxLSP = mL if mL > maxLSP else maxLSP
        maxNLSP = mN if mN > maxNLSP else maxNLSP

    GrdHisto = Graph.GetHistogram()
    if GrdHisto.Integral() <= 0: return

    StylingHisto = styleHisto(GrdHisto, Options, i, j, Parametervektor)
    StylingHisto.GetZaxis().SetTitle("Significance")
    GrdHisto.GetZaxis().SetTitle("Significance")
    StylingHisto.Draw("AXIS")
    GrdHisto.SetContour(2000)
    GrdHisto.Draw("same colz l")

    H167 = GrdHisto.Clone("167SIGMA")
    H167.SetContour(1)
    H167.SetContourLevel(0, 1.67)
    H167.SetLineColor(ROOT.kRed)
    H167.Draw("CONT3 same")
    H2 = GrdHisto.Clone("3SIGMA")
    H2.SetContour(1)
    H2.SetContourLevel(0, 3.)
    H2.Draw("CONT3 same")
    H3 = GrdHisto.Clone("5Sigma")
    H3.SetContour(1)
    H3.SetContourLevel(0, 5.)
    H3.SetLineStyle(ROOT.kDashed)
    H3.Draw("CONT3 same")

    DrawLabels(pu, i, j, Parametervektor, Options, variableName, region, CutValue)

    q = "_using%sand%s_" % (i, j)
    for l in range(n):
        if l == i or l == j: continue
        if Parametervektor[l] == "_-3_": continue
        q = q + "_Par%s_%s" % (l, Parametervektor[l])

    pu.saveHisto("%s/%s" % (Options.outputDir, CanvasName), Options.OutFileType)

    fixedString = "Fixed: "

    for d in range(n):
        if d == i or d == j: continue
        fixedString += "%s " % d
    pu.DrawTLatex(0.5, 0.97, "variable %s in region %s" % (var, region), 0.03, 52, 22)
    pu.DrawTLatex(0.5, 0.93, "%s" % (fixedString), 0.03, 52, 22)

    pu.GetCanvas().SaveAs("%s/AllSigGridPlots%s.pdf" % (Options.outputDir, bonusstr))


def CreateYieldHistogram(Options, analysis, var, region, variableName, signalpointlist, CutValue, n, i, j, Parametervektor, bonusstr=""):
    if len(signalpointlist) < Options.MinimumGridPoints:
        return

    CanvasName = "%s_%s_%s_%s_%s%s" % (analysis, Options.nominalName, region, var, CutValue, bonusstr)
    pu = PreparePlotUtils(Options, CanvasName)
    can = pu.GetCanvas()

    Graph = ROOT.TGraph2D()
    minLSP = 1e8
    minNLSP = 1e8
    maxLSP = 0
    maxNLSP = 0

    for point in signalpointlist:
        mN = point.GetParameter(i)
        mL = point.GetParameter(j)
        nSignalEvents = point.get_signalevents(CutValue)
        Graph.SetPoint(Graph.GetN(), mN, mL, nSignalEvents)
        minLSP = mL if mL < minLSP else minLSP
        minNLSP = mN if mN < minNLSP else minNLSP
        maxLSP = mL if mL > maxLSP else maxLSP
        maxNLSP = mN if mN > maxNLSP else maxNLSP

    GrdHisto = Graph.GetHistogram()
    if GrdHisto.Integral() <= 0: return

    style_histo = styleHisto(GrdHisto, Options, i, j, Parametervektor)
    style_histo.GetZaxis().SetTitle("Expected signal events")
    GrdHisto.GetZaxis().SetTitle("Expected signal events")
    style_histo.Draw("AXIS")
    GrdHisto.SetContour(2000)
    GrdHisto.Draw("colz l same")
    DrawLabels(pu, i, j, Parametervektor, Options, variableName, region, CutValue)

    q = "_using%sand%s_" % (i, j)
    for l in range(n):
        if l == i or l == j: continue
        if Parametervektor[l] == "_-3_": continue
        q = q + "_Par%s_%s" % (l, Parametervektor[l])
    pu.saveHisto("%s/%s" % (Options.outputDir, CanvasName), Options.OutFileType)


def CreateContaminationHistogram(Options,
                                 analysis,
                                 var,
                                 region,
                                 variableName,
                                 signalpointlist,
                                 CutValue,
                                 n,
                                 i,
                                 j,
                                 Parametervektor,
                                 bonusstr=""):
    if len(signalpointlist) < Options.MinimumGridPoints:
        return

    CanvasName = "%s_%s_%s_%s_%s%s" % (analysis, Options.nominalName, region, var, CutValue, bonusstr)
    pu = PreparePlotUtils(Options, CanvasName)
    can = pu.GetCanvas()

    Graph = ROOT.TGraph2D()
    minLSP = 1e8
    minNLSP = 1e8
    maxLSP = 0
    maxNLSP = 0

    nBackgroundEvents = signalpointlist[0].get_backgroundevents(CutValue)

    for point in signalpointlist:
        mN = point.GetParameter(i)
        mL = point.GetParameter(j)
        nSignalEvents = point.get_signalevents(CutValue)
        if (nSignalEvents + nBackgroundEvents <= 0): continue
        Graph.SetPoint(Graph.GetN(), mN, mL, nSignalEvents / (nSignalEvents + nBackgroundEvents))
        minLSP = mL if mL < minLSP else minLSP
        minNLSP = mN if mN < minNLSP else minNLSP
        maxLSP = mL if mL > maxLSP else maxLSP
        maxNLSP = mN if mN > maxNLSP else maxNLSP

    GrdHisto = Graph.GetHistogram()
    if GrdHisto.Integral() <= 0: return

    style_histo = styleHisto(GrdHisto, Options, i, j, Parametervektor)
    style_histo.GetZaxis().SetTitle("Expected signal contamination")
    GrdHisto.GetZaxis().SetTitle("Expected signal contamination")

    style_histo.Draw("AXIS")
    GrdHisto.SetContour(2000)
    GrdHisto.Draw("colz l same")
    DrawLabels(pu, i, j, Parametervektor, Options, variableName, region, CutValue)
    q = "_using%sand%s_" % (i, j)
    for l in range(n):
        if l == i or l == j: continue
        if Parametervektor[l] == "_-3_": continue
        q = q + "_Par%s_%s" % (l, Parametervektor[l])

    pu.saveHisto("%s/%s" % (Options.outputDir, CanvasName), Options.OutFileType)


def CreateHisto(Options, analysis, region, var, bonusstr=""):
    HistoSets = CreateHistoSets(Options, analysis, region, var, UseData=Options.useData)
    if not HistoSets: return
    if len(HistoSets) == 0 or not HistoSets[0].CheckHistoSet():
        print 'WARNING: Cannot calculate significance without background samples, skipping...'
        return False

    if len(HistoSets) > 1:
        print 'Currently only 1 DSConfig at a time supported'
        return False

    signallist = []
    for hset in HistoSets:
        print 'INFO: Loading histograms from DSConfig %s ...' % hset.GetName()
        SummedBackground = hset.GetSummedBackground()
        if not SummedBackground:
            print 'Failed to extract summed background histogram, exiting...'
            sys.exit(1)
        if not SummedBackground.isTH1():
            print 'This scripts only supports 1D histograms, skipping...'
            return
        variableName = SummedBackground.GetHistogram().GetXaxis().GetTitle()
        DataHist = None
        if len(hset.GetData()) > 0:
            DataHist = hset.GetData()[0]
        nSignals = len(hset.GetSignals())
        for i, Sig in enumerate(hset.GetSignals()):
            signallist.append(
                SignalPoint(Sig,
                            SummedBackground,
                            Data=DataHist,
                            BackgroundUncertainty=Options.BackgroundUncertainty,
                            UseSUSYSignificance=Options.UseSUSYSignificance))
        logging.info('Calculating significances...')
        ExecuteThreads(signallist, verbose=False, MaxCurrent=32)
        logging.info('Significance calculation done.')
        maximalsampleparameters = -1
        minimalsampleparameters = 1.e12
        for Sig2 in signallist:
            maximalsampleparameters = max(maximalsampleparameters, Sig2.GetNumberOfParameters())
            minimalsampleparameters = min(minimalsampleparameters, Sig2.GetNumberOfParameters())

        if len(signallist) == 0:
            print "ERROR: No Signalsamples found. exiting..."
            sys.exit(1)

        signal_points_list = []
        signalpointlist = []

        signalNBins = 0
        NumberOfSignalParameters = -1

        ListeDateienParN = {}

        for sample in signallist:
            if NumberOfSignalParameters == -1:
                signalhist = sample.GetHistogram()
                signalNBins = sample.GetNbins()
            NumberOfSignalParameters = sample.GetNumberOfParameters()

            if NumberOfSignalParameters in ListeDateienParN.iterkeys():
                ListeDateienParN[NumberOfSignalParameters].append(sample)
            else:
                ListeDateienParN[NumberOfSignalParameters] = [sample]

        ListeWerteIParN = {}
        for n in range(minimalsampleparameters, maximalsampleparameters + 1):
            if n in ListeDateienParN.iterkeys():
                ListeWerteIParN[n] = {}
                for sample in ListeDateienParN[n]:
                    for i in range(n):
                        if i in ListeWerteIParN[n].iterkeys():
                            if not sample.GetParameter(i) in ListeWerteIParN[n][i]:
                                ListeWerteIParN[n][i].append(sample.GetParameter(i))
                        else:
                            ListeWerteIParN[n][i] = [sample.GetParameter(i)]

        ListeDateienParNIJ = {}
        for n in range(minimalsampleparameters, maximalsampleparameters + 1):
            for i in range(n):
                for j in range(i + 1, n):
                    Parametervektor = []
                    for k in range(n):
                        if k in Options.ignore:
                            b = "_-3_"
                        else:
                            b = -2
                        Parametervektor.append(b)
                    Listenobjekt = ErstellungListe(ListeDateienParNIJ)
                    Listenobjekt.ErstelleListeDateienParNIJ(n - 1, i, j, n, Parametervektor, ListeWerteIParN)

        logging.info('Analyzing grid points available...')
        for n in range(minimalsampleparameters, maximalsampleparameters + 1):
            for i in range(n):
                if i in Options.ignore: continue
                for j in range(i + 1, n):
                    if n in ListeDateienParNIJ.iterkeys():
                        if i in ListeDateienParNIJ[n].iterkeys():
                            if j in Options.ignore: continue
                            if j in ListeDateienParNIJ[n][i].iterkeys():
                                NumbersAgreeWithOptions = True
                                if len(Options.parameters) != 0:
                                    NumbersAgreeWithOptions = False
                                    for r in range(len(Options.parameters)):
                                        if Options.parameters[r] == i:
                                            for k in range(len(Options.parameters)):
                                                if Options.parameters[k] == j:
                                                    NumbersAgreeWithOptions = True
                                if NumbersAgreeWithOptions == False: continue

                                print 'INFO: This may take a while, maybe consider to apply the "--ignore" option'
                                for Parametervektor in ListeDateienParNIJ[n][i][j]:
                                    signalpointlist = []
                                    signalcoords = []
                                    for sample in signallist:
                                        if sample.GetNumberOfParameters() != n:
                                            continue
                                        SampleAgreesWithVector = True
                                        for d in range(n):
                                            if d == i or d == j: continue
                                            if Parametervektor[d] == "_-3_":
                                                continue  # is ignored parameter
                                            if sample.GetParameter(d) != Parametervektor[d]:  # check if all other parameters agree
                                                SampleAgreesWithVector = False
                                            if len(Options.ignore) > 0:
                                                for xcoord, ycoord in signalcoords:
                                                    if sample.GetParameter(i) == xcoord and sample.GetParameter(j) == ycoord:
                                                        SampleAgreesWithVector = False
                                        if SampleAgreesWithVector:
                                            signalcoords.append((sample.GetParameter(i), sample.GetParameter(j)))
                                            signalpointlist.append(sample)

                                    if len(signalpointlist) == 0:
                                        continue

                                    TestSignal = signalpointlist[0]
                                    signalNBins = TestSignal.GetNbinsX()

                                    if not isinstance(TestSignal.GetParameter(i), int):
                                        continue
                                    if not isinstance(TestSignal.GetParameter(j), int):
                                        continue

                                    for b in range(1, signalNBins):
                                        CutValue = signalhist.GetXaxis().GetBinLowEdge(b)
                                        if Options.PointsToDraw is not None and len(
                                                Options.PointsToDraw) != 0 and (not CutValue in Options.PointsToDraw):
                                            continue
                                        if Options.PlotYields:
                                            CreateYieldHistogram(Options, analysis, var, region, variableName, signalpointlist, CutValue, n,
                                                                 i, j, Parametervektor, bonusstr)
                                        elif Options.PlotContamination:
                                            CreateContaminationHistogram(Options, analysis, var, region, variableName, signalpointlist,
                                                                         CutValue, n, i, j, Parametervektor, bonusstr)
                                        else:
                                            CreateSignificanceHistogram(Options, analysis, var, region, variableName, signalpointlist,
                                                                        CutValue, n, i, j, Parametervektor, bonusstr)


def setupSignificanceParser():
    parser = argparse.ArgumentParser(
        description=
        'This script produces MC only significance histograms from tree files. For more help type \"python SignificancePlots.py -h\"',
        prog='SignificancePlots',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.add_argument('-d', "--useData", action="store_true", default=False)
    parser.add_argument("--UseSUSYSignificance",
                        help="Use the Buttinger significance definition instead",
                        action="store_true",
                        default=False)
    parser.add_argument('--PlotYields', help='Plot yields instead of significances', action='store_true', default=False)
    parser.add_argument('--SoverB', help='Draw signal over background in ratio panel', action='store_true', default=False)
    parser.add_argument('--PlotContamination',
                        help='Plot signal contamination (S/[S+B]) instead of significances',
                        action='store_true',
                        default=False)
    parser.add_argument('--noStack', help='do not stack the different MC samples', action='store_true', default=False)
    parser.add_argument('--ShapeComp', help='draw different MC samples normalized to unit area', action='store_true', default=False)
    parser.add_argument("--RatRelCont",
                        help="Draw the relative contributions of the backgrounds to the total in the Ratio panel",
                        action='store_true',
                        default=False)
    parser.add_argument('--RatioSmp', help='Specify the sample label for the ratio to be drawn', default='')
    parser.add_argument('--xlabel', help='Specify the sample label for the x-axis', default="")  #"m_{#tilde{t}_{1}} [GeV]")
    parser.add_argument('--ylabel', help='Specify the sample label for the y-axis', default="")  #"m_{#tilde{#chi}^{0}_{1}} [GeV]")
    parser.add_argument(
        '--parameters',
        help=
        'Only these parameters will be plotted against each other. If the list is empty, then every parameter will be plotted against every parameter',
        default=[],
        type=int,
        nargs='+')
    parser.add_argument('--ignore', help='These parameters will be ignored', default=[], type=int, nargs='+')
    parser.add_argument('--MinimumGridPoints', help='minimum number of grid points needed to produce a plot', type=int, default=4)
    parser.add_argument('--BackgroundUncertainty',
                        help='specify relative uncertainty on background for significance calculation',
                        type=float,
                        default=0.3)
    parser.add_argument('--CutValueDigits', help='number of digits for the cut value print', type=int, default=0)
    parser.add_argument('--ZaxisMax',
                        help='Set the maximum of the z-axis. If not defined the highest Significance value is used',
                        type=float,
                        default=-1)
    parser.add_argument('--GreaterThan',
                        help="Draw the '>' sign on the plot legend. If this and LessThan are not set, this is automatic (but buggy).",
                        action='store_true',
                        default=False)
    parser.add_argument('--LessThan',
                        help="Draw the '<' sign on the plot legend. If this and GreaterThan are not set, this is automatic (but buggy).",
                        action='store_true',
                        default=False)
    parser.add_argument('--PointsToDraw', help="Points of the histogram to draw", type=float, nargs='+')

    parser.add_argument('--ExtraXoffset', help="How large is the offset along the x-axis", type=float, default=1.05)
    parser.add_argument('--ExtraYoffset', help="How large is the offset along the y-axis", type=float, default=1.00)
    return parser


if __name__ == "__main__":

    PlottingOptions = setupSignificanceParser().parse_args()

    if PlottingOptions.PlotYields and PlottingOptions.PlotContamination:
        print "Pick either PlotYields or PlotContamination or neither!"
        sys.exit(1)

    if PlottingOptions.GreaterThan and PlottingOptions.LessThan:
        print "Pick GreaterThan or LessThan or neither (automatic)"
        sys.exit(1)

    if os.path.isdir(PlottingOptions.outputDir) == False:
        os.system("mkdir -p " + PlottingOptions.outputDir)

    FileStructure = GetStructure(PlottingOptions)

    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    print '\n\n*****************\nThis script only works with cumulative histograms! No integration is performed here!\n*****************\n\n'

    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)

    bonusstr = ""
    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            if PlottingOptions.doLogY:
                bonusstr += '_LogY'
            dummy.SaveAs("%s/AllSigGridPlots%s.pdf[" % (PlottingOptions.outputDir, bonusstr))
            for var in FileStructure.GetConfigSet().GetVariables(ana, region):
                CreateHisto(PlottingOptions, ana, region, var, bonusstr=bonusstr)
            dummy.SaveAs("%s/AllSigGridPlots%s.pdf]" % (PlottingOptions.outputDir, bonusstr))

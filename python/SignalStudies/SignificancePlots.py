#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
from XAMPPplotting.Utils import setupBaseParser
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.FileStructureHandler import GetStructure
from XAMPPplotting.StatisticalFunctions import *
from ClusterSubmission.Utils import CreateDirectory
import logging


def CreateSiggiHistos(Signals, Background, RelUnc=0.3, ScaleSig=1., DoIntegral=False, UseSUSYSignificance=False):
    SignificanceHistos = []
    for N in Signals:
        Num = N.GetHistogram().Clone("Significance_" + N.GetHistogram().GetName())
        Num.Reset()
        for b in range(Utils.GetNbins(Num)):
            bkg_value = Background.GetBinContent(b)
            bkg_stat = Background.GetBinError(b)
            sig_value = N.GetBinContent(b)
            sig_stat = N.GetBinError(b)

            if DoIntegral:
                bkg_value, bkg_stat = Background.IntegrateWithError(b)
                sig_value, sig_stat = N.IntegrateWithError(b)

            rel_unc = RelUnc

            Sigma = ButtingerLefebvreSignificance(sig_value, bkg_value, rel_unc *
                                                  bkg_value) if UseSUSYSignificance else ROOT.RooStats.NumberCountingUtils.BinomialExpZ(
                                                      sig_value, bkg_value, rel_unc)

            if math.isnan(Sigma) or math.isinf(Sigma): Sigma = 0.
            Sigma = min(max(Sigma, 0.), 10)
            Num.SetBinContent(b, Sigma)
        SignificanceHistos.append(Num)
    return SignificanceHistos


def drawHisto(Options, analysis, region, var, bonusstr=""):
    HistList = []
    HistToDraw = []

    pu = PlotUtils(status=Options.label)
    pu.Size = 24
    pu.Lumi = Options.lumi

    HistoSets = CreateHistoSets(Options, ana, region, var, UseData=False)
    if not HistoSets or len(HistoSets) == 0 or not HistoSets[0].CheckHistoSet():
        print 'WARNING: Cannot calculate significance without background samples, skipping...'
        return False

    if len(HistoSets) > 1:
        print 'Currently only 1 DSConfig at a time supported'
        return False

    DefaultSet = HistoSets[-1]
    if Options.noSignal or len(DefaultSet.GetSignals()) == 0:
        print "This is significance plots without giving a signal samples this will be a useless journey"
        return False

    Significances = CreateSiggiHistos(DefaultSet.GetSignals(), DefaultSet.GetSummedBackground(), Options.BkgUncertainty,
                                      Options.SignalScale, Options.DoIntegral, Options.UseSUSYSignificance)
    if DefaultSet.GetSummedBackground().isTH2():
        drawInt2DHisto(pu, DefaultSet, Options, region, DrawData=False, PlotType="MCSig", SummaryFile="AllSignificancePlots%s" % bonusstr)
        return
    elif DefaultSet.GetSummedBackground().isTH3():
        return
    elif DefaultSet.GetSummedBackground().isTH2Poly():
        return

    # Create the TCanvas
    logstr = "" if not Options.doLogY else "_LogY"

    if Options.noDistribution:
        pu.Prepare1PadCanvas("Siggi_%s_%s_%s%s%s" % (var, Options.nominalName, region, bonusstr, logstr), 800, 600, Options.quadCanvas)
    else:
        pu.Prepare2PadCanvas("Siggi_%s_%s_%s%s%s" % (var, Options.nominalName, region, bonusstr, logstr), 800, 600, Options.quadCanvas)
        pu.GetTopPad().cd()
        if Options.doLogY:
            pu.GetTopPad().SetLogy()

    pu.CreateLegend(Options.LegendXCoords[0],
                    Options.LegendYCoords[0],
                    Options.LegendXCoords[1],
                    Options.LegendYCoords[1],
                    textsize=Options.LegendTextSize)

    # Create Stack containing all the backgrounds in it
    if not Options.noDistribution:
        stk = Stack(DefaultSet.GetBackgrounds())
        for Sig in DefaultSet.GetSignals(not Options.noSignalStack):
            HistList.append(Sig)
            HistToDraw.append((Sig, "same" + Sig.GetDrawStyle()))

        # draw the stack in order to get the histogram for the axis ranges
        if not Options.noSyst:
            HistList += [S.final_up_histo().get() for S in DefaultSet.GetSummedBackground().GetSystComponents()]
            HistList += [S.final_dn_histo().get() for S in DefaultSet.GetSummedBackground().GetSystComponents()]

        for Sig in DefaultSet.GetSignals():
            pu.AddToLegend([Sig], Style=Sig.GetLegendDrawStyle())

        pu.AddToLegend(DefaultSet.GetBackgrounds(), Style="FL")
        HistList += [DefaultSet.GetSummedBackground()]

    else:
        DefaultSet.GetSummedBackground().GetAxis(1).SetTitle("Significance")
        HistList = Significances
        HistToDraw = [(a, "sameHist") for a in Significances]

    ymin, ymax = pu.GetFancyAxisRanges(HistList, Options)
    if math.fabs(ymin) <= 1.e-10 and math.fabs(ymax) <= 1.e-10:
        print "Night time", ymin, "day time", ymax
        return False
    pu.drawStyling(DefaultSet.GetSummedBackground(), ymin, ymax, RemoveLabel=not Options.noRatio, TopPad=not Options.noRatio)

    if not Options.noDistribution:
        stk.Draw("sameHist")
        pu.drawSumBg(DefaultSet.GetSummedBackground(), not Options.noSyst)
    for H, Opt in HistToDraw:
        H.Draw(Opt)

    pu.DrawPlotLabels(0.195, 0.83, Options.regionLabel if len(Options.regionLabel) > 0 else region, analysis, Options.noATLAS)

    pu.DrawLegend()

    if not Options.noDistribution:
        pu.GetTopPad().RedrawAxis()
        pu.GetBottomPad().cd()
        pu.GetBottomPad().SetGridy()
        RatioLabel = "Significance"
        if len(Options.LowerPadRanges) == 2:
            pu.drawRatioStyling(DefaultSet.GetSummedBackground(), float(Options.LowerPadRanges[0]), float(Options.LowerPadRanges[1]),
                                RatioLabel)
        else:
            y_max = min(6.9, max([r.GetMaximum() for r in Significances]) * 1.05)
            DefaultSet.GetSummedBackground().GetXaxis().SetTitle(DefaultSet.GetSummedBackground().GetXaxis().GetTitle().replace(
                "Cumulative ", ""))
            pu.drawRatioStyling(DefaultSet.GetSummedBackground(), 0., y_max, RatioLabel)
        for r in Significances:
            r.Draw("sameHist")

    ROOT.gPad.RedrawAxis()

    PreString = analysis + "_" + region

    if not Options.summaryPlotOnly:
        pu.saveHisto("%s/SiggiPlots_%s_%s%s" % (Options.outputDir, PreString, var, logstr), Options.OutFileType)

    if not Options.noRatio:
        pu.GetTopPad().cd()

    pu.DrawTLatex(0.5, 0.94, "%s%s in %s (%s analysis)" % (var, logstr, region, analysis), 0.04, 52, 22)
    pu.GetCanvas().SaveAs("%s/AllSignificancePlots%s.pdf" % (Options.outputDir, bonusstr))


def drawInt2DHisto(PlotUtil, HistoSet, Options, region, DrawData=True, PlotType="MCSig", SummaryFile="AllSignificancePlots"):

    ROOT.gStyle.SetPaintTextFormat(".3f")
    ROOT.gStyle.SetTextFont(42)

    xW = 800
    if Options.quadCanvas:
        xW = 600

    signals = HistoSet.GetSignals()
    totalBackground = HistoSet.GetSummedBackground().GetHistogram()

    for sig in signals:
        sigHisto = totalBackground.Clone("%s_%s_sig" % (totalBackground.GetName(), sig.GetName()))
        sigHisto.Reset()

        for i in range(0, Utils.GetNbins(totalBackground)):
            Sigma = 0.
            if sig.GetHistogram().GetBinContent(i) > 0 and not sig.GetHistogram().GetBinContent(i) < sig.GetHistogram().GetBinError(i):
                if sig.GetHistogram().GetBinError(i) / sig.GetHistogram().GetBinContent(i) > 0.5:
                    print 'WARNING: Very low stats in signal: %.4f pm %.4f, returning significance of 0...' % (
                        sig.GetHistogram().GetBinContent(i), sig.GetHistogram().GetBinError(i))
                    continue
                else:
                    Sigma = ButtingerLefebvreSignificance(
                        sig.GetHistogram().GetBinContent(i), totalBackground.GetBinContent(i),
                        Options.BkgUncertainty) if Options.UseSUSYSignificance else ROOT.RooStats.NumberCountingUtils.BinomialExpZ(
                            sig.GetHistogram().GetBinContent(i), totalBackground.GetBinContent(i), Options.BkgUncertainty)
                    if Sigma < 0: Sigma = 0.
            sigHisto.SetBinContent(i, Sigma)
        if not sigHisto.Integral() > 0.:
            print 'WARNING: Empty significance histogram, skipping...'
            return

        PlotName = "%s_2D_%s_%s_%s" % (PlotType, sig.GetName(), region, var)
        PlotUtil.Prepare1PadCanvas(PlotName, xW, 600)
        can = PlotUtil.GetCanvas()
        sigHisto.GetZaxis().SetTitle("Significance")
        sigHisto.GetZaxis().SetLabelSize(sigHisto.GetYaxis().GetLabelSize())
        sigHisto.GetZaxis().SetTitleSize(sigHisto.GetYaxis().GetTitleSize())
        sigHisto.Draw("colz l")

        can.SetTopMargin(0.15)
        can.SetRightMargin(0.18)
        if not Options.noATLAS:
            PlotUtil.DrawAtlas(can.GetLeftMargin(), 0.92)
        PlotUtil.DrawLumiSqrtS(can.GetLeftMargin(), 0.87)
        if not Options.summaryPlotOnly:
            PlotUtil.saveHisto("%s/%s" % (Options.outputDir, PlotName), Options.OutFileType)

        PlotUtil.DrawTLatex(0.5, 0.94, "%s " % (sig.GetName()), 0.04, 52, 22)
        can.SaveAs("%s/%s.pdf" % (Options.outputDir, SummaryFile))


def setup_significance_parser():
    parser = argparse.ArgumentParser(
        description=
        'This script produces MC only significance histograms from tree files. For more help type \"python SignificancePlots.py -h\"',
        prog='SignificancePlots',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.add_argument("--UseSUSYSignificance", action="store_true", default=False)
    parser.add_argument("--BkgUncertainty", help="Assumed uncertainty on the background", default=0.3, type=float)
    parser.add_argument("--SignalScale", help="Scales the signal prediction", default=1., type=float)
    parser.add_argument("--noDistribution", help="Only the significances themselves are plotted", action='store_true', default=False)
    parser.add_argument("--DoIntegral",
                        help="Do you need to take the Integral in order to get the cumulative histogram and the right significance?",
                        action='store_true',
                        default=False)
    parser.set_defaults(outputDir="SignficancePlots/")
    parser.set_defaults(LegendXCoords=[0.42, 0.94])
    parser.set_defaults(LegendYCoords=[0.45, 0.87])
    parser.set_defaults(lumi=139.)
    return parser


if __name__ == "__main__":

    PlottingOptions = setup_significance_parser().parse_args()

    CreateDirectory(PlottingOptions.outputDir, False)

    if len(PlottingOptions.config) == 0:
        logging.error('Please specify a DSConfig to use for printing significances!')
        sys.exit(1)

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = GetStructure(PlottingOptions)

    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)

    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            bonusstr = '_%s_%s' % (ana, region)
            if PlottingOptions.doLogY:
                bonusstr += '_LogY'
            dummy.SaveAs("%s/AllSignificancePlots%s.pdf[" % (PlottingOptions.outputDir, bonusstr))
            for var in FileStructure.GetConfigSet().GetVariables(ana, region):
                drawHisto(PlottingOptions, ana, region, var, bonusstr=bonusstr)
            dummy.SaveAs("%s/AllSignificancePlots%s.pdf]" % (PlottingOptions.outputDir, bonusstr))

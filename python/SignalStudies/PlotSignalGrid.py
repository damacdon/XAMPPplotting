#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
import XAMPPplotting.PrintYields
import XAMPPplotting.FileStructureHandler
import XAMPPplotting.StatisticalFunctions
import XAMPPplotting.PlotLabels

from array import array
from pprint import pprint
from XAMPPplotting.StatisticalFunctions import *

TObjects = []


def GetGridRow(GridValues, LSP=-1):
    Row = {}
    for mN, mL in GridValues.iterkeys():
        if mL == LSP:
            Row[mN] = GridValues[(mN, mL)]


def GetGridPoints(GridValues):
    NLSPmasses = []
    [NLSPmasses.append(m[0]) for m in GridValues.iterkeys() if not m[0] in NLSPmasses]
    NLSPmasses = sorted(NLSPmasses)
    LSPmasses = {}
    for mN in NLSPmasses:
        LSPmasses[mN] = sorted([k[1] for k in GridValues.iterkeys() if k[0] == mN and k[1] > -1])
    return NLSPmasses, LSPmasses


def GetGridRow(GridValues, LSP=-1):
    Row = {}
    for mN, mL in GridValues.iterkeys():
        if mL == LSP:
            Row[mN] = GridValues[(mN, mL)]
    return Row


def CalculateZnGrid(GridYields, BgYield, RelUncert=0.2):
    Zn = {}
    for N, L in GridYields:
        Signal = GridYields[(N, L)]
        Background = BgYield
        PseudoSig = Background + Signal
        # ZnValue = XAMPPplotting.StatisticalFunctions.sigmaZn(PseudoSig, Background, RelUncert * Background)
        ZnValue = ROOT.RooStats.NumberCountingUtils.BinomialExpZ(Signal, Background, RelUncert)
        if ZnValue >= 10.:
            ZnValue = 10.
        Zn[(N, L)] = ZnValue
    return Zn


def GetCutFlows(Options, ana, region):
    CutFlowHisto = "InfoHistograms/CutFlow_weighted"
    FullHistoSet = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, ana, region, CutFlowHisto, UseData=False)[-1]
    return FullHistoSet


def GetSignalYieldsFromCutFlow(CutFlows):
    Signals = {}
    for S in CutFlows.GetSignals():
        #Discard all files which are not part of the model we're interested in
        if not S.GetName().startswith(Options.Model) or (len(Options.Couplings) > 0 and not S.GetName().endswith("_" + Options.Couplings)):
            continue
        S.SetLumi(Options.SignalScale * S.GetLumi())
        LSP, NLSP = XAMPPplotting.Utils.ExtractMasses(S.GetName())
        Y = XAMPPplotting.PrintYields.getSampleYield(S, False)
        #Signals [ (LSP,NLSP) ] = ( Y["Content"] , Y["Stat"])
        Signals[(LSP, NLSP)] = Y["Content"]

    return Signals


def GetBkgYieldFromCutFlow(CutFlows):
    if CutFlows.GetSummedBackground():
        return XAMPPplotting.PrintYields.getSampleYield(CutFlows.GetSummedBackground(), False)["Content"]
    return -1.


def GetGridHistogram(GridValues):
    NLSPmasses, LSPmasses = GetGridPoints(GridValues)
    Graph = ROOT.TGraph2D()
    TObjects.append(Graph)
    for mN in NLSPmasses:
        for mL in LSPmasses[mN]:
            Graph.SetPoint(Graph.GetN(), mN, mL, GridValues[(mN, mL)])
    GrdHisto = Graph.GetHistogram()
    GrdHisto.SetContour(99)
    GrdHisto.GetZaxis().SetLabelOffset(1.4 * GrdHisto.GetZaxis().GetLabelOffset())
    GrdHisto.GetXaxis().SetNdivisions(7)
    GrdHisto.GetYaxis().SetNdivisions(9)
    return GrdHisto


def DrawHistogram1D(Options, Column, HistoName, region, Yaxis, MaxY=-1, AddInfo=""):
    if len(Column) == 0:
        return False

    pu = PlotUtils(status=Options.label, size=24, lumi=Options.lumi)
    pu.Prepare1PadCanvas(HistoName, 800, 600, Options.quadCanvas)

    H1 = ROOT.TH1D("TH1" + HistoName, "TH1", len(Column), 0, len(Column))
    XAMPPplotting.PlotLabels.DrawXaxisMassLabel(H1, 1, ("%s_%s" % (Options.Model, Options.Couplings)))
    H1.GetYaxis().SetTitle(Yaxis)
    H1.SetMinimum(Options.Minimum)
    if MaxY > -1:
        H1.SetMaximum(MaxY)
    else:
        H1.SetMaximum(H1.GetMaximum() * 1.4)
    Bin = 1
    for N in sorted(Column):
        H1.GetXaxis().SetBinLabel(Bin, str(N))
        H1.SetBinContent(Bin, Column[N])
        Bin += 1

    H1.Draw("HIST")

    pu.DrawPlotLabels(0.195, 0.83, Options.regionLabel if len(Options.regionLabel) > 0 else region, "DeineMutter", Options.noATLAS)
    pu.saveHisto("%s/%s" % (Options.outputDir, HistoName), Options.OutFileType)
    return True


def DrawHistogram(Options, Grid, HistoName, Zaxis, Maximum=-1., DrawCont=True, AddInfo=""):

    if len(Grid) == 0:
        return False
    Row = GetGridRow(Grid)
    if len(Row) > 0.:
        return DrawHistogram1D(Options, Row, HistoName, Zaxis, Maximum, AddInfo)
    pu = PlotUtils(status=Options.label, size=24, lumi=Options.lumi)
    pu.Prepare1PadCanvas(HistoName, 800, 600, Options.quadCanvas)

    ROOT.gPad.SetRightMargin(0.22)
    ROOT.gPad.SetTopMargin(0.1)

    if Options.doLogY:
        pu.GetCanvas().SetLogz()
        HistoName += "_LogZ"

    H1 = GetGridHistogram(Grid)
    XAMPPplotting.PlotLabels.DrawXaxisMassLabel(H1, 1, ("%s_%s" % (Options.Model, Options.Couplings)))
    XAMPPplotting.PlotLabels.DrawYaxisMassLabel(H1, 2, ("%s_%s" % (Options.Model, Options.Couplings)))
    H1.GetZaxis().SetTitle(Zaxis)
    if Maximum > 0:
        H1.SetMaximum(Maximum)
    else:
        H1.SetMaximum(H1.GetMaximum() * 1.4)
    H1.SetMinimum(Options.Minimum)

    H1.Draw("COLZ")
    if DrawCont == True:
        H2 = H1.Clone("3Sigma")
        H2.SetContour(1)
        H2.SetContourLevel(0, 1.65)
        H2.Draw("CONT3 same")
        H3 = H1.Clone("5Sigma")
        H3.SetContour(1)
        H3.SetContourLevel(0, 3.)
        H3.SetLineStyle(ROOT.kDashed)
        H3.Draw("CONT3 same")

    x0 = 0.05
    y0 = 0.95
    if len(AddInfo) > 0:
        pu.DrawTLatex(0.22, 0.75, AddInfo, 26)
    pu.DrawLumiSqrtS(x0, y0, 11)
    if not Options.noATLAS:
        pu.DrawAtlas(x0 + 0.5, y0, 11)

    pu.saveHisto("%s/%s" % (Options.outputDir, HistoName), Options.OutFileType)


def PlotSignal(Options, SignalYields, BkgYield, ana, region):
    Zn = {}
    if Options.DoZn:
        Zn = CalculateZnGrid(SignalYields, BkgYield, RelUncert=Options.BkgUncertainty)

    #Check whethe a row or a grid has been given...
    HistName = "Yield_%s_%s_%s_%s_%.f" % (Options.Model, Options.Couplings, ana, region, Options.lumi)
    DrawHistogram(Options,
                  SignalYields,
                  HistName,
                  Zaxis=Options.zTitle,
                  Maximum=Options.Maximum,
                  DrawCont=False,
                  AddInfo=region if BkgYield <= 0. else "#splitline{Bkg: %.2f}{%s}" % (BkgYield, region))
    if Options.DoZn:
        HistName = "Zn_%s_%s_%s_%s_%.f" % (Options.Model, Options.Couplings, ana, region, Options.lumi)
        DrawHistogram(Options, SignalYields, HistName, Zaxis="Significance", Maximum=Options.MaxZn, DrawCont=True, AddInfo=region)

    return True


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description=
        'This script produces MC only significance histograms from tree files. For more help type \"python SignificancePlots.py -h\"',
        prog='SignificancePlots',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = XAMPPplotting.Utils.setupBaseParser(parser)
    parser.add_argument("--Model", help="Which model do you want to get yields from", required=True)
    parser.add_argument("--Couplings", help="Couplings to constrain the model?", default="")
    parser.add_argument("--CutSignificance", help="Scales the signal prediction", default=5., type=float)
    parser.add_argument("--CutYields", help="Scales the signal prediction", default=-1, type=float)
    parser.add_argument("--BkgUncertainty", help="Assumed uncertainty on the background", default=0.3, type=float)
    parser.add_argument("--SignalScale", help="Scales the signal prediction", default=1., type=float)
    parser.add_argument("--zTitle", help="Title of the z axis", default="Expected events")
    parser.add_argument("--MaxZn", help="Maximum Zn value to plot", type=float, default=8.)
    parser.add_argument("--Maximum", help="Maximum range of the histogram", type=float, default=-1.)
    parser.add_argument("--Minimum", help="Minimum range of the histogram", type=float, default=0.)
    parser.add_argument("--DoZn", help="Also create the Zn plot of the grid", action="store_true", default=False)
    parser.set_defaults(lumi=36.1)

    #parser.add_argument("--DoNotUseCutFlow
    Options = parser.parse_args()

    if os.path.isdir(Options.outputDir) == False:
        os.system("mkdir -p " + Options.outputDir)

    if len(Options.config) == 0:
        print 'Please specify a DSConfig to use for printing significances!'
        sys.exit(1)

    # Analyze the samples and adjust the Options
    FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(Options)

    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")
    ROOT.gStyle.SetPaintTextFormat(".3f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetPalette(55)

    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)

    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            CutFlows = GetCutFlows(Options, ana, region)
            SignalYields = GetSignalYieldsFromCutFlow(CutFlows)
            BkgYield = GetBkgYieldFromCutFlow(CutFlows)
            PlotSignal(Options, SignalYields, BkgYield, ana, region)

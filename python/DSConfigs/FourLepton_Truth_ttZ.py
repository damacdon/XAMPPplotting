#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.FileStructureHandler import GetFileHandler, GetSystPairer

GetSystPairer().pair_systematics("A14VarDn", "A14VarUp")
GetSystPairer().set_systematic_title("multileg", "Generator")
GetSystPairer().set_systematic_title("A14VarDn", "A14 tune")
GetSystPairer().set_systematic_title("A14VarUp", "A14 tune")

Path = " /ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-01-26/Truth_Syst/"
Path1 = " /ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-01-26/Truth_Syst_combined/"

#GetSystPairer().create_envelope("PDFset=260")
## python XAMPPplotting/python/MCPlots.py  -c XAMPPplotting/python/DSConfigs/FourLepton_Truth_ttZ.py --RatioSmp Nominal --noStack --regions 4L
Nominal = DSconfig(
    colour=ROOT.kBlack,  #ROOT.TColor.GetColor(111,224,5) , 
    label="t#bar{t}Z (Nominal)",
    name="Nominal",
    filepath=Path1 + "aMCatNLOPy8_ttZ.root",
    fillstyle=0,
    markerstyle=0,
    markersize=0,
    sampletype=SampleTypes.Irreducible,
    LegendOrder=3)

GetFileHandler().LoadFile(Nominal.Filepaths[0])

RENORM__1UP = DSconfig(colour=ROOT.kGreen + 3,
                       label="t#bar{t}Z (#mu_{R}#uparrow)",
                       name="RENORM__1UP",
                       Nominal="RENORM__1UP",
                       ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                       filepath=Path + "aMCatNLOPy8_ttZ.root",
                       fillstyle=0,
                       markerstyle=0,
                       markersize=0,
                       sampletype=SampleTypes.Irreducible,
                       LegendOrder=4)

RENORM__1DOWN = DSconfig(colour=ROOT.kGreen - 2,
                         label="t#bar{t}Z (#mu_{R}#downarrow)",
                         name="RENORM__1DOWN",
                         Nominal="RENORM__1DOWN",
                         ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                         filepath=Path + "aMCatNLOPy8_ttZ.root",
                         fillstyle=0,
                         markerstyle=0,
                         markersize=0,
                         sampletype=SampleTypes.Irreducible,
                         LegendOrder=2)

FACTOR__1UP = DSconfig(colour=ROOT.kBlue + 2,
                       label="t#bar{t}Z (#mu_{F}#uparrow)",
                       name="FACTOR__1UP",
                       Nominal="FACTOR__1UP",
                       ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                       filepath=Path + "aMCatNLOPy8_ttZ.root",
                       fillstyle=0,
                       markerstyle=0,
                       markersize=0,
                       sampletype=SampleTypes.Irreducible,
                       LegendOrder=5)

FACTOR__1DOWN = DSconfig(colour=ROOT.kBlue - 3,
                         label="t#bar{t}Z (#mu_{F}#downarrow)",
                         name="FACTOR__1DOWN",
                         Nominal="FACTOR__1DOWN",
                         ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                         filepath=Path + "aMCatNLOPy8_ttZ.root",
                         fillstyle=0,
                         markerstyle=0,
                         markersize=0,
                         sampletype=SampleTypes.Irreducible,
                         LegendOrder=1)
Generator = DSconfig(colour=ROOT.kViolet + 1,
                     label="ttZ (Sherpa)",
                     name="Generator",
                     ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                     filepath=Path + "Sherpa221_ttZ_multileg.root",
                     fillstyle=0,
                     markerstyle=0,
                     markersize=0,
                     sampletype=SampleTypes.Irreducible,
                     LegendOrder=1)
#

#for syst in GetSystPairer().get_systematics():
#    if syst.find("PDFset") != -1: GetSystPairer().exclude_systematic(syst)

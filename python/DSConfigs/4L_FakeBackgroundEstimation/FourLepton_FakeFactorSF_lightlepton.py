import ROOT, os, sys
from XAMPPplotting.Defs import *
from XAMPPplotting.CheckMetaData import GetNormalizationDB
#from XAMPPplotting.PeriodRunConverter import GetPeriodRunConverter
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi

from XAMPPplotting.FileUtils import ResolvePath, ReadInputConfig

PATH = "/ptmp/mpp/maren/Cluster/OUTPUT/2019-11-18/4L_Scalefactor_lightlepton/"

# ~ CONFIGPATH = ResolvePath("XAMPPmultilep/data/InputConf/MPI/SFAnalysis/")
CONFIGPATH = ResolvePath("v26_samples/")

Periods = [Cfg.replace(".conf", "") for Cfg in os.listdir(CONFIGPATH) if Cfg.find("data") != -1]

CalcLumi = False

if CalcLumi:
    Files = []
    for P in Periods:
        Files.extend(ReadInputConfig("%s/%s.conf" % (CONFIGPATH, P)))
    LUMI = 0.
    for R in GetNormalizationDB(Files).GetRunNumbers():
        LUMI += CalculateRecordedLumi(R)
    print LUMI
    exit(1)
else:
    LUMI = 138.964783162

Data = DSconfig(lumi=LUMI,
                colour=ROOT.kBlack,
                label="Data",
                name="Data",
                filepath=["%s/%s.root" % (PATH, P) for P in Periods],
                sampletype=SampleTypes.Data)

ttbar = DSconfig(
    colour=ROOT.kBlue - 2,
    name="ttbar",
    label="top",
    filepath=[PATH + "PowHegPy8_ttbar_incl.root"],  #please use only PowhegPy_top.root when xsec for single top is OK
    sampletype=SampleTypes.Irreducible)

Zjets = DSconfig(
    colour=ROOT.kViolet - 9,
    name="Zjets",
    label="Z+Jets",
    filepath=[
        PATH + "PowHegPy8_Zee.root", PATH + "PowHegPy8_Ztautau.root"
        #PATH + "Sherpa221_Zee.root",
        #PATH + "Sherpa221_Ztautau.root"
    ],
    sampletype=SampleTypes.Irreducible)

Zmm = DSconfig(
    colour=ROOT.kAzure,
    name="Zmumu",
    label="Z#mu#mu",
    filepath=[
        PATH + "PowHegPy8_Zmumu.root"
        #PATH + "Sherpa221_Zmumu.root"
    ],
    sampletype=SampleTypes.Irreducible)

Wjets = DSconfig(
    colour=ROOT.kViolet - 7,
    name="Wjets",
    label="W+Jets",
    filepath=[
        #PATH + "PowHegPy8_Wenu.root",
        #PATH + "PowHegPy8_Wmunu.root",
        #PATH + "PowHegPy8_Wtaunu.root"
        PATH + "Sherpa221_Wenu.root",
        PATH + "Sherpa221_Wmunu.root",
        PATH + "Sherpa221_Wtaunu.root"
    ],
    sampletype=SampleTypes.Irreducible)

VV = DSconfig(colour=ROOT.kGreen - 3,
              name="Multibosons",
              label="Multibosons",
              filepath=[PATH + "Sherpa221_VV.root", PATH + "Sherpa221_VVV.root"],
              sampletype=SampleTypes.Irreducible)

tV = DSconfig(
    colour=ROOT.kYellow + 1,
    name="ttV_tVV_ttVV",  #do not use /
    label="top+V",
    filepath=[
        PATH + "Sherpa_ttV.root", PATH + "aMCatNLOPy8_ttW.root", PATH + "aMCatNLOPy8_ttZ.root", PATH + "MG5Py8_ttWW.root",
        PATH + "MG5Py8_ttWZ.root"
        #PATH + "aMCAtNLO_ttY.root",
        #PATH + "aMCAtNLO_ttZ.root",
        #PATH + "aMCatNLOPy8_ttV.root",
        #PATH + "aMcAtNlo_tWZ.root"
    ],
    sampletype=SampleTypes.Irreducible)

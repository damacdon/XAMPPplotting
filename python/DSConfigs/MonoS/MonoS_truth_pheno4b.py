#!/usr/bin/env python
import os
from XAMPPplotting.Defs import *
ROOT.gStyle.SetOptStat(0)
BasePath = '/ptmp/mpp/pgadow/monoS/plotting/monoSbb_phenopaper'

# Specify samples
# Signal MC
ds_signal = os.path.join(BasePath, "MonoSbb_dsid600004_zp1100_dm100_hs70.root")
monoSbb_zp1100_dm100_hs70 = DSconfig(name="monoSbb_zp1100_dm100_hs70",
                                     label="(m_{Z'},m_{hs})=(1100,70)",
                                     colour=ROOT.kRed,
                                     filepath=ds_signal,
                                     sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "MonoSbb_dsid600033_zp625_dm100_hs70.root")
monoSbb_zp625_dm100_hs70 = DSconfig(name="monoSbb_zp625_dm100_hs70",
                                    label="(m_{Z'},m_{hs})=(625,70)",
                                    colour=ROOT.kBlue,
                                    filepath=ds_signal,
                                    sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "MonoSbb_dsid600034_zp1700_dm100_hs70.root")
monoSbb_zp1700_dm100_hs70 = DSconfig(name="monoSbb_zp1700_dm100_hs70",
                                     label="(m_{Z'},m_{hs})=(1700,70)",
                                     colour=ROOT.kOrange,
                                     filepath=ds_signal,
                                     sampletype=SampleTypes.Signal)

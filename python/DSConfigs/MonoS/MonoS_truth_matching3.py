#!/usr/bin/env python
import os
from XAMPPplotting.Defs import *
ROOT.gStyle.SetOptStat(0)
BasePath = '/nfs/dust/atlas/user/pgadow/monoS/plotting/truth/'

# Specify samples
# Signal MC
ds_signal = os.path.join(BasePath, "monoSWW_CKKWL_01jet_zp1000_dm300_hs260.root")
monoSWW_CKKWL_01jet_zp1000_dm300_hs260 = DSconfig(name="monoSWW_CKKWL_01jet_zp1000_dm300_hs260",
                                                  label="CKKW: (m_{Z'},m_{hs},m_{DM})=(1000,260,300)",
                                                  colour=ROOT.kRed,
                                                  filepath=ds_signal,
                                                  sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "monoSWW_MLM_01jet_zp1000_dm300_hs260.root")
monoSWW_MLM_01jet_zp1000_dm300_hs260 = DSconfig(name="monoSWW_MLM_01jet_zp1000_dm300_hs260",
                                                label="MLM: (m_{Z'},m_{hs},m_{DM})=(1000,260,300)",
                                                colour=ROOT.kViolet,
                                                filepath=ds_signal,
                                                sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "monoSWW_zp1000_dm300_hs260.root")
monoSWW_zp1000_dm300_hs260 = DSconfig(name="monoSWW_zp1000_dm300_hs260",
                                      label="simple: (m_{Z'},m_{hs},m_{DM})=(1000,260,300)",
                                      colour=ROOT.kRed - 7,
                                      filepath=ds_signal,
                                      sampletype=SampleTypes.Signal)

#! /usr/bin/env python
from XAMPPplotting import TauCommon as TC
from XAMPPplotting.Defs import *


def check(method):
    if not method:
        print "Unable to load ", method
        exit(1)


#via JOs - To Do

lumi = 138.963023  #fb^-1, calculated on v22, June 21

date = "2019-07-12"

folder = "OPTCBA_v3_2019_07_12_16_38_45"

years = ["data15", "data16", "data17", "data18"]

data_conf_path = "XAMPPplotting/InputConf/LepHadStau/Data/"

main_root_files_path = "/ptmp/mpp/zenon/Cluster/OUTPUT/%s/%s/" % (date, folder)

fake_root_files_path = ""
#"/ptmp/mpp/zenon/Cluster/Output/%s/LH%sFakes/"%(date, years_tag)

include_signal = True

signal_point = "200_1"

#load samples
if not fake_root_files_path:

    tc = TC.TauCommon.ctr_common_path("stau lephad",
                                      path=main_root_files_path,
                                      lumi=lumi,
                                      include_fakes=False,
                                      include_signal=include_signal,
                                      signal_point=signal_point)
else:
    tc = TC.TauCommon("stau lephad",
                      main_path=main_root_files_path,
                      aux_path=fake_root_files_path,
                      lumi=lumi,
                      include_fakes=True,
                      include_signal=include_signal,
                      signal_point=signal_point)

check(tc.load_samples())

#unfortunately this won't work in a for-loop
q0 = tc.get_ds_configs()[0]  # data
q1 = tc.get_ds_configs()[1]  # multiV
q2 = tc.get_ds_configs()[2]  # higgs
q3 = tc.get_ds_configs()[3]  # top
q4 = tc.get_ds_configs()[4]  # Zll
q5 = tc.get_ds_configs()[5]  # Ztt
q6 = tc.get_ds_configs()[6]  # W
q7 = tc.get_ds_configs()[7]  # signal

#q8 = tc.get_ds_configs()[8] # fakes


def main(argv):
    print "Done!"


##
# @brief execute main
##
if __name__ == "__main__":
    main(sys.argv[1:])

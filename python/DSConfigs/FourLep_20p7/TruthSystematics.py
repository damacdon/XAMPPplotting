#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.FileStructureHandler import GetFileHandler, GetSystPairer

#GetSystPairer().create_group("Generator", "Generator")
#GetSystPairer().find_envelope("Generator").append_systematic("VV")
#GetSystPairer().find_envelope("Generator").append_systematic("ttZ")
#GetSystPairer().exclude_systematic("Generator")
#GetSystPairer().set_systematic_title("ScaleUp", "scale")
#GetSystPairer().set_systematic_title("ScaleDn", "scale")
#GetSystPairer().pair_systematics("ScaleDn", "ScaleUp")

#GetSystPairer().create_deviation("PDFset=261", "PDF")
#GetSystPairer().create_envelope("otherPDF", "PDF (alt.)")
#GetSystPairer().find_envelope("otherPDF").append_systematic("CT14nlo")
#GetSystPairer().find_envelope("otherPDF").append_systematic("MMHT")
#GetSystPairer().find_envelope("otherPDF").append_systematic("CT14NNLO")
#GetSystPairer().find_envelope("otherPDF").append_systematic("MMHT2014nlo68")

#GetSystPairer().pair_systematics("QSF025", "QSF4")
#GetSystPairer().pair_systematics("CKKW15", "CKKW30")
#GetSystPairer().pair_systematics("A14VarDn", "A14VarUp")

#GetSystPairer().set_systematic_title("RENORM", "#mu_{R}")
#GetSystPairer().set_systematic_title("FACTOR", "#mu_{F}")
#GetSystPairer().set_systematic_title("ALPHAS", "#alpha_{s}")
#GetSystPairer().set_systematic_title("RENORM_AND-FACTOR", "#mu_{R}#wedge#mu_{F}")
#GetSystPairer().set_systematic_title("RENORM_1UP-FACTOR", "#mu_{R}#uparrow#mu_{F}")
#GetSystPairer().set_systematic_title("RENORM_1DOWN-FACTOR", "#mu_{R}#downarrow#mu_{F}")
#GetSystPairer().set_systematic_title("A14VarDn", "A14 tune")
#GetSystPairer().set_systematic_title("A14VarUp", "A14 tune")
#GetSystPairer().set_systematic_title("QSF025", "#mu_{Q}")
#GetSystPairer().set_systematic_title("QSF4", "#mu_{Q}")

Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-05/FourLep_36ifb/"
## Command to create the combined files
## python XAMPPmultilep/macros/combineTruthUncertainties.py --in_file_dir /ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-05/FourLep_36ifb/ --out_file_dir /ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-05/FourLep_36ifb_combined/ --nominal_ttz_sample MG5_ttZ --alternative_ttz_sample SherpaLO_ttZ

Path1 = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-05/FourLep_36ifb_combined/"

#GetSystPairer().create_envelope("PDFset=260")
## python XAMPPplotting/python/MCPlots.py  -c XAMPPplotting/python/DSConfigs/FourLepton_Truth_ttZ.py --RatioSmp Nominal --noStack --regions 4L

GetSystPairer().pair_systematics("MadGraph_ttZScale_Down", "MadGraph_ttZScale_Up")

GetSystPairer().set_systematic_title("MadGraph_ttZScale_Up", "#mu_{F}#wedge#mu_{R}")
GetSystPairer().set_systematic_title("MadGraph_ttZScale_Down", "#mu_{F}#wedge#mu_{R}")

GetSystPairer().create_group("Generator", "Generator")
GetSystPairer().find_envelope("Generator").append_systematic("SherpaTTZ")
GetSystPairer().find_envelope("Generator").append_systematic("Sherpa221_ZZ")
GetSystPairer().find_envelope("Generator").append_systematic("PowHegZZ")
GetSystPairer().find_envelope("Generator").append_systematic("NNNLO")

GetSystPairer().exclude_systematic("NNNLO")
GetSystPairer().exclude_systematic("NTrials")
GetSystPairer().exclude_systematic("MEWeight")
GetSystPairer().exclude_systematic("WeightNormalisation")
GetSystPairer().exclude_systematic("RenNom_Fac__Nom")
#GetSystPairer().exclude_systematic("PowHegZZ")
GetSystPairer().exclude_systematic("Sherpa221_ZZ")

GetSystPairer().create_deviation("MUR1_MUF1_PDF261", "PDF")
GetSystPairer().pair_systematics("FacNom_Ren__1down", "FacNom_Ren__1up")
GetSystPairer().pair_systematics("RenNom_Fac__1down", "RenNom_Fac__1up")
GetSystPairer().pair_systematics("Scales__1down", "Scales__1up")
GetSystPairer().set_systematic_title("FacNom_Ren", "#mu_{R}")
GetSystPairer().set_systematic_title("RenNom_Fac", "#mu_{F}")
GetSystPairer().set_systematic_title("Scales", "#mu_{F}#wedge#mu_{R}")

GetSystPairer().create_envelope("AltPDF", "PDF (alt.)")
GetSystPairer().find_envelope("AltPDF").append_systematic("MUR1_MUF1_PDF13000")
GetSystPairer().find_envelope("AltPDF").append_systematic("MUR1_MUF1_PDF25300")
GetSystPairer().find_envelope("AltPDF").append_systematic("MUR1_MUF1_PDF270000")
GetSystPairer().find_envelope("AltPDF").append_systematic("MUR1_MUF1_PDF269000")

Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2017-03-13/"

# Scale = DSconfig(colour=ROOT.kBlack,name="Scales", label="Scale",filepath=Path+"/ttZ_Scales.root",sampletype=SampleTypes.Irreducible)
# Generator = DSconfig(colour=ROOT.kBlack,name="Generator", label="Generator",filepath=Path+"/ttZ_Generator.root",sampletype=SampleTypes.Irreducible)

ttV = DSconfig(colour=ROOT.kBlack,
               label="t#bar{t}Z",
               name="ttV",
               filepath=[Path + "/ttZ_Variations/ttZ.root"],
               fillstyle=0,
               ExcludeSyst=["NNNLO"],
               sampletype=SampleTypes.Irreducible)

ZZ = DSconfig(colour=ROOT.kBlack,
              label="ZZ",
              name="ZZ",
              filepath=[Path + "/ZZ_Variations/ZZ.root"],
              fillstyle=0,
              sampletype=SampleTypes.Irreducible)

#Sherpa222_ZZ = DSconfig(
#    colour=ROOT.kBlack,
#    #   ROOT.TColor.GetColor(111, 224, 5),
#    label="VV",
#    name="Sherpa222_ZZ",
#    filepath=Path1 + "Sherpa222_VV.root",
#    fillstyle=0,
#    markerstyle=0,
#    markersize=0,
#    sampletype=SampleTypes.Irreducible,
#    LegendOrder=-1)

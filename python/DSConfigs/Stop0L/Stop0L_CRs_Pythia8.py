#! /usr/bin/env python
from XAMPPplotting.Defs import *

# specify things like 'lumi' for data, 'colour', an unique 'name' and a 'label' for the dataset to be printed in the plots

# if needed, specify an unique name of the dataset config which can be drawn in comparisons of different dataset configs
name = "BG using t#bar{t} Pythia8"

# Data
Data = DSconfig(lumi=3.20905,
                colour=ROOT.kBlack,
                label="Data",
                filepath="/ptmp/mpp/niko/Cluster/Output/2016-05-19/Stop0L_ttbarCR_v01/Data15_All_Input.root",
                sampletype=SampleTypes.Data)

# Background MC
ttV = DSconfig(colour=ROOT.kRed + 3,
               name="ttV",
               label="t#bar{t}+V",
               filepath="/ptmp/mpp/niko/Cluster/Output/2016-05-19/Stop0L_ttbarCR_v01/ttV_Input.root",
               sampletype=SampleTypes.Irreducible)
Diboson = DSconfig(colour=ROOT.kYellow,
                   name="Diboson",
                   label="Diboson",
                   filepath="/ptmp/mpp/niko/Cluster/Output/2016-05-19/Stop0L_ttbarCR_v01/Diboson_Input.root",
                   sampletype=SampleTypes.Reducible)
SingleTop = DSconfig(colour=ROOT.kGreen + 3,
                     name="SingleTop",
                     label="Single Top",
                     filepath="/ptmp/mpp/niko/Cluster/Output/2016-05-19/Stop0L_ttbarCR_v01/singleTop_Input.root",
                     sampletype=SampleTypes.Reducible)
ttbarPowheg8 = DSconfig(colour=ROOT.kGreen - 9,
                        name="ttbarPowheg8",
                        label="t#bar{t} Powheg 8",
                        filepath="/ptmp/mpp/niko/Cluster/Output/2016-05-19/Stop0L_ttbarCR_v01/ttbarPowheg8_Input.root",
                        sampletype=SampleTypes.Reducible)
Wjets = DSconfig(colour=ROOT.kAzure + 1,
                 name="Wjets",
                 label="W+jets",
                 filepath="/ptmp/mpp/niko/Cluster/Output/2016-05-19/Stop0L_ttbarCR_v01/Wjets_Input.root",
                 sampletype=SampleTypes.Reducible)
Zjets = DSconfig(colour=ROOT.kBlue + 3,
                 name="Zjets",
                 label="Z+jets",
                 filepath="/ptmp/mpp/niko/Cluster/Output/2016-05-19/Stop0L_ttbarCR_v01/Zjets_Input.root",
                 sampletype=SampleTypes.Reducible)

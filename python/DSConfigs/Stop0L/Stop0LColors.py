from XAMPPplotting.Defs import *

sigColors = {
    '600_300': ROOT.kMagenta,
    'GG_1700_400': ROOT.kMagenta,
    '1000_1': ROOT.kOrange + 6,
    '800_1': ROOT.kMagenta,
    '700_400': ROOT.kOrange + 6,
    '300_127': ROOT.kMagenta,
    '400_212': ROOT.kOrange + 6,
    '350_177': ROOT.kMagenta,
    '450_277': ROOT.kMagenta + 3,
    '500_327': ROOT.kCyan - 3,
    '700_100_50': ROOT.kMagenta + 3,
    '700_100_50_mixed': ROOT.kMagenta,
    '400_100_50': ROOT.kMagenta + 3,
    "450_100_50_mixed": ROOT.kMagenta,
}
# from Stop0L paper 2017
# bkgColors = {
#         'Z':ROOT.kYellow,
#         'W':ROOT.kAzure+1,
#         'ttbar':ROOT.kGreen-9,
#         'ttV':ROOT.kRed+3,
#         'ttGamma':909,
#         'singleTop':ROOT.kGreen+3,
#         'dibosons':603,
#         'QCD':0,
#         'VGamma':ROOT.kCyan,
#         'ttbar_Herwigpp':ROOT.kOrange+7,
#         'ttbar_radLo':ROOT.kAzure+6,
#         'ttbar_radHi':ROOT.kBlue-6,
#         'ttbar_Sherpa':ROOT.kOrange,
#         'singleTop_Herwigpp':ROOT.kGreen+6,
#         'singleTop_radLo':ROOT.kGreen+7,
#         'singleTop_radHi':ROOT.kGreen+8,
#         'singleTop_DS':ROOT.kGreen+9,
#         }
# from DMHF paper 2017
bkgColors = {
    'Z': ROOT.kYellow - 7,
    'W': ROOT.kGreen + 2,
    'ttbar': ROOT.kRed - 7,
    'ttV': ROOT.kMagenta + 1,
    'ttGamma': 909,
    'singleTop': ROOT.kAzure + 1,
    'dibosons': ROOT.kAzure - 5,
    'QCD': 0,
    'VGamma': ROOT.kAzure - 5,
    'ttbar_Herwigpp': ROOT.kOrange + 7,
    'ttbar_radLo': ROOT.kAzure + 6,
    'ttbar_radHi': ROOT.kBlue - 6,
    'ttbar_Sherpa': ROOT.kOrange,
    'singleTop_Herwigpp': ROOT.kGreen + 6,
    'singleTop_radLo': ROOT.kGreen + 7,
    'singleTop_radHi': ROOT.kGreen + 8,
    'singleTop_DS': ROOT.kGreen + 9,
}


def setupPalette():
    Red = array('d', [0.9764, 0.9956, 0.8186, 0.5301, 0.1802, 0.0232, 0.0780, 0.0592, 0.2082])
    Green = array('d', [0.9832, 0.7862, 0.7328, 0.7492, 0.7178, 0.6419, 0.5041, 0.3599, 0.1664])
    Blue = array('d', [0.0539, 0.1968, 0.3499, 0.4662, 0.6425, 0.7914, 0.8385, 0.8684, 0.5293])
    Length = array('d', [0.0000, 0.1250, 0.2500, 0.3750, 0.5000, 0.6250, 0.7500, 0.8750, 1.0000])
    ROOT.TColor.CreateGradientColorTable(9, Length, Red, Green, Blue, 1500)

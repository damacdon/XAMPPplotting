#! /usr/bin/env python
from XAMPPplotting.Defs import *

BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-15/stop_jetMulti/'  # for jet multiplicity
# BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-06-20/stop_signalJetPts/' # for jet pt requirments

SMs = [
    "Zjets_Sherpa221_Input.root", "Wjets_Sherpa221_Input.root", "ttbar_Input.root", "singleTop_Input.root", "Diboson_Input.root",
    "ttV_Input.root"
]
AllSM = DSconfig(colour=ROOT.kBlack,
                 name="AllSM",
                 label="SM",
                 filepath=[BasePath + S for S in SMs],
                 sampletype=SampleTypes.Irreducible,
                 markerstyle=5,
                 linestyle=1,
                 fillstyle=0)

# Signal MC
# TT_directTT_800_1 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_800_1",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(800,1) GeV",filepath=BasePath+"TT_directTT_800_1_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
TT_directTT_1000_1 = DSconfig(colour=ROOT.kRed,
                              name="TT_directTT_1000_1",
                              label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(1000,1) GeV",
                              filepath=BasePath + "TT_directTT_1000_1_a821_r7676_Input.root",
                              sampletype=SampleTypes.Signal,
                              markerstyle=ROOT.kOpenCircle)
TT_directTT_600_300 = DSconfig(colour=ROOT.kGreen,
                               name="TT_directTT_600_300",
                               label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,300) GeV",
                               filepath=BasePath + "TT_directTT_600_300_a821_r7676_Input.root",
                               sampletype=SampleTypes.Signal,
                               markerstyle=ROOT.kOpenTriangleUp)
TT_directTT_400_212 = DSconfig(colour=ROOT.kBlue,
                               name="TT_directTT_400_212",
                               label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(400,212) GeV",
                               filepath=BasePath + "TT_directTT_400_212_a821_r7676_Input.root",
                               sampletype=SampleTypes.Signal,
                               markerstyle=ROOT.kOpenTriangleDown)

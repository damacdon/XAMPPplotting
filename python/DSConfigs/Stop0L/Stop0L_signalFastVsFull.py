#! /usr/bin/env python
from XAMPPplotting.Defs import *

BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-04-25/stop_signalFullFast_v2/'  # for jet multiplicity

# put this as regionLabel: (m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(800,100) GeV

# Signal MC
TT_directTT_800_100_full = DSconfig(colour=ROOT.kRed,
                                    name="TT_directTT_800_100_full",
                                    label="ATLAS full simulation (full-sim.)",
                                    filepath=BasePath + "TT_directTT_800_100_Input.root",
                                    sampletype=SampleTypes.Irreducible)
TT_directTT_800_100 = DSconfig(colour=ROOT.kBlue,
                               name="TT_directTT_800_1",
                               label="ATLAS fast simulation (AF2)",
                               filepath=BasePath + "TT_directTT_800_100_a821_r7676_Input.root",
                               sampletype=SampleTypes.Signal)

# TT_directTT_800_200_full = DSconfig(colour=ROOT.kRed,name="TT_directTT_800_200_full",label="ATLAS full simulation (full-sim.)",filepath=BasePath+"TT_directTT_800_200_Input.root",sampletype=SampleTypes.Irreducible)
# TT_directTT_800_200 = DSconfig(colour=ROOT.kBlue,name="TT_directTT_800_200",label="ATLAS fast simulation (AF2)",filepath=BasePath+"TT_directTT_800_200_a821_r7676_Input.root",sampletype=SampleTypes.Signal)

#! /usr/bin/env python
from XAMPPplotting.Defs import *

BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-17/dmhf_jetMult/'  # for jet multiplicity
# BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-17/dmhf_jetPts/' # for jet pt requirments

SMs = [
    "Zjets_Sherpa221_Input.root", "Wjets_Sherpa221_Input.root", "ttbar_Input.root", "singleTop_Input.root", "Diboson_Input.root",
    "ttV_Input.root"
]
AllSM = DSconfig(colour=ROOT.kBlack,
                 name="AllSM",
                 label="SM",
                 filepath=[BasePath + S for S in SMs],
                 sampletype=SampleTypes.Irreducible,
                 markerstyle=5,
                 linestyle=1,
                 fillstyle=0)

# Signal MC
DE_tt_c1_M400 = DSconfig(colour=ROOT.kRed,
                         name="DE_tt_c1_M400",
                         label="(m_{M},m_{#varphi})=(400,0.1) GeV (c_{1}=1)",
                         filepath=BasePath + "DE_tt_c1_M400_Input.root",
                         sampletype=SampleTypes.Signal,
                         markerstyle=ROOT.kOpenCircle)
DE_tt_c2_M600 = DSconfig(colour=ROOT.kGreen,
                         name="DE_tt_c2_M600",
                         label="(m_{M},m_{#varphi})=(600,0.1) GeV (c_{2}=1)",
                         filepath=BasePath + "DE_tt_c2_M600_Input.root",
                         sampletype=SampleTypes.Signal,
                         markerstyle=ROOT.kOpenTriangleUp)

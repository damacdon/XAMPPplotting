import sys, logging, ROOT, math
from ClusterSubmission.Utils import convertStdVector
from XAMPPplotting.Defs import SampleTypes, DSconfig
from XAMPPplotting.FileStructureHandler import *


def GetOrderedSampleList(Samples):
    CustomLegendOrder = True

    for H in Samples:
        if math.isnan(H.GetLegendOrder()) or H.GetLegendOrder() < 0:
            CustomLegendOrder = False
        if math.isnan(H.Integral()):
            logging.warning("Sample %s returns not a number for variable %s in region %s. Thus it is discarded" %
                            (H.GetName(), H.GetVariableName(), H.GetRegion()))
    if CustomLegendOrder:
        return sorted([H for H in Samples if H.isValid()], key=lambda H: H.GetLegendOrder())
    return sorted([H for H in Samples if H.isValid()], key=lambda H: H.Integral())


class HistoSet(object):
    def __init__(self, Analysis="", Variable="", Region="", UseData=True, ConfigSet=None, BreakDownBkg=True):
        self.__name = ConfigSet.GetName()
        self.__analysis = Analysis
        self.__var = Variable
        self.__region = Region
        self.__UseData = UseData
        self.__lumi = 1. if not UseData else 0.
        self.__SumBG = None
        self.__Samples = []
        self.__Signals = []
        self.__SigPlusSumBG = []
        self.__Backgrounds = []
        self.__Data = []
        self.__has_SmpSF = False

        self.__LoadSamples(ConfigSet.DSConfigs(), BreakDownBkg=BreakDownBkg)

    def __LoadSamples(self, SamplesToUse, BreakDownBkg=True):
        #Create a sample histo for all the SamplesToUse in the list
        SummedFiles = []
        for Smp in SamplesToUse:
            #Do not add samples with the same name twice.
            #If we do not use data skip also those files from the histo set
            if self.GetSample(Smp.Name): continue
            ### Skip to load the data samples
            if not self.__UseData and Smp.SampleType == SampleTypes.Data: continue

            if Smp.SampleType == SampleTypes.Reducible or Smp.SampleType == SampleTypes.Irreducible:
                SummedFiles += Smp.Filepaths
                if not BreakDownBkg: continue
            self.__Samples += [self.__LoadHisto(sample=Smp)]
        #Now we have the DSConfig loaded let"s sort it
        for Smp in self.__Samples:
            if Smp.isSignal():
                self.__Signals.append(Smp)
            if Smp.isBackground():
                self.__Backgrounds.append(Smp)
            if self.__UseData and Smp.isData():
                self.__Data.append(Smp)

        self.__Signals = GetOrderedSampleList(self.__Signals)
        self.__Backgrounds = GetOrderedSampleList(self.__Backgrounds)
        self.__Data = GetOrderedSampleList(self.__Data)

        SummedConfig = DSconfig(name="SumBG", label="Total MC", filepath=SummedFiles)
        self.__SumBG = self.__LoadHisto(SummedConfig)
        self.__SetLumi()
        if self.__has_SmpSF:
            self.__SumBG.Reset()
            for b in self.__Backgrounds:
                self.__SumBG.Add(b)

    def __SetLumi(self):
        for Smp in self.__Samples:
            Smp.SetLumi(self.__lumi)
        if self.__SumBG:
            self.__SumBG.SetLumi(self.__lumi)

    def __LoadHisto(self, sample):
        if len(sample.Filepaths) == 0:
            logging.error("The sample %s does not have any associated files" % (sample.Name))
            return None

        if sample.SampleType != SampleTypes.Data or self.GetAnalysis() in GetFileHandler().GetFileStructure(
                sample.Filepaths[0]).get_analyses_names():
            Histo = ROOT.XAMPP.SampleHisto(sample.Name, self.GetVariable(), self.GetAnalysis(), self.GetRegion(),
                                           convertStdVector(sample.Filepaths))
        else:
            ### open the first file of the histogram
            ana_name_touse = self.GetAnalysis()
            try:
                ana_name_touse = [
                    x for x in GetFileHandler().GetFileStructure(sample.Filepaths[0]).get_analyses_names()
                    if x.startwith(self.GetAnalysis())
                ][0]
            except:
                ana_name_touse = GetFileHandler().GetFileStructure(sample.Filepaths[0]).get_analyses_names()[0]
            Histo = ROOT.XAMPP.SampleHisto(sample.Name, self.GetVariable(), ana_name_touse, self.GetRegion(),
                                           convertStdVector(sample.Filepaths))

        # Set the type of the histogram
        if sample.SampleType == SampleTypes.Signal:
            Histo.setSampleType(ROOT.XAMPP.SampleHisto.Signal)
        elif sample.SampleType == SampleTypes.Irreducible:
            Histo.setSampleType(ROOT.XAMPP.SampleHisto.Irreducible)
        elif sample.SampleType == SampleTypes.Reducible:
            Histo.setSampleType(ROOT.XAMPP.SampleHisto.Reducible)
        elif sample.SampleType == SampleTypes.Data:
            Histo.setSampleType(ROOT.XAMPP.SampleHisto.Data)

        Histo.exclude_systematics(convertStdVector(sample.ExcludeSyst))
        Histo.set_systematics(convertStdVector(sample.SystToUse))
        if sample.SetNominal: Histo.set_nominal(sample.NominalSyst)

        Histo.setTheoryUncertainty(sample.TheoUncert)

        ##Find the sample with the highest reported luminosity
        if Histo.isData() and self.__lumi < sample.Lumi:
            self.__lumi = sample.Lumi
        elif Histo.isSignal():
            Histo.SetLineStyle(ROOT.kDashed)

        if Histo.Integral() < 0.:
            logging.warning("%s has a negative integral=%f" % (Histo.GetName(), Histo.Integral()))

        Histo.SetTitle(sample.Label)

        Histo.SetLineColor(sample.LineColour)
        # for now, do not set the FillColor for signal since it will not draw the signal as a dashed line only
        if not sample.SampleType == SampleTypes.Signal:
            Histo.SetFillColor(sample.FillColour)
        Histo.SetMarkerColor(sample.MarkerColour)
        Histo.SetFillColorAlpha(sample.FillColourAlpha)
        Histo.SetLineStyle(sample.LineStyle)
        Histo.SetFillStyle(sample.FillStyle)
        Histo.SetMarkerStyle(sample.MarkerStyle)
        Histo.SetMarkerSize(sample.MarkerSize)
        Histo.Scale(sample.ScaleFactor)
        self.__has_SmpSF = self.__has_SmpSF or sample.ScaleFactor != 1.

        if Histo.Truncate():
            self.__has_SmpSF = True
        Histo.setLegendOrder(sample.LegendOrder)
        if len(sample.DrawStyle) > 0: Histo.setDrawStyle(sample.DrawStyle)
        return Histo

    def SetLumi(self, L):
        if self.__UseData: return
        self.__lumi = L
        self.__SetLumi()

    def GetLumi(self):
        return self.__lumi

    def GetName(self):
        return self.__name

    def GetVariable(self):
        return self.__var

    def GetRegion(self):
        return self.__region

    def GetAnalysis(self):
        return self.__analysis

    def GetSample(self, sampleName, IfFailRetBg=False):
        for Smp in self.__Samples:
            if Smp.GetName() == sampleName:
                return Smp
        if IfFailRetBg:
            return self.GetSummedBackground()
        return None

    def GetBackgrounds(self):
        return self.__Backgrounds

    def GetIrreducible(self):
        Irred = []
        for Bkg in self.GetBackgrounds():
            if Bkg.isIrreducible():
                Irred.append(Bkg)
        return Irred

    def GetReducible(self):
        Red = []
        for Bkg in self.GetBackgrounds():
            if Bkg.isReducible():
                Red.append(Bkg)
        return Red

    def GetSignals(self, StackOnSumBg=False):
        if not StackOnSumBg or self.GetSummedBackground() is None or not self.GetSummedBackground().isValid():
            return self.__Signals
        if len(self.__SigPlusSumBG) == 0:
            Stacked = [ROOT.XAMPP.SampleHisto("Stacked_%s" % (Sig.GetName()), Sig, self.GetSummedBackground()) for Sig in self.__Signals]
            self.__SigPlusSumBG = [S for S in Stacked if S.loadIntoMemory()]
            for i, H in enumerate(self.__SigPlusSumBG):
                H.SetMarkerColor(self.__Signals[i].GetHistogram().GetMarkerColor())
                H.SetMarkerStyle(self.__Signals[i].GetHistogram().GetMarkerStyle())
                H.SetMarkerSize(self.__Signals[i].GetHistogram().GetMarkerSize())
                H.SetFillColor(self.__Signals[i].GetHistogram().GetFillColor())
                H.SetFillStyle(self.__Signals[i].GetHistogram().GetFillStyle())
                H.SetLineColor(self.__Signals[i].GetHistogram().GetLineColor())
                H.SetLineStyle(self.__Signals[i].GetHistogram().GetLineStyle())
                H.SetLumi(self.GetSummedBackground().GetLumi())
        return self.__SigPlusSumBG

    def GetData(self):
        return self.__Data

    def GetSummedBackground(self):
        return self.__SumBG

    def CheckHistoSet(self):
        if self.GetSummedBackground() is not None and self.GetSummedBackground().isValid(): return True
        else:
            logging.warning("No summed background histogram found. Please be aware that only MC plots will work if you run with --RatioSmp")
        for Sig in self.GetSignals():
            if Sig.isValid():
                return True
        return False

    def GetSampleNames(self, AppendData=True, AppendBackground=True, AppendSignal=True):
        Names = []
        for Smp in self.__Samples:
            if not AppendData and Smp.isData():
                continue
            if not AppendBackground and Smp.isBackground():
                continue
            if not AppendSignal and Smp.isSignal():
                continue

            Names.append(Smp.GetName())
        return Names

    def isTH1(self):
        if self.GetSummedBackground() is not None and self.GetSummedBackground().isValid():
            return self.GetSummedBackground().isTH1()
        for Sig in self.GetSignals():
            if Sig.isValid():
                return Sig.isTH1()
        return False

    def isTH2(self):
        if self.GetSummedBackground() is not None and self.GetSummedBackground().isValid():
            return self.GetSummedBackground().isTH2()
        for Sig in self.GetSignals():
            if Sig.isValid():
                return Sig.isTH2()
        return False

    def isTH3(self):
        if self.GetSummedBackground() is not None and self.GetSummedBackground().isValid():
            return self.GetSummedBackground().isTH3()
        for Sig in self.GetSignals():
            if Sig.isValid():
                return Sig.isTH3()
        return False

    def Dimension(self):
        return self.GetSummedBackground().GetDimension()


def CreateHistoSets(Options, analysis, region, var, UseData=True, skip_consistency=False, break_down_bkg=True):
    FullHistoSetList = []
    while GetStructure(Options).Next():

        CurrentHistoSet = HistoSet(Analysis=analysis,
                                   Variable=var,
                                   Region=region,
                                   UseData=UseData,
                                   ConfigSet=GetStructure().GetConfigSet(),
                                   BreakDownBkg=break_down_bkg)
        CurrentHistoSet.SetLumi(Options.lumi)
        if skip_consistency or CurrentHistoSet.CheckHistoSet():
            FullHistoSetList.append(CurrentHistoSet)
    if len(FullHistoSetList) == 0:
        return None
    return FullHistoSetList

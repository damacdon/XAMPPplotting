#if ATHENA_RELEASE_SERIES == 212
#include <FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h>
#include <PMGAnalysisInterfaces/IPMGCrossSectionTool.h>
#include <PathResolver/PathResolver.h>
#include <XAMPPplotting/BTaggingScaleFactor.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/ReaderProvider.h>
#include <XAMPPplotting/UtilityReader.h>
namespace XAMPP {

    //##############################################################################################################
    //                                        B-Tagging Scale Factor
    //##############################################################################################################
    BTaggingSF::BTaggingSF(const std::string& Name) :
        m_Name(Name),
        m_JetAuthor("AntiKt4EMPFlowJets"),
        m_Tagger("MV2c10"),
        m_WP("FixedCutBEff_77"),
        m_CalibrationRelease("xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2019-07-30_v1.root"),
        m_JetCollection(),
        m_xSecDir("dev/PMGTools/2019-11-28/PMGxsecDB_mc16.txt"),
        m_MetaData(NormalizationDataBase::getDataBase()),
        m_Jet(nullptr),
        m_JetPt(nullptr),
        m_JetEta(nullptr),
        m_JetTagWeight(nullptr),
        m_JetLabelID(nullptr),
        m_JetIsBTagged(nullptr),
        m_JetIsAssociated(nullptr),
        m_bTaggingTool("BTaggingEfficiencyTool/BTagTool"),
        m_Syst(),
        m_SystNames(),
        m_bTagSFReaders(),
        m_bTagSFReaders_Itr(m_bTagSFReaders.end()) {
        Weight::getWeighter()->RegisterWeightElement(this);
    }
    bool BTaggingSF::configureWeight(std::ifstream& inf) {
        std::string line;
        IfDefLineParser ReadLine;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "End_BTaggingWeight")) {
                if (!InitBTaggingTool()) return false;
                if (!InitJets()) return false;
                // Reset the strings since they're no longer needed
                m_Tagger = m_WP = m_JetAuthor = std::string();
                return true;
            } else if (IsKeyWordSatisfied(sstr, "Tagger"))
                m_Tagger = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "CalibRelease"))
                m_CalibrationRelease = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "JetAuthor"))
                m_JetAuthor = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "JetCollection"))
                m_JetCollection = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "OperatingPoint"))
                m_WP = GetWordFromStream(sstr);
        }
        Error("BTaggingSF::configureWeight", "Missing 'End_BTaggingWeight  statement");

        return false;
    }
    double BTaggingSF::GetTotalBTagSF() const {
        double JetWeightBTag_New = 1.;
        int n_associated = 0;
        // TODO: Check first whether we are using track jets or calo jets as the SF is calculated differently for these two
        // jet collections. Leaving it for now as it is because we are not intending to re-calculate the b-tagging SFs in
        // the resolved region.
        if (m_JetCollection == "TrackJetReader") {
            if (m_Jet->Size() > 0) {
                for (size_t j = 0; j < m_Jet->Size(); ++j) {
                    double scaleFactorSingleJet =
                        CalculateBTagSF(m_JetPt->readEntry(j), m_JetEta->readEntry(j), m_JetTagWeight->readEntry(j),
                                        m_JetLabelID->readEntry(j), m_JetIsBTagged->readEntry(j));
                    if (m_JetIsAssociated->readEntry(j)) ++n_associated;
                    // Use only the first two associated track jets for b-tagging. If there are more associated jets, they are ignored.
                    if (n_associated > 2 && m_JetIsAssociated->readEntry(j)) continue;
                    JetWeightBTag_New *= scaleFactorSingleJet;
                }
            }
        } else {
            if (m_Jet->Size() > 0) {
                for (size_t j = 0; j < m_Jet->Size(); ++j) {
                    double scaleFactorSingleJet =
                        CalculateBTagSF(m_JetPt->readEntry(j), m_JetEta->readEntry(j), m_JetTagWeight->readEntry(j),
                                        m_JetLabelID->readEntry(j), m_JetIsBTagged->readEntry(j));
                    JetWeightBTag_New *= scaleFactorSingleJet;
                }
            }
        }
        return JetWeightBTag_New;
    }
    bool BTaggingSF::InitBTaggingTool() {
        m_bTaggingTool.setTypeAndName("BTaggingEfficiencyTool/b_TaggingTool_" + name());
        // Choose properties for bTaggingTool from here:
        // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/Root/BTaggingEfficiencyTool.cxx#L119

        std::string MC_index = "410470";

        std::string sample_name;

        asg::AnaToolHandle<PMGTools::IPMGCrossSectionTool> pmg_DB("PMGTools::PMGCrossSectionTool/BkgXsecTool");
        pmg_DB.retrieve().ignore();
        pmg_DB->readInfosFromFiles(GetPathResolvedFileList(std::vector<std::string>{m_xSecDir}));

        for (const auto& dsid : m_MetaData->GetListOfMCSamples()) {
            sample_name = pmg_DB->getSampleName(dsid);
            if ((sample_name.find("Sherpa_222") != std::string::npos) || (sample_name.find("Sherpa_221") != std::string::npos) ||
                (sample_name.find("Sh_221") != std::string::npos)) {
                MC_index = "410250";
            } else if ((sample_name.find("Pythia8") != std::string::npos) || (sample_name.find("PhPy8") != std::string::npos)) {
                MC_index = "410470";
            } else if ((sample_name.find("Herwig") != std::string::npos)) {  // VF: not validated as we don't use Herwig samples
                MC_index = "410558";
            }
        }

        if (!m_bTaggingTool.setProperty("TaggerName", m_Tagger).isSuccess())
            return false;
        else if (!m_bTaggingTool.setProperty("OperatingPoint", m_WP).isSuccess())
            return false;
        else if (!m_bTaggingTool.setProperty("JetAuthor", m_JetAuthor).isSuccess())
            return false;
        else if (!m_bTaggingTool.setProperty("ScaleFactorFileName", m_CalibrationRelease).isSuccess())
            return false;
        // Actually I've no clue why this is here or about their actual meaning....
        else if (!m_bTaggingTool.setProperty("EigenvectorReductionB", "Medium").isSuccess())
            return false;
        else if (!m_bTaggingTool.setProperty("EigenvectorReductionC", "Medium").isSuccess())
            return false;
        else if (!m_bTaggingTool.setProperty("EigenvectorReductionLight", "Medium").isSuccess())
            return false;
        else if (!m_bTaggingTool.setProperty("EfficiencyBCalibrations", MC_index).isSuccess())
            return false;
        else if (!m_bTaggingTool.setProperty("EfficiencyCCalibrations", MC_index).isSuccess())
            return false;
        else if (!m_bTaggingTool.setProperty("EfficiencyTCalibrations", MC_index).isSuccess())
            return false;
        else if (!m_bTaggingTool.setProperty("EfficiencyLightCalibrations", MC_index).isSuccess())
            return false;
        else if (!m_bTaggingTool.initialize().isSuccess()) {
            XAMPP::Error("BTaggingSF::BTaggingSF()", "Could not set up BTaggingEfficiencyTool.");
            return false;
        }
        return true;
    }
    bool BTaggingSF::InitJets() {
        // names of jet readers taken from MonoH RunConfig
        m_Jet = ParReaderStorage::GetInstance()->GetReader(m_JetCollection);
        if (m_Jet == nullptr) {
            Error("BTaggingSF()", Form("Whaat the jet collection %s does not exist.", m_JetCollection.c_str()));
            return false;
        }
        // retrieve jet variables which are needed for calculation of scale factor
        m_JetPt = m_Jet->RetrieveVariable("pt");
        m_JetEta = m_Jet->RetrieveVariable("eta");
        m_JetTagWeight = m_Jet->RetrieveVariable(m_Tagger);
        m_JetLabelID = m_Jet->RetrieveVariable("HadronConeExclTruthLabelID");
        m_JetIsBTagged = m_Jet->RetrieveVariable("bjet");
        if (m_JetCollection == "TrackJetReader") m_JetIsAssociated = m_Jet->RetrieveVariable("isAssociated");
        return true;
    }

    double BTaggingSF::CalculateBTagSF(double pt, double eta, double tagWeight, int labelID, bool isBTagged) const {
        // fill a CalibrationDataVariables object for each jet:
        // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface/CalibrationDataInterface/CalibrationDataVariables.h

        Analysis::CalibrationDataVariables CalibDataVar;
        CalibDataVar.jetAuthor = m_bTaggingTool->getJetAuthor();
        CalibDataVar.jetPt = pt * 1000;  // pt in Mev
        CalibDataVar.jetEta = eta;
        CalibDataVar.jetTagWeight = tagWeight;

        float sfJet = 1.;
        if (fabs(eta) < 2.5) {
            // If the jet is NOT b-tagged: Use InefficiencyScaleFactor as JetWeightBTag_New instead of ScaleFactor
            if (isBTagged == true) {
                // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/xAODBTaggingEfficiency/BTaggingEfficiencyTool.h#L94
                if (m_bTaggingTool->getScaleFactor(labelID, CalibDataVar, sfJet) == CP::CorrectionCode::Error) {
                    XAMPP::Error("BTaggingSF::CalculateBTagSF()", "Could not calculate ScaleFactor.");
                }
            } else {
                // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/xAODBTaggingEfficiency/BTaggingEfficiencyTool.h#L112
                if (m_bTaggingTool->getInefficiencyScaleFactor(labelID, CalibDataVar, sfJet) == CP::CorrectionCode::Error) {
                    XAMPP::Error("BTaggingSF::CalculateBTagSF()", "Could not calculate InefficiencyScaleFactor.");
                }
            }
        }
        return sfJet;
    }

    std::string BTaggingSF::name() const { return m_Name; }

    bool BTaggingSF::init(TTree* t) {
        if (m_bTagSFReaders.empty()) {
            m_Syst.push_back(CP::SystematicSet());
            // Use PseudoScalarVarReaders to write new bTagging SFs and their systs into TTree
            m_bTagSFReaders.push_back(bTagSFReaders(PseudoScalarVarReader::GetReader(name()), (*m_Syst.begin())));
            m_bTagSFReaders_Itr = m_bTagSFReaders.begin();
        }

        // First check whether all jet readers have been declared
        if (!m_Jet || !m_Jet->init(t)) return false;
        if (!m_JetPt || !m_JetPt->init(t)) return false;
        if (!m_JetEta || !m_JetEta->init(t)) return false;
        if (!m_JetTagWeight || !m_JetTagWeight->init(t)) return false;
        if (!m_JetLabelID || !m_JetLabelID->init(t)) return false;
        if (!m_JetIsBTagged || !m_JetIsBTagged->init(t)) return false;
        if (m_JetCollection == "TrackJetReader") {
            if (!m_JetIsAssociated || !m_JetIsAssociated->init(t)) return false;
        }
        return true;
    }
    double BTaggingSF::read() {
        double W = GetTotalBTagSF();
        if (!SimultaneousSyst())
            m_bTagSFReaders_Itr->PseudoR->SetValue(W);
        else {
            for (const auto R : m_bTagSFReaders) {
                for (const auto& syst : m_Syst) {
                    if (R.Set.name() == syst.name()) {
                        // Calculate bTagging SF for each systematic variation
                        if (m_bTaggingTool->applySystematicVariation(syst) != CP::SystematicCode::Ok) {
                            XAMPP::Error("BTaggingSF::GetTotalBTagSF()", "Could not apply systematic " + syst.name());
                        } else {
                            // fill nominal+syst weight branches in output tree
                            R.PseudoR->SetValue(GetTotalBTagSF());
                        }
                    }
                }
            }
        }
        return W;
    }
    ITreeVarReader* BTaggingSF::GetTreeVarReader() {
        if (m_bTagSFReaders_Itr == m_bTagSFReaders.end()) {
            XAMPP::Warning("BTaggingSF::GetTreeVarReader()", "Could not find the TreeVarReader");
            return PseudoScalarVarReader::GetReader(name());
        }
        return m_bTagSFReaders_Itr->PseudoR;
    }
    ITreeVarReader* BTaggingSF::GetSystematicReader(const std::string& Syst) {
        for (const auto& R : m_bTagSFReaders) {
            if (R.Set.name() == Syst) { return R.PseudoR; }
        }
        return PseudoScalarVarReader::GetReader(name());
    }
    std::vector<std::string> BTaggingSF::FindWeightVariations(TTree*) {
        // Same procedure as for PileUpWeight
        std::vector<std::string> V;
        // Retrieving bTagging systematics as in this example:
        // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/util/BTaggingEfficiencyToolTester.cxx#L146
        CP::SystematicSet systs = m_bTaggingTool->affectingSystematics();
        if (m_Syst.empty()) {
            for (auto iter = systs.begin(); iter != systs.end(); ++iter) {
                CP::SystematicVariation var = *iter;
                CP::SystematicSet set;
                if (XAMPP::IsElementInList(Weight::getWeighter()->excludedSystematics(), var.name())) continue;
                set.insert(var);
                m_Syst.push_back(set);
                V.push_back(var.name());
            }
            m_Syst.push_back(CP::SystematicSet());
        } else {
            for (auto iter = systs.begin(); iter != systs.end(); ++iter) {
                CP::SystematicVariation var = *iter;
                V.push_back(var.name());
            }
        }

        if (m_bTagSFReaders.empty()) {
            for (auto& S : m_Syst) {
                if (XAMPP::IsElementInList(Weight::getWeighter()->excludedSystematics(), S.name())) continue;
                m_bTagSFReaders.push_back(
                    bTagSFReaders(PseudoScalarVarReader::GetReader(name() + (S.name().empty() ? "" : "_" + S.name())), S));
            }
        }
        ResetSystematic();
        m_SystNames = (V);
        return V;
    }
    IWeightElement::SystStatus BTaggingSF::ApplySystematic(const std::string& variation) {
        // Also here same as for PileUpWeight
        IWeightElement::SystStatus Status = IWeightElement::SystStatus::NotAffected;
        for (const auto& syst : m_Syst) {
            if (syst.name() == variation) {
                XAMPP::Info("BTaggingSF::ApplySystematic()", "Systematic " + variation + " affects the JetWeightBTag_new.");
                if (m_bTaggingTool->applySystematicVariation(syst) != CP::SystematicCode::Ok) {
                    XAMPP::Error("BTaggingSF::ApplySystematic()", "Could not apply the variation");
                    Status = IWeightElement::SystStatus::SystError;
                } else {
                    Status = IWeightElement::SystStatus::Affected;
                    for (m_bTagSFReaders_Itr = m_bTagSFReaders.begin(); m_bTagSFReaders_Itr != m_bTagSFReaders.end();
                         ++m_bTagSFReaders_Itr) {
                        if (m_bTagSFReaders_Itr->PseudoR->name().find(variation) != std::string::npos) return Status;
                    }
                }
            }
        }
        return Status;
    }
    void BTaggingSF::ResetSystematic() {
        // By construction the nominal reader should be at the end of the vector
        for (m_bTagSFReaders_Itr = m_bTagSFReaders.begin(); m_bTagSFReaders_Itr != m_bTagSFReaders.end(); ++m_bTagSFReaders_Itr) {
            if (m_bTagSFReaders_Itr->PseudoR->name() == name()) return;
        }
    }

    // returns a vector containing all systematic names
    std::vector<std::string> BTaggingSF::GetWeightVariations() { return m_SystNames; }

    BTaggingSF::~BTaggingSF() {}
    BTaggingSF* BTaggingSF::GetWeighter(const std::string& Name) {
        if (!Weight::getWeighter()->GetWeightElement(Name)) { return new BTaggingSF(Name); }
        return dynamic_cast<BTaggingSF*>(Weight::getWeighter()->GetWeightElement(Name));
    }

}  // namespace XAMPP
#endif

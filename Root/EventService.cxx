#include <XAMPPplotting/EventService.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/Selector.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/Weight.h>

namespace XAMPP {
    bool TreeIndexer::m_buildCommonHash = false;

    bool TreeIndexer::init() {
        if (m_tree == nullptr) {
            Error("TreeIndexer::init()", "No tree object was given");
            return false;
        }
        m_tree->SetBranchStatus("*", 0);
        m_tree->SetCacheSize(m_service->cacheSize());
        if (m_buildCommonHash) {
            m_eventHash[0] = m_eventHash[1] = 0;
            m_hash_branch = m_tree->GetBranch("CommonEventHash");
            if (!m_hash_branch) {
                Error("TreeIndexer::init()",
                      Form("The tree %s has no branch `CommonEventHash`. This one is mandatory to ensure the mapping between the friends",
                           m_tree->GetName()));
                return false;
            }
            m_hash_branch->SetStatus(true);
            if (m_tree->AddBranchToCache("CommonEventHash") == -1) {
                Error("TreeIndexer::init()", "Common event hash could not be added to the tree cache");
                return false;
            }
            if (m_tree->SetBranchAddress("CommonEventHash", &m_eventHash) != 0) {
                Error("TreeIndexer::init()", "Coult not connect the local hash with the tree");
                return false;
            }
        }
        m_event = 0;
        m_evInTree = m_tree->GetEntries();
        return true;
    }
    bool TreeIndexer::getEntry(Long64_t entry) {
        if (entry >= m_evInTree) {
            Error("EventService::getEntry()", Form("The entry %llu is out of range", entry));
            return false;
        }
        m_event = entry;
        if (!m_parent && m_buildCommonHash && !m_hash_branch->GetEntry(entry)) {
            Error("EventService::getEntry()", "Failed to update the common event hash");
            return false;
        }
        if (!m_service->partialEvent()) {
            sync_indexer();
            if (m_tree->GetEntry(m_event) == -1) return false;
        }
        if (m_learningPhase && m_event > 100) {
            m_learningPhase = false;
            m_tree->StopCacheLearningPhase();
        }
        return true;
    }
    TreeIndexer::TreeIndexer(TFriendElement* tree, std::shared_ptr<TreeIndexer> parent) :
        TreeIndexer(Open(EventService::getService()->in_file_path()), tree->GetTree()->GetName()) {
        m_parent = parent;
        m_tree_to_sync = tree->GetTree();
    }
    TreeIndexer::~TreeIndexer() {
        if (m_tree) delete m_tree;
    }
    TreeIndexer::TreeIndexer(std::shared_ptr<TFile> f, const std::string& tree_name) :
        m_tree_to_sync(nullptr),
        m_in_file(f),
        m_tree(nullptr),
        m_hash_branch(nullptr),
        m_parent(nullptr),
        m_eventHash(),
        m_event(0),
        m_evInTree(0),
        m_service(EventService::getService()),
        m_learningPhase(true) {
        m_eventHash[0] = m_eventHash[1] = -1;

        if (m_in_file) m_in_file->GetObject(tree_name.c_str(), m_tree);
        m_tree_to_sync = m_tree;
        if (m_tree)
            m_evInTree = m_tree->GetEntries();
        else
            m_evInTree = -1;
    }

    void TreeIndexer::buildCommonHash(bool B) { m_buildCommonHash = B; }
    //########################################################
    //                  EventService
    //########################################################
    EventService* EventService::m_Inst = nullptr;

    EventService* EventService::getService() {
        if (m_Inst == nullptr) m_Inst = new EventService();
        return m_Inst;
    }
    EventService::~EventService() { m_Inst = nullptr; }

    bool EventService::getEntry(Long64_t entry) {
        if (!m_master_indexer) {
            Error("EventService::getEntry()", "No tree has been initialized");
            return false;
        }
        if (!m_master_indexer->getEntry(entry)) return false;
        if (!partialEvent()) {
            for (const auto& idx : m_friend_indexer) {
                if (!idx->getEntry(entry)) return false;
            }
        }
        m_weighter->NewEvent();
        return true;
    }
    std::shared_ptr<TreeIndexer> EventService::get_master() const { return m_master_indexer; }
    bool EventService::loadTree(const std::string& tree_name) {
        if (tree_name.empty()) {
            Error("EventService::loadTree()", "No tree given ");
            return false;
        }
        if (!m_inFile) {
            Error("EventService::loadTree()", "No input file has been opened");
            return false;
        }
        if (!m_master_indexer || m_master_indexer->tree()->GetName() != tree_name) {
            m_master_indexer = std::make_shared<TreeIndexer>(m_inFile, tree_name);
        }
        ++m_TreesLoaded;
        m_friend_indexer.clear();
        return true;
    }
    bool EventService::setupTree() {
        if (!m_master_indexer) {
            Error("EventService::setupTree()", "No Tree indexer loaded");
            return false;
        }
        TTree* tree = m_master_indexer->tree();
        if (tree->GetListOfFriends()) {
            TreeIndexer::buildCommonHash(true);
            for (const auto& my_friend : *tree->GetListOfFriends()) {
                m_friend_indexer.push_back(std::make_unique<TreeIndexer>(dynamic_cast<TFriendElement*>(my_friend), m_master_indexer));
                Info("EventService::setupTree()", Form("Add %s as friend of the analysis tree %s", my_friend->GetName(), tree->GetName()));
            }
        } else {
            TreeIndexer::buildCommonHash(false);
        }

        if (!m_master_indexer->init()) return false;
        bool err = false;
        for (const auto& idx : m_friend_indexer) {
            if (!idx->init()) return false;
            if (idx->entries() < m_master_indexer->entries()) {
                Error("EventService::setupTree()",
                      Form("The friend tree %s has less events than %s (%llu vs. %llu). The file is corrupted!!!", idx->tree()->GetName(),
                           m_master_indexer->tree()->GetName(), idx->entries(), m_master_indexer->entries()));
                err = true;
            }
        }
        if (err) return false;
        if (m_weighter == nullptr) m_weighter = XAMPP::Weight::getWeighter();
        if (m_selector && !m_selector->begin(tree)) return false;
        if (!m_weighter->init(tree)) {
            Error("EventService::setupTree()", "Could not setup the weights");
            return false;
        }
        for (const auto& R : m_readerToLoad) {
            if (!R->init(tree)) {
                Error("EventService::setupTree()", "Failed to initialize " + R->name());
                return false;
            }
        }
        return true;
    }

    bool EventService::AppendReader(ITreeVarReader* R) {
        if (R == nullptr) {
            Error("EventService::AppendReader()", "No reader given");
            return false;
        }
        if (!IsElementInList(m_readerToLoad, R)) m_readerToLoad.push_back(R);
        return true;
    }
    bool EventService::SetReaders(const std::vector<ITreeVarReader*>& Vector) {
        for (auto& R : Vector) {
            if (!AppendReader(R)) return false;
        }
        return true;
    }
    void EventService::flushReaders() { m_readerToLoad.clear(); }
    EventService::EventService() :
        m_weighter(nullptr),
        m_loadEvent(false),
        m_readerToLoad(),
        m_CacheSize(4.e6),
        m_TreesLoaded(0),
        m_kinematicSyst(),
        m_nominal(nullptr),
        m_master_indexer(nullptr),
        m_friend_indexer(),
        m_selector(nullptr) {}
    void EventService::set_selector(Selector* selector) { m_selector = selector; }
    void EventService::loadEntireEvent() { m_loadEvent = true; }
    void EventService::setCacheSize(size_t S) { m_CacheSize = S; }

    std::shared_ptr<IKinematicSyst> EventService::getSyst(const std::string& name) const {
        for (const auto& syst : m_kinematicSyst) {
            if (syst->syst_name() == name) return syst;
        }
        return std::shared_ptr<IKinematicSyst>();
    }
    const std::vector<std::shared_ptr<IKinematicSyst> >& EventService::all_systematics() const { return m_kinematicSyst; }

    bool EventService::registerKinematic(std::shared_ptr<IKinematicSyst> syst) {
        for (const auto& known : m_kinematicSyst) {
            // Check that the systematic names are unique
            if (known->syst_name() == syst->syst_name()) {
                Error("EventService::registerKinematic()",
                      Form("There exists already a kinematic systematic called %s", syst->syst_name().c_str()));
                return false;
            }
            // Now check that only the nominal tree can be read multiple times
            if (!syst->is_nominal()) continue;
            if (syst->is_nominal() == known->is_nominal()) {
                Error("EventService::registerKinematic()", "Nominal is defined twice? Please check");
                return false;
            }
        }
        if (syst->is_nominal()) m_nominal = syst;
        m_kinematicSyst.push_back(syst);
        return true;
    }
    TreeIndexer* EventService::getIndexer(TBranch* br) const {
        if (br->GetTree() == m_master_indexer->tree()) { return m_master_indexer.get(); }
        for (const auto& idx : m_friend_indexer) {
            if (br->GetTree() == idx->synchronization_tree()) return idx.get();
        }
        return nullptr;
    }

    std::string EventService::in_file_path() const { return m_inFile ? m_filePath : ""; }
    bool EventService::openFile(const std::string& file_path) {
        m_inFile = Open(file_path);
        if (!m_inFile) return false;
        m_filePath = file_path;
        m_friend_indexer.clear();
        m_master_indexer.reset();
        return true;
    }
    bool EventService::openFile(const std::shared_ptr<TFile>& file_obj) {
        if (m_inFile == file_obj) return true;
        m_inFile = file_obj;
        if (!file_obj) {
            Error("Eventservice::openFile()", "Nothing has been delivered");
            return false;
        }
        m_filePath = file_obj->GetName();
        m_friend_indexer.clear();
        m_master_indexer.reset();
        return true;
    }
}  // namespace XAMPP

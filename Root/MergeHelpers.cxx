#include <TFriendElement.h>
#include <XAMPPplotting/MergeHelpers.h>
#include <XAMPPplotting/MetaDataTreeReader.h>
#include <XAMPPplotting/PlottingUtils.h>
namespace XAMPP {
    //############################################################
    //                  CorruptedFileChecker
    //############################################################
    bool CorruptedFileChecker::isFileOk(const std::string& path) { return isFileOk(Open(path)); }
    bool CorruptedFileChecker::isFileOk(std::shared_ptr<TFile> f) {
        if (!f) return false;
        TTree* tree = nullptr;
        f->GetObject("MetaDataTree", tree);
        if (!tree || tree->GetEntries() == 0) {
            Error("CorruptedFileChecker()", Form("No meta data tree found in file %s", f->GetName()));
            return false;
        }
        bool has_nominal = false;
        for (auto key : *f->GetListOfKeys()) {
            std::string key_name = key->GetName();
            f->GetObject(key_name.c_str(), tree);
            if (!tree) continue;
            if (!checkFriendShip(tree)) return false;
            if (key_name.find("Nominal") != std::string::npos) has_nominal = true;
        }
        if (!has_nominal) {
            Error("CorruptedFileChecker()", Form("%s has no nominal tree", f->GetName()));
            return false;
        }
        // NormalizationDataBase::resetDataBase();
        // if (!NormalizationDataBase::getDataBase()->init(std::vector<std::shared_ptr<TFile>>{f})) return false;
        std::vector<unsigned int> dsids = getDSIDs(f);  // NormalizationDataBase::getDataBase()->GetListOfMCSamples();
        if (std::find_if(dsids.begin(), dsids.end(), [](unsigned int dsid) { return dsid != 0 && (dsid < 100000 || dsid >= 1000000); }) !=
            dsids.end()) {
            Error("CorruptedFileChecker()", Form("Spotted invalid Meta Data in file %s", f->GetName()));
            return false;
        }
        Info("CorruptedFileChecker()", Form("Yeah the file %s seems to be healthy :-)", f->GetName()));
        return true;
    }
    bool CorruptedFileChecker::checkFriendShip(TTree* tree) {
        if (!tree->GetListOfFriends()) return true;
        for (const auto& my_friend : *tree->GetListOfFriends()) {
            TTree* friend_tree = dynamic_cast<TFriendElement*>(my_friend)->GetTree();
            if (friend_tree->GetEntries() < tree->GetEntries()) {
                Error("CorruptedFileChecker()",
                      Form("The tree %s is in a close friendship with %s but has more entries than the friend %llu vs. %llu",
                           tree->GetName(), friend_tree->GetName(), tree->GetEntries(), friend_tree->GetEntries()));
                return false;
            }
        }
        return true;
    }
    std::vector<unsigned int> CorruptedFileChecker::getLHEWeights(const std::string& path) { return getLHEWeights(Open(path)); }
    std::vector<unsigned int> CorruptedFileChecker::getLHEWeights(std::shared_ptr<TFile> f) {
        std::vector<unsigned int> lhe;
        if (!f) return lhe;
        TTree* meta_tree = nullptr;
        f->GetObject("MetaDataTree", meta_tree);
        unsigned int proc_id;
        if (!meta_tree || !meta_tree->GetBranch("ProcessID") || meta_tree->SetBranchAddress("ProcessID", &proc_id) < 0) {
            if (!meta_tree) Warning("CorruptedFileChecker()", "Could not read the MetaDataTree");
            return lhe;
        }
        lhe.reserve(meta_tree->GetEntries());
        for (long int e = 0; meta_tree->GetEntry(e) && (e < meta_tree->GetEntries()); ++e) {
            if (proc_id >= 1000) lhe.push_back(proc_id);
        }
        ClearFromDuplicates(lhe);
        std::sort(lhe.begin(), lhe.end());
        return lhe;
    }
    std::vector<unsigned int> CorruptedFileChecker::getDSIDs(const std::string& f) { return getDSIDs(Open(f)); }
    std::vector<unsigned int> CorruptedFileChecker::getDSIDs(const std::shared_ptr<TFile> f) {
        std::vector<unsigned int> dsids;
        if (!f) return dsids;
        TTree* meta_tree = nullptr;
        f->GetObject("MetaDataTree", meta_tree);
        unsigned int mcChannelNumber;
        if (!meta_tree || !meta_tree->GetBranch("mcChannelNumber") ||
            meta_tree->SetBranchAddress("mcChannelNumber", &mcChannelNumber) < 0) {
            if (!meta_tree) Warning("CorruptedFileChecker()", "Could not read the MetaDataTree");
            return dsids;
        }
        dsids.reserve(meta_tree->GetEntries());
        for (long int e = 0; meta_tree->GetEntry(e) && (e < meta_tree->GetEntries()); ++e) { dsids.push_back(mcChannelNumber); }
        ClearFromDuplicates(dsids);
        std::sort(dsids.begin(), dsids.end());

        return dsids;
    }

    //#####################################################
    //                  NtupleFileInfo
    //#####################################################
    NtupleFileInfo::NtupleFileInfo(const std::string& in_path) : m_path(in_path), m_trees(), m_lhe(), m_size(0) {
        std::shared_ptr<TFile> f = Open(in_path);
        if (!CorruptedFileChecker::isFileOk(f)) return;

        for (const auto& key : *f->GetListOfKeys()) {
            TTree* t = nullptr;
            f->GetObject(key->GetName(), t);
            if (t != nullptr) m_trees.push_back(key->GetName());
        }
        ClearFromDuplicates(m_trees);
        std::sort(m_trees.begin(), m_trees.end());
        m_lhe = CorruptedFileChecker::getLHEWeights(f);
        m_size = f->GetSize();
    }
    Long64_t NtupleFileInfo::file_size() const { return m_size; }
    bool NtupleFileInfo::is_good() const { return !m_trees.empty(); }
    unsigned int NtupleFileInfo::num_lhe() const { return m_lhe.size(); }
    unsigned int NtupleFileInfo::num_trees() const { return m_trees.size(); }
    std::string NtupleFileInfo::path() const { return m_path; }
    bool NtupleFileInfo::share_same_trees(const NtupleFileInfo& other) const {
        if (other.m_trees.size() != m_trees.size() || !is_good()) return false;
        for (unsigned int t = 0; t < m_trees.size(); ++t) {
            if (m_trees[t] != other.m_trees[t]) return false;
        }
        return true;
    }
    bool NtupleFileInfo::can_be_merged(const NtupleFileInfo& other) const {
        if (!share_same_trees(other)) return false;
        if (num_lhe() != other.num_lhe()) return false;
        for (unsigned int l = 0; l < num_lhe(); ++l) {
            if (m_lhe[l] != other.m_lhe[l]) return false;
        }
        return true;
    }
    //#####################################################
    //                  MergeHelper
    //#####################################################
    MergeHelper::MergeHelper() : m_files(), m_last(m_files.end()) {}
    bool MergeHelper::add_file(const std::string& f) {
        std::vector<NtupleFileInfo>::const_iterator itr =
            std::find_if(m_files.begin(), m_files.end(), [&f](const NtupleFileInfo& info) { return info.path() == f; });
        if (itr == m_files.end()) {
            m_files.push_back(NtupleFileInfo(f));
            return m_files.back().is_good();
        }
        return itr->is_good();
    }
    void MergeHelper::add_file(const std::vector<std::string>& files) {
        for (const auto& f : files) add_file(f);
    }
    std::vector<std::string> MergeHelper::get_good_files() {
        skim_bad_files();
        std::vector<std::string> good_files;

        good_files.reserve(m_files.size());
        unsigned int cluster_num = 0;
        const NtupleFileInfo* last = nullptr;
        double total_size = 0;
        for (const auto& f : m_files) {
            good_files.push_back(f.path());
            if (last == nullptr)
                last = &f;
            else if (!last->can_be_merged(f)) {
                ++cluster_num;
                last = &f;
            }
            double f_size = double(f.file_size()) / 1024 / 1024 / 1024;
            total_size += f_size;
        }
        Info("MergeHelper()", Form("The dataset has %.3f GB and is grouped into %u diffrent clusters.", total_size, cluster_num + 1));
        return good_files;
    }
    std::vector<Long64_t> MergeHelper::get_sizes() {
        skim_bad_files();
        std::vector<Long64_t> good;
        good.reserve(m_files.size());
        for (const auto& f : m_files) good.push_back(f.file_size());
        return good;
    }
    void MergeHelper::skim_bad_files() {
        EraseFromVector<NtupleFileInfo>(m_files, [](const NtupleFileInfo& info) { return !info.is_good(); });
        std::sort(m_files.begin(), m_files.end(), [](const NtupleFileInfo& a, const NtupleFileInfo& b) {
            if (a.num_trees() != b.num_trees()) return a.num_trees() < b.num_trees();
            if (a.num_lhe() != b.num_lhe()) return a.num_lhe() < b.num_lhe();
            return a.file_size() > b.file_size();
        });
        m_last = m_files.end();
    }
    bool MergeHelper::split_file(const std::string& f) {
        std::vector<NtupleFileInfo>::const_iterator itr =
            std::find_if(m_files.begin(), m_files.end(), [&f](const NtupleFileInfo& a) { return a.path() == f; });
        if (itr == m_files.end()) {
            throw std::range_error(Form("The file %s is unkown. Please make sure to add it before calling this class.", f.c_str()));
        }
        if (m_last == m_files.end()) {
            m_last = itr;
            return false;
        }
        if (!(*m_last).can_be_merged(*itr)) {
            m_last = itr;
            return true;
        }
        return false;
    }

}  // namespace XAMPP
